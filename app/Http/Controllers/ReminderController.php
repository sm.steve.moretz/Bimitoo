<?php

namespace App\Http\Controllers;

use App\Reminder;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReminderController extends Controller
{
    public function setRem(){
        $validated = \request()->validate([
            'date' => ['required'],
        ]);
        Reminder::create([
            'set_at'=>Carbon::parse($validated['date']),
            'user_id'=>auth()->user()->id
        ]);
        return json_encode(['status'=>'ok']);
    }
}
