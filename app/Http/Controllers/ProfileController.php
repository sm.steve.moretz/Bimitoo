<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        return view('profile-user',[
            'user'=>auth()->user()->toArray()
        ]);
    }

    public function indexAdmin(){
        return view('profile-admin',[

        ]);
    }
}
