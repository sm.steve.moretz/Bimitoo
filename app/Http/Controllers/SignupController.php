<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Hash;

class SignupController extends Controller
{
    public function showPhone(){
        if(auth()->user()){
            return \Redirect::to('/');
        }else{
            return view('register');
        }
    }
    public function storePhone(){
        $validated = \request()->validate([
            'mobile' => ['required', 'regex:/09[0-9]{9}/','unique:users,phone'],
        ]);
        $code = Rand(1000, 9999);
        session(['code'=>$code]);


        $message = "سایت بیمیتو :‌ رمز شما : ".$code;
        $to = $validated['mobile'];
        $endPoint = "http://login.niazpardaz.ir/SMSInOutBox/SendSms?username=09389147874&password=901591&from=09389147874&to=$to&text=$message";
        $client = new Client();
        try{
            $response = $client->get($endPoint);
            return view('verification',$validated);
        } catch (RequestException $e) {
            return $e->getResponse()->getBody()->getContents();
        }
//
//        try {
//            $parameters['userName'] = "09389147874";
//            $parameters['password'] = "901591";
////            $parameters['fromNumber'] = "your dedicated sms number";
//            $parameters['toNumbers'] = array("09120001234");
//            $parameters['messageContent'] = "سایت بیمیتو :‌ رمز شما : ".$code;
//            $parameters['isFlash'] = false;
//            $recId = array(0);
//            $status = 0x0;
//            $parameters['recId'] = &$recId ;
//            $parameters['status'] = &$status ;
//            dd($sms_client->SendSMS($parameters)->SendSMSResult);
//        }
//        catch (Exception $e)
//        {
//            dd('Caught exception: ',  $e->getMessage(), "\n");
//        }
    }
    public function showVerify(){
        return \Redirect::to('/register');
    }
    public function verify(){
        $validated = \request()->validate([
            'mobile' => ['required', 'regex:/09[0-9]{9}/','unique:users,phone'],
        ]);
        if (session('code') != null && \request()->code == session('code')) {
            return redirect()->action('SignupController@showSaveUser',\request()->all());
        }else{
            return view('verification',$validated)->withErrors(['*کد اشتباه است']);
        }
    }
    public function showSaveUser()
    {
        return view('signup',[
            'code' => \request('code'),
            'mobile' => \request('mobile')
        ]);
    }
    public function saveUser(){
        $validated = \request()->validate([
            'password' => ['confirmed','min:8']
        ]);
        if (session('code') != null && \request()->code == session('code')) {
            dd(\request()->all(),Hash::make(\request('password')),Hash::check('19371937',Hash::make(\request('password'))));
        }else{
            return redirect()->action('SignupController@showPhone');
        }
    }

}
