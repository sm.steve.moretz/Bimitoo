<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class CheckTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'time:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the time offset';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "admin created_at " .json_encode(User::findOrFail(1)->selectRaw('created_at,pdate(CONVERT_TZ(created_at,"+00:00","'.env('timeOffset').'")) as jalali')->get());
    }
}
