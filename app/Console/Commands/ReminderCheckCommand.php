<?php

namespace App\Console\Commands;

use App\Reminder;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;

class ReminderCheckCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reminders = Reminder::where('set_at', '<', Carbon::now()->toDateTimeString());
        foreach ($reminders->get() as $reminder){
            $user = User::findOrFail($reminder->user_id);
            $phone = $user->phone;
            //TODO send sms to the user
            $message = "یادآور شما از سایت بیمیتو";
            $endPoint = "http://login.niazpardaz.ir/SMSInOutBox/SendSms?username=09389147874&password=901591&from=09389147874&to=$phone&text=$message";
            $client = new Client();
            try{
                $response = $client->get($endPoint);
            } catch (RequestException $e) {
                return $e->getResponse()->getBody()->getContents();
            }
            echo $phone;
        }
        $reminders->delete();
    }
}
