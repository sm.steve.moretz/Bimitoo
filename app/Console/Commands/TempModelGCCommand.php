<?php

namespace App\Console\Commands;

use App\Services\TempUility;
use Illuminate\Console\Command;

class TempModelGCCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tempmodel:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove Expired TempModels';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        TempUility::gc();
        echo 'done';
    }
}
