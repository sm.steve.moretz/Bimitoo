<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\IsAdmin;

Route::get('/', function () {
    return view('welcome');
});
Route::get('/about-us', 'UserRoutesController@about_us');
Route::get('/index', 'UserRoutesController@index');
Route::get('/contact-us', 'UserRoutesController@contact_us');
Route::get('/post', 'UserRoutesController@post');
Route::get('/post-text', 'UserRoutesController@post_text');
Route::get('/rules', 'UserRoutesController@rules');
Route::get('/save', 'UserRoutesController@save');

//-----------------------------------------------------------------------------------------------------------------------------

Route::get('/third-party-insurance', 'UserRoutesController@third_party_insurance');
Route::get('/third-party-insurance-compare', 'UserRoutesController@third_party_insurance_compare');
Route::get('/auto-body-insurance', 'UserRoutesController@auto_body_insurance');
Route::get('/auto-body-insurance-compare', 'UserRoutesController@auto_body_insurance_compare');
Route::get('/collaboration', 'UserRoutesController@collaboration');
Route::get('/collaboration-compare', 'UserRoutesController@collaboration_compare');
Route::get('/erth-insurance', 'UserRoutesController@erth_insurance');
Route::get('/erth-insurance-compare', 'UserRoutesController@erth_insurance_compare');
Route::get('/fire-insurance', 'UserRoutesController@fire_insurance');
Route::get('/fire-insurance-compare', 'UserRoutesController@fire_insurance_compare');
Route::get('/life-insurance', 'UserRoutesController@life_insurance');
Route::get('/life-insurance-compare', 'UserRoutesController@life_insurance_compare');
Route::get('/motor-third-party-insurance', 'UserRoutesController@motor_third_party_insurance');
Route::get('/motor-third-party-insurance-compare', 'UserRoutesController@motor_third_party_insurance_compare');
Route::get('/other-insurance', 'UserRoutesController@other_insurance');
Route::get('/other-insurance-compare', 'UserRoutesController@other_insurance_compare');
Route::get('/supplementary-health-insurance', 'UserRoutesController@supplementary_health_insurance');
Route::get('/supplementary-health-insurance-compare', 'UserRoutesController@supplementary_health_insurance_compare');
Route::get('/third-party-insurance', 'UserRoutesController@third_party_insurance');
Route::get('/third-party-insurance-compare', 'UserRoutesController@third_party_insurance_compare');
Route::get('/travel-insurance', 'UserRoutesController@travel_insurance');
Route::get('/travel-insurance-compare', 'UserRoutesController@travel_insurance_compare');


Auth::routes();
Route::get('/register', 'SignupController@showPhone');
Route::post('/reg', 'SignupController@storePhone');
Route::get('/verify', 'SignupController@showVerify');
Route::post('/verify', 'SignupController@verify');
Route::get('/saveUser', 'SignupController@showSaveUser');
Route::post('/saveUser', 'SignupController@saveUser');


Route::post('/checkout','ShopController@checkout')->middleware('auth');
Route::get('/verify/{tempModel}','ShopController@verify')->middleware('auth');


Route::get('/profile','ProfileController@index')->middleware('auth');
Route::get('/admin/profile','ProfileController@indexAdmin')->middleware(IsAdmin::class);

Route::get('/test',function (){

});

Route::get('/reminder/set','ReminderController@setRem')->middleware('auth');

Route::group([
    'prefix' => 'user'
], function () {
    Route::get('/receipts/{page}/{number}','ReceiptController@index')->middleware('auth');
});

Route::group([
    'prefix' => 'admin'
], function () {
    Route::get('/receipts/{page}/{number}','ReceiptController@indexAdmin')->middleware(IsAdmin::class);
});

Route::get('a/{rest?}', function ($rest) {
    Artisan::call($rest);
    echo Artisan::output();
    return "<br>".'php artisan '.$rest;
})->where('rest', '(.*)');


Route::any('/pull', function () {
    if(request('password') === 'as6as5d4as9d87'){
        return shell_exec("cd /home/bimitooc/public_html && /usr/local/cpanel/3rdparty/lib/path-bin/git pull 2>&1");
    }
    abort(404);
});
