    <!--  remebering  -->
    <div class="row">
        <div class="col-xs-12" id="remembering">
            <div class="row ">
                <div class="col-xs-12">
                  <div class="text-center rem-title-box" data-toggle="collapse" data-target="#rem"> 
                    <img src="images/belll.png" alt="">
                    <h4 class="rem-title">یادآور تمدید یا اقساط بیمه</h4>
                  </div>
                </div>
            </div>
            <div class="row rem-box collapse" id="rem">
                <div class="col-xs-12">
                    <form action="">
                        <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group input-box">
                                    <label class="lable-bime">نام</label>
                                    <input class="frm-input form-control" type="text" name="" value="">
                                </div>
                        </div>
                        <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group input-box">
                                    <label class="lable-bime">نام خانوادگی</label>
                                    <input class="frm-input form-control" type="text" name="" value="">
                                </div>
                        </div>
                        <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group input-box">
                                <label class="lable-bime">یادآور برای بیمه ی </label>
                                <select class="frm-input form-control select-bime" id="">
                                    
                                        <option value=""></option>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group input-box">
                                    <label class="lable-bime">توضیحات یادآوری</label>
                                    <input class="frm-input form-control" type="text" name="" value="">
                                </div>
                        </div>
                        <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group input-box">
                                    <label class="lable-bime">تاریخ سر رسید</label>
                                    <input type="text" class="frm-input form-control" id="date1">
                                </div>
                        </div>
                        <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group input-box">
                                    <label class="lable-bime">شماره موبایل</label>
                                    <input class="frm-input form-control" type="text" name="" value="">
                                </div>
                        </div>
                        <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group input-box">
                                    <label class="lable-bime"> استان</label>
                                    <select class="frm-input form-control" >
                                    
                                            <option value=""></option>
                                  
                                    </select>
                                </div>
                        </div>
                        <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group input-box">
                                    <label class="lable-bime">شهر</label>
                                    <select class="frm-input form-control" >                                  
                                            <option value=""></option>
                                    </select>
                                </div>
                        </div>
                        <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group input-box">
                                    <input type="submit" value="یاد آوری کن "  class="btn rem-btn press-btn" >
                                </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- feature -->
    <section>
        <div class="facility-section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-lg-3 col-md-6 col-sm-12 facility-box">
                            <div class="card feature-card">
                                <div class="feature-img">
                                    <img src="images/consultant.png" class="card-img-top " alt="...">
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title">مشاوره تخصصی بیمه</h4>
                                    <p class="card-text">
                                        ارائه مشاوره تخصصی، کاملا بی طرفانه و رایگان توسط کارشناسان بیمه ای بی می تو
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 col-sm-12 facility-box">
                            <div class="card feature-card">
                                <div class="feature-img">
                                    <img src="images/compare.png" class="card-img-top " alt="...">
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title">مقایسه قیمت و شرایط بیمه</h4>
                                    <p class="card-text">
                                        مقایسه قیمت، خدمات و پوشش ها برای انتخاب بهترین بیمه
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 col-sm-12 facility-box">
                            <div class="card feature-card">
                                <div class="feature-img">
                                    <img src="images/free-shippping.png" class="card-img-top " alt="...">
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title"> خرید سریع و آسان</h4>
                                    <p class="card-text">
                                        ارسال فوری در تهران و مراکز استان ها بصورت کاملا رایگان
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 col-sm-12 facility-box">
                            <div class="card feature-card">
                                <div class="feature-img">
                                    <img src="images/cod.png" class="card-img-top " alt="...">
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title"> پرداخت آسان و مطمئن</h4>
                                    <p class="card-text">
                                        امکان پرداخت حق بیمه به سه روش آنلاین، کارت به کارت و پرداخت در محل
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- insurans -->
    <section>
        <div class="insurans-list">
            <div class="logo-title">
                <h2>درست انتخاب کنیم</h2>
            </div>
            <div class="row compare-bime">
            
                    <div class="col-lg-1 col-md-3 col-sm-3 col-6">
                        <div class="mediab">
                            <img src="images/pasargadinsurance.png" alt="...">
                        </div>
                    </div>

                    <div class="col-lg-1 col-md-3 col-sm-3 col-6">
                        <div class="mediab">
                            <img src="images/mic.png" alt="...">
                        </div>
                    </div>

                    <div class="col-lg-1 col-md-3 col-sm-3 col-6">
                        <div class="mediab">
                            <img src="images/novininsurance.png" alt="...">
                        </div>
                    </div>

                    <div class="col-lg-1 col-md-3 col-sm-3 col-6">
                        <div class="mediab">
                            <img src="images/bime-razi.png" alt="...">
                        </div>
                    </div>

                    <div class="col-lg-1 col-md-3 col-sm-3 col-6">
                        <div class="mediab">
                            <img src="images/bimehmellat.png" alt="...">
                        </div>
                    </div>

                    <div class="col-lg-1 col-md-3 col-sm-3 col-6">
                        <div class="mediab">
                            <img src="images/bime-parsian.png" alt="...">
                        </div>
                    </div>

                    <div class="col-lg-1 col-md-3 col-sm-3 col-6">
                        <div class="mediab">
                            <img src="images/bime-saman.png" alt="...">
                        </div>
                    </div>

                    <div class="col-lg-1 col-md-3 col-sm-3 col-6">
                        <div class="mediab">
                            <img src="images/bime-iran.png" alt="...">
                        </div>
                    </div>

                    <div class="col-lg-1 col-md-3 col-sm-3 col-6">
                        <div class="mediab">
                            <img src="images/bime-day.png" alt="...">
                        </div>
                    </div>

                    <div class="col-lg-1 col-md-3 col-sm-3 col-6">
                        <div class="mediab">
                            <img src="images/bime-dana.png" alt="...">
                        </div>
                    </div>

                    <div class="col-lg-1 col-md-3 col-sm-3 col-6">
                        <div class="mediab">
                            <img src="images/bime-asia.png" alt="...">
                        </div>
                    </div>

                    <div class="col-lg-1 col-md-3 col-sm-3 col-6">
                        <div class="mediab">
                            <img src="images/bime-alborz.png" alt="...">
                        </div>
                    </div>
                
            </div>
        </div>
    </section>
    <!-- article-->
    <section>
        <div class="article-box">
            <div class="row">
                <div class="logo-title">
                    <h2> اخبار بیمه  </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 blog-boxx" >
                        <div class="blog-box">
                            <img src="images/3.jpg" class="blog-img">
                            <div class="blog-op">
                                <h4 class="blog-title">بیمه مسئولیت پزشکان  </h4>
                                <a href="#" class="btn blog-submit">ادامه مطلب </a>
                            </div>
                        </div>
                    </div>
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 blog-boxx" >
                        <div class="blog-box">
                            <img src="images/9.jpg" class="blog-img">
                            <div class="blog-op">
                                <h4 class="blog-title">بیمه مسئولیت پزشکان  </h4>
                                <a href="#" class="btn blog-submit">ادامه مطلب </a>
                            </div>
                        </div>
                    </div>
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 blog-boxx" >
                        <div class="blog-box">
                            <img src="images/5.jpg" class="blog-img">
                            <div class="blog-op">
                                <h4 class="blog-title">بیمه مسئولیت پزشکان  </h4>
                                <a href="#" class="btn blog-submit">ادامه مطلب </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
