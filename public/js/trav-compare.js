/* ------------------------------------------------------ in first  -----------------------------------------------------------------*/

//to save send object data
var SendObj;

var CountryListv=[];
var CountryListt=[];

/* get from api and prev page and check are empty or not*/
GetInfoFromApiPrev();

/* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/

$("#TCountries-select").change(function () {

    let el=$(this);

    let SelElVl=el.val();

    if (SelElVl !="00"){

        let stop="option[value='"+SelElVl+"']";
        let SelEl= el.find(stop);

        SelEl.toggleClass("sel");


        SelectedCountry();


        ShowCountry(CountryListt);

        checkEmptyCountry(el);

        let NewCount=CountEmptyBox();

        let NameInObj=$(this).attr("NameInObj");

        SendObj[NameInObj]=CountryListv;
        PostData(SendObj,NewCount);
    }

});

$("#TBirthDate-select").change(function () {

    let nv=$(this).val();

    let t=nv.length;
    if  (t==0) {nv=" ";}

    $(this).attr("value",nv);


});

/* check empty */
$("#wizard-compare4 .compare-input").change(function(){

    let el=$(this);

    let elid=el.attr("id");

    if (elid != "TCountries-select") {

        let Newval=el.val();

        let NameInObj=el.attr("NameInObj");

        let ty=el.attr("type");
        if ( ty=="date" ) { Newval=el.attr("value"); }


        checkEmpty(el);

        let NewCount=CountEmptyBox();

        SendObj[NameInObj]=Newval;
        PostData(SendObj,NewCount);
    }


});

/* -------------------------------------------------------------- my functions -----------------------------------------*/

/* get information from api for insurance form */
function  GetInfoFromApiPrev() {

    /* get list of selects box from api */

    const data = new FormData();
    data.append('_token', '{{csrf_token()}}');

    let TravCountries=[];
    let TravDurations=[];

    (async () =>{

        try {
            const response = await fn1('api/insurance/insurance/travel/basic-data', 'GET', data, {});
            const res = await response.json();

            TravCountries=res.Countries;
            TravDurations=res.Durations;
        }
        catch (e) {
            console.log(e);
        }

        /* get selected option from prev page */
        let searchParams = new URLSearchParams(window.location.search);
        let SCountryList= searchParams.get('CountryList');
        let STravDurations= searchParams.get('TravDurations');
        let STravBirthDate= searchParams.get('TravBirthDate');

        var SCountryArr=SCountryList.split(',');


        /* put select box lists and selected option-check empty or not - check count az empty or not */
        let element;
        element=$("#TCountries-select");
        AddInput(element,TravCountries);
        AddSelctedCountry(element,SCountryArr);
        checkEmptyCountry(element);
        SelectedCountry();
        ShowCountry(CountryListt);
        element=$("#TDurations-select");
        AddInput(element,TravDurations);
        AddSelcted(element,STravDurations);
        checkEmpty(element);
        element=$("#TBirthDate-select");
        AddValue(element,STravBirthDate);
        checkEmpty(element);

        /* obj to post */

        let CountryIds=CountryListv;
        let DurationId=parseInt(STravDurations);
        let BirthDate=STravBirthDate;

        SendObj={ CountryIds:CountryIds,DurationId:DurationId,BirthDate:BirthDate,
            _token : '{{csrf_token()}}'
        };


        /* count neccessary empty fileds */
        let emptyCount=CountEmptyBox();

        PostData(SendObj,emptyCount);


    })();

}
/* put arrays of api in select inputs */
function AddInput(el,arr) {


    let child="";

    for (let i=0;i<arr.length;i++) {

        child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

    }

    el.children().remove();
    let FirstOp="<option value='00'> انتخاب کنید  </option>";
    el.append(FirstOp);
    el.append(child);
}
/* put selected to selected option in element */
function AddSelcted(el,sel) {

    el.find("option[value="+sel+"]").attr("selected","selected");
    el.val(sel);
}
/* put value */
function AddValue(el,vl) {
    el.attr("value",vl);
}
/* put Sel to selected list */
function AddSelctedCountry(el,arr) {

    el.find("option").each(function () {

        let ov=$(this).attr("value");
        let ot=$(this).text();
        $(this).attr("tx",ot);

        for(let i=0;i<arr.length;i++){

                if(ov==arr[i]) {
                    $(this).addClass("sel");
                    break;
                }
        }

    });

}
/* check empty fields */
function checkEmpty(el){

    let sv=el.val();

    let ty=el.attr("type");
    if (ty=="date") { sv=el.attr("value"); }


    //no select-empty
    if ( (sv == "00" ) ||(sv==" ") ||(sv.trim().length==0) ) {

          //show error
        el.parent().addClass("empty-box");
    }
    // select-full
    else {
        //hide error
        el.parent().removeClass("empty-box");
    }

}
/* check empty Country fields */
function checkEmptyCountry(el){

    var CountSel=0;

    el.find("option").each(function () {

        let oc=$(this).attr("Class");

        if (oc=="sel") {CountSel=CountSel+1;}

    });

    //no sleceted country
    if ( CountSel==0) {
        el.parent().addClass("empty-box");

    }
    //sleceted country
    else {
        el.parent().removeClass("empty-box");
    }

}
/* count empty box */
function CountEmptyBox() {

    let i=0;
    $("#wizard-compare4").find(".empty-box.req").each(function () {
        i=i+1;
    });

    return i;
}
/* post data to api */
function PostData(obj,emptyCount){

    //is empty
    if (emptyCount > 0 ) {


        $(".result-list").children().remove();

        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> توجه! برای نمایش بسته های پیشنهادی بیمه ، اطلاعات مورد نیاز را وارد نمایید</span></div>";

        setTimeout(function () {
            $(".result-list").append(str);
        },200);
    }
    //is full
    else {

        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/travel/inquiry', 'POST', obj, {});
                const res = await response.json();


                var msg=res.Message;
                var ResArr=res.Inquiries;
                var CompArr=res.Companies;
                var DurArr=res.Durations;
                var Coverages=res.Coverages;
                var Zones=res.Zones;

                $(".result-list").children().remove();

                //has error message
                if (msg !=null) {

                    let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span>"+ msg +"</span></div>";
                    setTimeout(function () {
                        $(".result-list").append(str);
                    },200);
                }

                //no error message
                else{

                    if (ResArr.length==0) {

                        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> هیچ نتیجه ای برای این بیمه وجود ندارد</span></div>";
                        setTimeout(function () {
                            $(".result-list").append(str);
                        },200);
                    }
                    else {
                        loading(2000);

                        for (let i=0;i<ResArr.length;i++) {

                            var UniqueId=ResArr[i].UniqueId;

                            var TarhTitle=ResArr[i].Title;
                            var TarhSubTitle=ResArr[i].SubTitle;

                            var FirstAmount=ResArr[i].CashPrice.FirstAmount;
                            var FinalAmount=ResArr[i].CashPrice.FinalAmount;

                            var dis=false;

                            if (FirstAmount != FinalAmount ) {
                                dis=true;
                            }

                            var HasInstallments=ResArr[i].HasInstallments;

                            var  CoverageId=ResArr[i].Details.CoverageId;
                            var  ZoneId=ResArr[i].Details.ZoneId;

                            var CompanyId=ResArr[i].CompanyId;
                            var DurationId=ResArr[i].DurationId;

                            for (let j=0;j<CompArr.length;j++){
                                if ( CompArr[j].Id==CompanyId )  {
                                    var CompTitlt=CompArr[j].Title;
                                    var CompLogo=CompArr[j].LogoURL;
                                    var CompStar=CompArr[j].FinancialStrength;
                                    var CompBranch=CompArr[j].CompensationBranchCount;
                                }
                            }

                            for (let j=0;j<DurArr.length;j++){
                                if ( DurArr[j].Id==DurationId )  {
                                    var DurTitlt=DurArr[j].Title;
                                }
                            }

                            for (let j=0;j<Coverages.length;j++){

                                if ( Coverages[j].Id==CoverageId )  {
                                    var CoverTitlt=Coverages[j].Title;
                                }
                            }

                            for (let j=0;j<Zones.length;j++){

                                if ( Zones[j].Id==ZoneId )  {
                                    var ZoneTitl=Zones[j].Title;
                                }
                            }

                            let  str="<div class='result-row' id="+UniqueId+">";

                            str=str+"<div class='result-row-box'>";


                            str=str+ "<div class='company-logo'><img src="+CompLogo+" alt="+CompTitlt+"></div>";

                            str=str+ "<div class='company-desc'><div class='desc-col'><span> سطح توانگری مالی </span>";
                            for (let k=0;k<CompStar;k++){
                                str=str+" <span class='fa fa-star star'></span>";
                            }
                            str=str+ "</div><div class='desc-col'><span class='branchCount'>"+CompBranch+"</span><span> شعبه پرداخت خسارت </span></div></div>";

                            str=str+ "<div class='plan-desc'><div class='plan-col'><span> پوشش : </span>"+CoverTitlt+"</div>";
                            str=str+ "<div class='plan-col'><span> ناحیه سفر : </span>"+ZoneTitl+"</div></div>";

                            str=str+ "<div class='company-dur'>"+DurTitlt+"</div>" ;

                            str=str+ "<div class='company-money'>";
                            if (dis) {
                                str=str+"<div class='company-col discount'>"+FirstAmount+"</div>" ;
                            }
                            str=str+"<div class='company-col'>"+FinalAmount+"</div>" ;
                            str=str+ "<p class='rial'> ریال </p></div>";

                            str=str+ "<div class='pay-box'> <button class='btn'> خرید نقدی </button>" ;
                            if (HasInstallments !=null) {
                                str=str+ "<button class='btn aghsat'> اقساطی </button>" ;
                            }
                            str=str+ "</div>";

                            str=str+ "</div>";

                            str=str+ "</div>";

                            setTimeout(function () {
                                $(".result-list").append(str);
                            },200);

                        }
                    }
                }


            }
            catch (e) {
                console.log(e);
            }

        })();
    }

}
/* for loading*/
function loading(time) {

    $(".loading-box").removeClass("dn");
    $(".loading-box .loading-sq").addClass("rotate-class");

    setTimeout(function () {
        $(".loading-box").addClass("dn");
        $(".loading-box .loading-sq").removeClass("rotate-class");
    },time);

}
/* update SelectedCountry */
function SelectedCountry() {

    let arrv=[];
    let arrt=[];

    $("#TCountries-select").find(".sel").each(function () {

        let cl=$(this).attr("class");

        if (cl=="sel") {

            let v=$(this).attr("value");
            let t=$(this).attr("tx");

            arrv.push(v);
            arrt.push(t);
        }

    });

    CountryListv=arrv;
    CountryListt=arrt;

}
/* show country list */
function ShowCountry(list) {

    $(".country-list").children().remove();

    let st="";
    st=st+"<div class='country-title'>کشورهای انتخابی : </div>";

    for (let i=0;i<list.length;i++) {

        st=st+"<div class='country-box'>"+list[i]+"</div>";
    }

    $(".country-list").append(st);


}
