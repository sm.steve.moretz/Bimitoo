
    /* ------------------------------------------------------ in first  -----------------------------------------------------------------*/

    /* get from api */
    GetInfoFromApi();

    /* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/

    /* go to compare page */
    $("#wizard7 .wizard-finish").click(function () {

        let queryst="";

        let EstateTypes=$("#EstateTypes-select").val();
        queryst=queryst+"EstateTypes="+EstateTypes;
        let StructureTypes=$("#StructureTypes-select").val();
        queryst=queryst+"&StructureTypes="+StructureTypes;
        let Unit=$("#Unit-input").val();
        queryst=queryst+"&Unit="+Unit;
        let AreaUnitPrices=$("#AreaUnitPrices-select").val();
        queryst=queryst+"&AreaUnitPrices="+AreaUnitPrices;
        let Meter=$("#Meter-input").val();
        queryst=queryst+"&Meter="+Meter;
        let Cost=$("#HomeCost-input").val();
        queryst=queryst+"&Cost="+Cost;


        window.open("http://bimitoo.com/fire-insurance-compare?"+queryst,"_self");

    });

    /* check estate is apartemant or no */
    $("#EstateTypes-select").change(function () {

        let v=$(this).val();
        let parcol=$(this).parents(".tab-col");

        //is apartemant
        if (v=="3") {

            $(".unit-condition").removeClass("dn");
            parcol.removeClass("col-md-6");
            parcol.addClass("col-md-4");

        }
        else {

            $(".unit-condition").addClass("dn");
            parcol.addClass("col-md-6");
            parcol.removeClass("col-md-4");

        }

    });

    /* -------------------------------------------------------------- my functions -----------------------------------------*/
    /* get information from api for insurance form */
    function  GetInfoFromApi() {

        const data = new FormData();
        data.append('_token', '{{csrf_token()}}');

        let EstateTypes=[];
        let StructureTypes=[];
        let AreaUnitPrices=[];

        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/fire/basic-data', 'GET', data, {});
                const res = await response.json();
                EstateTypes=res.EstateTypes;
                StructureTypes=res.StructureTypes;
                AreaUnitPrices=res.AreaUnitPrices;
            }
            catch (e) {
                console.log(e);
            }

            let element;
            element=$("#EstateTypes-select");
            AddInput(element,EstateTypes);
            element=$("#StructureTypes-select");
            AddInput(element,StructureTypes);
            element=$("#AreaUnitPrices-select");
            AddInput(element,AreaUnitPrices);

        })();
    }
    /* put arrays of api in select inputs */
    function AddInput(el,arr) {

        let child="";

        for (let i=0;i<arr.length;i++) {

            child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

        }
        el.children().remove();
        let FirstOp="<option value='00'> انتخاب کنید  </option>";
        el.append(FirstOp);
        el.append(child);
    }


