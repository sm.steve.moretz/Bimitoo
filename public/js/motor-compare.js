/* ------------------------------------------------------ in first  -----------------------------------------------------------------*/

//to save send object data
var SendObj;

/* get from api and prev page and check are empty or not*/
GetInfoFromApiPrev();

/* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/

$("#MotorEnd-date").change(function () {

    let nv=$(this).val();

    let t=nv.length;
    if  (t==0) {nv=" ";}

    $(this).attr("value",nv);

});

/* check empty */
$("#wizard-compare2 .compare-input").change(function(){

    let el=$(this);

    let myid=el.attr("id");

    if ( ( myid !="MotorInsuranceStatuses-select")&&( myid !="MotorLossstatus-select") ){

        let Newval=el.val();

        let NameInObj=el.attr("NameInObj");

        let ty=el.attr("type");
        if ( ty=="date" ) { Newval=el.attr("value"); }

        checkEmpty(el);

        let NewCount=CountEmptyBox();

        SendObj[NameInObj]=Newval;
        PostData(SendObj,NewCount);
    }

});

/* check active or not in steps of form */
$("#MotorInsuranceStatuses-select").change(function () {

    let v=$(this).val();
    //has prev insurance
    if (v=="2") {

        //show or hide conditional filed
        $(".motor-condition.conditional-box").removeClass("dn");

        //change req fileds
        $(".motor-condition.conditional-box > .form-group").addClass("req");


    }
    //dont not has prev insurance
    else {

        //show or hide conditional filed
        $(".motor-condition.conditional-box").addClass("dn");

        //change req fileds
        $(".motor-condition.conditional-box > .form-group").removeClass("req");

    }

    let el=$(this);
    checkEmpty(el);

    let NewCount=CountEmptyBox();

    let NameInObj=el.attr("NameInObj");

    SendObj[NameInObj]=v;
    PostData(SendObj,NewCount);


});
$("#MotorLossstatus-select").change(function () {

    let v=$(this).val();
    if (v=="0") {

        $(".motor-condition.conditional-loss").addClass("dn");
        $(".motor-condition.conditional-loss > .form-group").removeClass("req");
    }
    else {

        $(".motor-condition.conditional-loss").removeClass("dn");
        $(".motor-condition.conditional-loss > .form-group").addClass("req");

    }

    let el=$(this);
    checkEmpty(el);

    let NewCount=CountEmptyBox();

    let NameInObj=el.attr("NameInObj");

    SendObj[NameInObj]=v;
    PostData(SendObj,NewCount);

});


/* -------------------------------------------------------------- my functions -----------------------------------------*/

/* get information from api for insurance form */
function  GetInfoFromApiPrev() {

    /* get list of selects box from api */

    const data = new FormData();
    data.append('_token', '{{csrf_token()}}');

    let MotorTypes=[];
    let MotorProductionYears=[];
    let MotorInsuranceStatuses=[];
    let MotorCompanies=[];
    let MotorDurations=[];
    let MotorDriverDiscounts=[];
    let MotorThirdPartyDiscounts=[];
    let MotorLifeLosses=[];
    let MotorPropertyLosses=[];
    let MotorDriverLosses=[];
    let MotorCoverageTypes=[];

    (async () =>{

        try {
            const response = await fn1('api/insurance/insurance/motor/basic-data', 'GET', data, {});
            const res = await response.json();


            MotorTypes=res.MotorTypes;
            MotorProductionYears=res.ProductionYears;
            MotorInsuranceStatuses=res.InsuranceStatuses;
            MotorCompanies=res.Companies;
            MotorDurations=res.Durations;
            MotorDriverDiscounts=res.DriverDiscounts;
            MotorThirdPartyDiscounts=res.ThirdPartyDiscounts;
            MotorLifeLosses=res.LifeLosses;
            MotorPropertyLosses=res.PropertyLosses;
            MotorDriverLosses=res.DriverLosses;
            MotorCoverageTypes=res.CoverageTypes;


        }
        catch (e) {
            console.log(e);
        }

        /* get selected option from prev page */
        let searchParams = new URLSearchParams(window.location.search);
        let SMotorModels= searchParams.get('MotorModels');
        let SMotorProductionYears= searchParams.get('MotorProductionYears');
        let SMotorInsuranceStatuses= searchParams.get('MotorInsuranceStatuses');
        let SMotorCompanies= searchParams.get('MotorCompanies');
        let SMotorDurations= searchParams.get('MotorDurations');
        let SMotorEnddate= searchParams.get('MotorEnddate');
        let SMotorDriverDiscounts= searchParams.get('MotorDriverDiscounts');
        let SMotorThirdPartyDiscounts= searchParams.get('MotorThirdPartyDiscounts');
        let SMotorLossstatus= searchParams.get('MotorLossstatus');
        let SMotorLifeLosses= searchParams.get('MotorLifeLosses');
        let SMotorPropertyLosses= searchParams.get('MotorPropertyLosses');
        let SMotorDriverLosses= searchParams.get('MotorDriverLosses');

        /* put select box lists and selected option-check empty or not - check count az empty or not */
        let element;
        element=$("#MotorModel-select");
        AddInput(element,MotorTypes);
        AddSelcted(element,SMotorModels);
        checkEmpty(element);
        element=$("#MotorProductionYears-select");
        AddInput(element,MotorProductionYears);
        AddSelcted(element,SMotorProductionYears);
        checkEmpty(element);
        element=$("#MotorInsuranceStatuses-select");
        AddInput(element,MotorInsuranceStatuses);
        AddSelcted(element,SMotorInsuranceStatuses);
        checkEmpty(element);

        let v=SMotorInsuranceStatuses;
        //has prev insurance
        if (v=="2") {

            //show or hide conditional filed
            $(".motor-condition.conditional-box").removeClass("dn");

            //change req fileds
            $(".motor-condition.conditional-box > .form-group").addClass("req");


        }
        //dont not has prev insurance
        else {

            //show or hide conditional filed
            $(".motor-condition.conditional-box").addClass("dn");

            //change req fileds
            $(".motor-condition.conditional-box > .form-group").removeClass("req");

        }

        element=$("#MotorCompanies-select");
        AddInput(element,MotorCompanies);
        AddSelcted(element,SMotorCompanies);
        checkEmpty(element);
        element=$("#MotorDurations-select");
        AddInput(element,MotorDurations);
        AddSelcted(element,SMotorDurations);
        checkEmpty(element);
        element=$("#MotorEnd-date");
        AddValue(element,SMotorEnddate);
        checkEmpty(element);
        element=$("#MotorDriverDiscounts-select");
        AddInput(element,MotorDriverDiscounts);
        AddSelcted(element,SMotorDriverDiscounts);
        checkEmpty(element);
        element=$("#MotorThirdPartyDiscounts-select");
        AddInput(element,MotorThirdPartyDiscounts);
        AddSelcted(element,SMotorThirdPartyDiscounts);
        checkEmpty(element);
        element=$("#MotorLossstatus-select");
        AddSelcted(element,SMotorLossstatus);
        checkEmpty(element);

        let vv=SMotorLossstatus;
        if (vv=="0") {

            $(".motor-condition.conditional-loss").addClass("dn");
            $(".motor-condition.conditional-loss > .form-group").removeClass("req");
        }
        else {

            $(".motor-condition.conditional-loss").removeClass("dn");
            $(".motor-condition.conditional-loss > .form-group").addClass("req");

        }

        element=$("#MotorLifeLosses-select");
        AddInput(element,MotorLifeLosses);
        AddSelcted(element,SMotorLifeLosses);
        checkEmpty(element);
        element=$("#MotorPropertyLosses-select");
        AddInput(element,MotorPropertyLosses);
        AddSelcted(element,SMotorPropertyLosses);
        checkEmpty(element);
        element=$("#MotorDriverLosses-select");
        AddInput(element,MotorDriverLosses);
        AddSelcted(element,SMotorDriverLosses);
        checkEmpty(element);
        element=$("#MotorCoverageTypes-select");
        AddInput(element,MotorCoverageTypes);
        checkEmpty(element);

        /* obj to post */

        let MotorTypeId=parseInt(SMotorModels);
        let ProductionYearId=parseInt(SMotorProductionYears);
        let PreviousInsuranceStatusId=parseInt(SMotorInsuranceStatuses);
        let PreviousCompanyId=parseInt(SMotorCompanies);
        let PreviousDurationId=parseInt(SMotorDurations);
        let PreviousExpirationDate=SMotorEnddate;
        let DriverDiscountId=parseInt(SMotorDriverDiscounts);
        let ThirdPartyDiscountId=parseInt(SMotorThirdPartyDiscounts);
        let LifeLossId=parseInt(SMotorLifeLosses);
        let PropertyLossId=parseInt(SMotorPropertyLosses);
        let DriverLossId=parseInt(SMotorDriverLosses);


        SendObj={ MotorTypeId:MotorTypeId,ProductionYearId:ProductionYearId,PreviousInsuranceStatusId:PreviousInsuranceStatusId,PreviousDurationId:PreviousDurationId,
            PreviousCompanyId:PreviousCompanyId ,PreviousExpirationDate:PreviousExpirationDate ,DriverDiscountId:DriverDiscountId ,ThirdPartyDiscountId:ThirdPartyDiscountId ,


            _token : '{{csrf_token()}}'
        };

        /* count neccessary empty fileds */
        let emptyCount=CountEmptyBox();

        PostData(SendObj,emptyCount);


    })();

}
/* put arrays of api in select inputs */
function AddInput(el,arr) {


    let child="";

    for (let i=0;i<arr.length;i++) {

        child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

    }

    el.children().remove();
    let FirstOp="<option value='00'> انتخاب کنید  </option>";
    el.append(FirstOp);
    el.append(child);
}
/* put selected to selected option in element */
function AddSelcted(el,sel) {

    el.find("option[value="+sel+"]").attr("selected","selected");
    el.val(sel);
}
/* put value */
function AddValue(el,vl) {
    el.attr("value",vl);
}
/* check empty fields */
function checkEmpty(el){

    let sv=el.val();

    let ty=el.attr("type");
    if (ty=="date") { sv=el.attr("value"); }

    //no select-empty
    if ( (sv == "00" ) ||(sv==" ") ) {
        //show error
        el.parent().addClass("empty-box");
    }
    // select-full
    else {
        //hide error
        el.parent().removeClass("empty-box");
    }

}
/* count empty box */
function CountEmptyBox() {

    let i=0;
    $("#wizard-compare2").find(".empty-box.req").each(function () {
        i=i+1;
    });
    return i;
}
/* post data to api */
function PostData(obj,emptyCount){


    //is empty
    if (emptyCount > 0 ) {


        $(".result-list").children().remove();

        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> توجه! برای نمایش بسته های پیشنهادی بیمه ، اطلاعات مورد نیاز را وارد نمایید</span></div>";

        setTimeout(function () {
            $(".result-list").append(str);
        },200);
    }
    //is full
    else {

        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/motor/inquiry', 'POST', obj, {});
                const res = await response.json();

                console.log(res);


                var msg=res.Message;
                var ResArr=res.Inquiries;
                var CompArr=res.Companies;
                var DurArr=res.Durations;

                $(".result-list").children().remove();

                //has error message
                if (msg !=null) {

                    let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span>"+ msg +"</span></div>";
                    setTimeout(function () {
                        $(".result-list").append(str);
                    },200);
                }

                //no error message
                else {
                    if (ResArr.length==0) {

                        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> هیچ نتیجه ای برای این بیمه وجود ندارد</span></div>";
                        setTimeout(function () {
                            $(".result-list").append(str);
                        },200);
                    }
                    else {
                        loading(2000);
                        for (let i=0;i<ResArr.length;i++) {

                            var UniqueId=ResArr[i].UniqueId;

                            var FirstAmount=ResArr[i].CashPrice.FirstAmount;
                            var FinalAmount=ResArr[i].CashPrice.FinalAmount;

                            var TarhTitle=ResArr[i].Title;
                            var TarhSubTitle=ResArr[i].SubTitle;

                            var dis=false;

                            if (FirstAmount != FinalAmount ) {
                                dis=true;
                            }

                            var HasInstallments=ResArr[i].HasInstallments;

                            var Description=ResArr[i].Details.Description;
                            var DelayPenalty=ResArr[i].Details.DelayPenalty;
                            var PenaltyDayCount=ResArr[i].Details.PenaltyDayCount;

                            var CompanyId=ResArr[i].CompanyId;
                            var DurationId=ResArr[i].DurationId;

                            for (let j=0;j<CompArr.length;j++){
                                if ( CompArr[j].Id==CompanyId )  {
                                    var CompTitlt=CompArr[j].Title;
                                    var CompLogo=CompArr[j].LogoURL;
                                    var CompStar=CompArr[j].FinancialStrength;
                                    var CompBranch=CompArr[j].CompensationBranchCount;
                                }
                            }

                            for (let j=0;j<DurArr.length;j++){
                                if ( DurArr[j].Id==DurationId )  {
                                    var DurTitlt=DurArr[j].Title;
                                }
                            }

                            let  str="<div class='result-row' id="+UniqueId+">";

                            str=str+"<div class='result-row-box'>";

                            if (TarhTitle !=null) {
                                str=str+ "<div class='tarh-box'><span>"+TarhTitle+"</span>";
                            }
                            if (TarhSubTitle !=null) {
                                str=str+ "<span class='trah-sub'>"+TarhSubTitle+"</span>";
                            }
                            if (TarhTitle !=null) {
                                str=str+"</div>";
                            }

                            str=str+ "<div class='company-logo'><img src="+CompLogo+" alt="+CompTitlt+"></div>";

                            str=str+ "<div class='company-desc'><div class='desc-col'><span> سطح توانگری مالی </span>";
                            for (let k=0;k<CompStar;k++){
                                str=str+" <span class='fa fa-star star'></span>";
                            }
                            str=str+ "</div><div class='desc-col'><span class='branchCount'>"+CompBranch+"</span><span> شعبه پرداخت خسارت </span></div></div>";

                            if (Description!=null){
                                str=str+ "<div class='company-cover'><pre>"+Description+"</pre></div>";
                            }

                            str=str+ "<div class='company-dur'>"+DurTitlt+"</div>" ;

                            str=str+ "<div class='company-money'>";
                            if (dis) {
                                str=str+"<div class='company-col discount'>"+FirstAmount+"</div>" ;
                            }
                            str=str+"<div class='company-col'>"+FinalAmount+"</div>" ;
                            str=str+ "<p class='rial'> ریال </p></div>";

                            str=str+ "<div class='penalty-box'> <div class='penalty-col'> <span>   تعداد روز های جریمه : </span>"+ PenaltyDayCount+" </div>" ;
                            str=str+ "<div class='penalty-col'> <span>   مبلغ جریمه دیرکرد  : </span>"+ DelayPenalty+" ریال </div>" ;
                            str=str+ "</div>";

                            str=str+ "<div class='pay-box'> <button class='btn'> خرید نقدی </button>" ;
                            if (HasInstallments !=null) {
                                str=str+ "<button class='btn aghsat'> اقساطی </button>" ;
                            }
                            str=str+ "</div>";

                            str=str+ "</div>";

                            str=str+ "</div>";

                            setTimeout(function () {
                                $(".result-list").append(str);
                            },200);

                        }
                    }
                }



            }
            catch (e) {
                console.log(e);
            }

        })();
    }

}
/* for loading*/
function loading(time) {

      $(".loading-box").removeClass("dn");
      $(".loading-box .loading-sq").addClass("rotate-class");

      setTimeout(function () {
          $(".loading-box").addClass("dn");
          $(".loading-box .loading-sq").removeClass("rotate-class");
      },time);

}
