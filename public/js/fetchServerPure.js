

function fn1 (url,method = 'GET',body,headers = {},props) {

        var Env ={
            domain : 'http://bimitoo.com',
        };

        if (method && method.toUpperCase() === 'GET' && body) {
            if (!url.endsWith('?')) url += '?';
            var index = 0;
            for (var p in body) {
                if (index++ !== 0) url += '&';
                url += p + '=' + body[p];
            }
            body  = undefined;
        }
        var res=fetch(Env.domain +'/'+ url, {
            credentials: "include",
            ...props,
            method : method,
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Content-Type': 'application/json',
                ...headers
            },
            body: body ? JSON.stringify({...body}) : undefined
        });

        return res;
};
