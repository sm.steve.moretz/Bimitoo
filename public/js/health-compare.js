/* ------------------------------------------------------ in first  -----------------------------------------------------------------*/

//to save send object data
var SendObj;

/* get from api and prev page and check are empty or not*/
GetInfoFromApiPrev();

/* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/
$(document).on("mouseenter",".HealhDetail",function () {

    $(this).find(".HoverDeta").removeClass("dn");
});
$(document).on("mouseleave",".HealhDetail",function () {

    $(this).find(".HoverDeta").addClass("dn");
});

$(document).on("click",".takfolrem-btn",function () {

    let tpar=$(this).parent(".takafol-row");

    tpar.remove();
});
$(document).on("change",".takafol-row #HealthDate-input",function () {

    let nv=$(this).val();

    let t=nv.length;
    if  (t==0) {nv=" ";}

    $(this).attr("value",nv);
});
$(document).on("change",".takafol-row .compare-input",function () {

    let el=$(this);
    checkEmpty(el);

});

$(".takfoladd-btn").click(function () {

    let ind=$(".takafol-box").children().length;
    let indd=ind+1;
    let takid="tak"+indd;

    let st1= "<div class='form-group req'> " ;
        st1=st1 +"<label class='compare-label'> تاریخ تولد  : </label>";
        st1=st1+"<span class='req-star'> * </span>";
        st1=st1+"<input type='date' class='compare-input form-control' id='HealthDate-input' value=' '>";
        st1=st1 +"  </div>" ;

    let st2= "<div class='form-group req'> " ;
        st2=st2 +"<label class='compare-label'>  بیمه گر پایه :  </label>";
        st2=st2+"<span class='req-star'> * </span>";
        st2=st2+"<select class='compare-input form-control' id='HealthBasicInsurers-select'>";
        st2=st2+"<option value='00'> انتخاب کنید</option>";
        st2=st2 +"   </select>";
        st2=st2 +"  </div>" ;

    let takafolist="<div class='takafol-row' id='"+takid+"'> <button class='takfolrem-btn'> &times; </button> "+st1+st2+"</div>";

    $(".takafol-box").append(takafolist);

    let trow=$(".takafol-box").find("#"+takid);

    AddTakafolList(trow);

});
$(".takfolest-btn").click(function () {

    PostTakafolList();

});

$("#HealthDate-input").change(function () {

    let nv=$(this).val();

    let t=nv.length;
    if  (t==0) {nv=" ";}

    $(this).attr("value",nv);

});

/* check empty */
$("#wizard-compare6 .compare-input").change(function(){

    let el=$(this);

    let Newval=el.val();

    let NameInObj=el.attr("NameInObj");

    checkEmpty(el);

    let NewCount=CountEmptyBox();

    SendObj[NameInObj]=Newval;
    PostData(SendObj,NewCount);

});

/* -------------------------------------------------------------- my functions -----------------------------------------*/

/* get information from api for insurance form */
function  GetInfoFromApiPrev() {

    /* get list of selects box from api */

    const data = new FormData();
    data.append('_token', '{{csrf_token()}}');

    let BasicInsurers=[];

    (async () =>{

        try {
            const response = await fn1('api/insurance/insurance/health/basic-data', 'GET', data, {});
            const res = await response.json();

            BasicInsurers=res.BasicInsurers;

        }
        catch (e) {
            console.log(e);
        }

        /* get selected option from prev page */
        let searchParams = new URLSearchParams(window.location.search);
        let SHealthDate= searchParams.get('HealthDate');
        let SHealthBasicInsurers= searchParams.get('HealthBasicInsurers');

        /* put select box lists and selected option-check empty or not - check count az empty or not */
        let element;
        element=$("#HealthBasicInsurers-select");
        AddInput(element,BasicInsurers);
        AddSelcted(element,SHealthBasicInsurers);
        checkEmpty(element);
        element=$("#HealthDate-input");
        AddValue(element,SHealthDate);
        checkEmpty(element);

        /* obj to post */

        let BirthDate=SHealthDate;
        let BasicInsuranceId=parseInt(SHealthBasicInsurers);

        SendObj={ BirthDate:BirthDate,BasicInsuranceId:BasicInsuranceId,

            _token : '{{csrf_token()}}'
        };

        /* count neccessary empty fileds */
        let emptyCount=CountEmptyBox();

        PostData(SendObj,emptyCount);


    })();

}
/* put arrays of api in select inputs */
function AddInput(el,arr) {


    let child="";

    for (let i=0;i<arr.length;i++) {

        child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

    }

    el.children().remove();
    let FirstOp="<option value='00'> انتخاب کنید  </option>";
    el.append(FirstOp);
    el.append(child);
}
/* put arrays of checkbox in select inputs */
function AddSelcted(el,sel) {

    el.find("option[value="+sel+"]").attr("selected","selected");
    el.val(sel);
}
/* put value */
function AddValue(el,vl) {
    el.attr("value",vl);
}
/* check empty fields */
function checkEmpty(el){

    let sv=el.val();

    let ty=el.attr("type");
    if (ty=="date") { sv=el.attr("value"); }

    //no select-empty
    if ( (sv == "00" ) ||(sv==" ") ) {
        //show error
        el.parent().addClass("empty-box");
    }
    // select-full
    else {
        //hide error
        el.parent().removeClass("empty-box");
    }

}
/* count empty box */
function CountEmptyBox() {

    let i=0;
    $("#wizard-compare6").find(".empty-box.req").each(function () {
        i=i+1;
    });
    return i;
}
/* post data to api */
function PostData(obj,emptyCount){

    //is empty
    if (emptyCount > 0 ) {

        $(".result-list").children().remove();

        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> توجه! برای نمایش بسته های پیشنهادی بیمه ، اطلاعات مورد نیاز را وارد نمایید</span></div>";

        setTimeout(function () {
            $(".result-list").append(str);
        },200);
    }
    //is full
    else {

        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/health/inquiry', 'POST', obj, {});
                const res = await response.json();

                console.log("res of health");
                console.log(res);

                var msg=res.Message;
                var ResArr=res.Inquiries;
                var CompArr=res.Companies;
                var DurArr=res.Durations;
                var MoreDet=res.MoreDetails;

                $(".result-list").children().remove();


                //has error message
                if (msg.length !=0) {

                    let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span>"+ msg +"</span></div>";
                    setTimeout(function () {
                        $(".result-list").append(str);
                    },200);

                }

                //no error message
                else {


                    if (ResArr.length==0) {

                        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> هیچ نتیجه ای برای این بیمه وجود ندارد</span></div>";
                        setTimeout(function () {
                            $(".result-list").append(str);
                        },200);
                    }
                    else {
                        loading(2000);
                        for (let i=0;i<ResArr.length;i++) {

                            var UniqueId=ResArr[i].UniqueId;

                            var TarhTitle=ResArr[i].Title;
                            var TarhSubTitle=ResArr[i].SubTitle;

                            var FirstAmount=ResArr[i].CashPrice.FirstAmount;
                            var FinalAmount=ResArr[i].CashPrice.FinalAmount;

                            var dis=false;

                            if (FirstAmount != FinalAmount ) {
                                dis=true;
                            }

                            var HasInstallments=ResArr[i].HasInstallments;


                            var CompanyId=ResArr[i].CompanyId;
                            var DurationId=ResArr[i].DurationId;

                            for (let j=0;j<CompArr.length;j++){
                                if ( CompArr[j].Id==CompanyId )  {
                                    var CompTitlt=CompArr[j].Title;
                                    var CompLogo=CompArr[j].LogoURL;
                                    var CompStar=CompArr[j].FinancialStrength;
                                    var CompBranch=CompArr[j].CompensationBranchCount;
                                }
                            }

                            for (let j=0;j<DurArr.length;j++){
                                if ( DurArr[j].Id==DurationId )  {
                                    var DurTitlt=DurArr[j].Title;
                                }
                            }


                            var RowMoreArr=ResArr[i].MoreDetails;
                            var DetailBoxes="";
                            for (let m=0;m<RowMoreArr.length;m++){

                                var MoreId=RowMoreArr[m].Id;
                                var MoreVal=RowMoreArr[m].Value;

                                for (let l=0;l<MoreDet.length;l++){

                                    if ( MoreId==MoreDet[l].Id )  {
                                        var MoreTitle=MoreDet[l].Title;
                                        var MoreDEsc=MoreDet[l].Description;
                                    }
                                }

                                DetailBoxes=DetailBoxes+"<div class='HealhDetail'>";
                                    DetailBoxes=DetailBoxes+"<span class='HealhDetailbox' >"+ MoreTitle +" : "+ MoreVal+"</span>";
                                    DetailBoxes=DetailBoxes+"<span class='HoverDeta dn'>"+ MoreDEsc +" </span>";
                                DetailBoxes=DetailBoxes+"</div>";

                            }

                            var NeedToCompleteTheForm=ResArr[i].Details.NeedToCompleteTheForm;
                            if (NeedToCompleteTheForm) { var NeedToCompleteTheFormv="دارد" ;}
                            else { var NeedToCompleteTheFormv="ندارد" ;}

                            var FranchiseFee=ResArr[i].Details.FranchiseFee;
                            var FranchiseFeev=FranchiseFee+"%";

                            var Description=ResArr[i].Details.Description;


                            var PersonsDetails=ResArr[i].PersonsDetails;
                            var PersonST="";
                            for (let m=0;m<PersonsDetails.length;m++){

                                let numberp=m+1;

                                let costt="هزینه نفر"+numberp+"ام";

                                let PersonPrice=PersonsDetails[m].Price;

                                PersonST=PersonST+"<div class='PersonPrice-box'>";
                                PersonST=PersonST+"<span class='PersonPrice' >"+costt+" : "+ PersonPrice +"</span>";
                                PersonST=PersonST+"</div>";

                            }

                            let  str="<div class='result-row' id="+UniqueId+">";

                            str=str+"<div class='result-row-box'>";

                            if (TarhTitle !=null) {
                                str=str+ "<div class='tarh-box'><span>"+TarhTitle+"</span>";
                            }
                            if (TarhSubTitle !=null) {
                                str=str+ "<span class='trah-sub'>"+TarhSubTitle+"</span>";
                            }
                            if (TarhTitle !=null) {
                                str=str+"</div>";
                            }


                            str=str+ "<div class='company-logo'><img src="+CompLogo+" alt="+CompTitlt+"></div>";

                            str=str+ "<div class='company-desc'><div class='desc-col'><span> سطح توانگری مالی </span>";
                            for (let k=0;k<CompStar;k++){
                                str=str+" <span class='fa fa-star star'></span>";
                            }
                            str=str+ "</div><div class='desc-col'><span class='branchCount'>"+CompBranch+"</span><span> شعبه پرداخت خسارت </span></div></div>";


                            str=str+ "<div class='company-desc health-form'><div class='desc-col'><span> تکمیل فرم پزشکی : </span>"+ NeedToCompleteTheFormv+"</div>";
                            str=str+ "<div class='desc-col'><span>   فرانشیز :</span>"+FranchiseFeev+"</div>";
                            str=str+ "<div class='desc-col'>"+Description+"</div></div>";


                            str=str+ "<div class='company-dur'>"+DurTitlt+"</div>" ;

                            str=str+ "<div class='company-money'>";
                            if (dis) {
                                str=str+"<div class='company-col discount'>"+FirstAmount+"</div>" ;
                            }
                            str=str+"<div class='company-col'>"+FinalAmount+"</div>" ;
                            str=str+ "<p class='rial'> ریال </p></div>";


                            str=str+ "<div class='pay-box'> <button class='btn'> خرید نقدی </button>" ;
                            if (HasInstallments !=null) {
                                str=str+ "<button class='btn aghsat'> اقساطی </button>" ;
                            }
                            str=str+ "</div>";


                            str=str+ "</div>";

                            if (RowMoreArr.length !=0){

                                str=str+ "<div class='result-row-detail'><div class='plan-col'>" ;
                                let colid="covcollapse"+i;
                                let colidd="#covcollapse"+i;
                                str=str+ "<a href="+colidd +" data-toggle='collapse' > جزییات پوشش <span class='fa fa-angle-down text-danger'></span> </a>";
                                str=str+ "<div class='collapse' id="+colid+"><div class='healthDetrow'>"+DetailBoxes+"</div></div></div></div>" ;
                            }
                            if (PersonsDetails.length !=0){

                                str=str+ "<div class='result-row-detail'><div class='plan-col'>" ;
                                let colid="percollapse"+i;
                                let colidd="#percollapse"+i;
                                str=str+ "<a href="+colidd +" data-toggle='collapse' > جزییات قیمت ها <span class='fa fa-angle-down text-danger'></span> </a>";
                                str=str+ "<div class='collapse' id="+colid+"><div class='healthDetrow'>"+PersonST+"</div></div></div></div>" ;
                            }

                            str=str+ "</div>";


                            setTimeout(function () {
                                $(".result-list").append(str);
                            },200);

                        }
                    }
                }


            }
            catch (e) {
                console.log(e);
            }

        })();
    }

}
/* for loading*/
function loading(time) {

    $(".loading-box").removeClass("dn");
    $(".loading-box .loading-sq").addClass("rotate-class");

    setTimeout(function () {
        $(".loading-box").addClass("dn");
        $(".loading-box .loading-sq").removeClass("rotate-class");
    },time);

}
/* for Add TakafolList*/
function AddTakafolList(tr) {

    const data = new FormData();
    data.append('_token', '{{csrf_token()}}');

    let BasicInsurers=[];

    (async () =>{

        try {
            const response = await fn1('api/insurance/insurance/health/basic-data', 'GET', data, {});
            const res = await response.json();

            BasicInsurers=res.BasicInsurers;

        }
        catch (e) {
            console.log(e);
        }

        let element=tr.find("#HealthBasicInsurers-select");
        AddInput(element,BasicInsurers);
        checkEmpty(element);
        element=tr.find("#HealthDate-input");
        checkEmpty(element);

    })();
}
/* for post TakafolList*/
function PostTakafolList(){
    let temp=[];

    $(".takafol-row").each(function () {

        let tdr=$(this).find("#HealthDate-input").attr("value");
        let thr=parseInt($(this).find("#HealthBasicInsurers-select").val());

        let tobj={BirthDate:tdr,BasicInsuranceId:thr};

        temp.push(tobj);
    });

    let NewCount=CountEmptyBox();

    let NameInObj="FamilyMembers";

    SendObj[NameInObj]=temp;
    PostData(SendObj,NewCount);
}

