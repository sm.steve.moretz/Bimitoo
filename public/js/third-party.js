
    /* ------------------------------------------------------ in first  -----------------------------------------------------------------*/

    /* set index in wizard form */
    let el=$("#wizard1");
    SetWizardStepId(el);
    SetWizardIndText(el);

    /* get from api */
    GetInfoFromApi();

    /* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/

    /* get model */
    $("#Brands-select").change(function () {

        let CarBrand=$(this).val();

        if (CarBrand !="00"){
            GetCarModel(CarBrand);
        }

    });

    /* check active or not in steps of form */
    $("#InsuranceStatuses-select").change(function () {

        let v=$(this).val();
        let par=$("#wizard1");

        //has prev insurance
        if (v==2) {

            par.find(".wizard-conditional").removeClass("dn");
            //hide Number-date filed
            par.find(".number-condition").addClass("dn");
            //show conditional-btn-next filed
            par.find(".conditional-btn-next").removeClass("dn");
            //hide conditional-btn-reg filed
            par.find(".number-condition-reg").addClass("dn");
        }
        //dont not has prev insurance
        else {

            par.find(".wizard-conditional").addClass("dn");
            //show Number-date filed
            par.find(".number-condition").removeClass("dn");
            //hide conditional-btn-next filed
            par.find(".conditional-btn-next").addClass("dn");
            //show conditional-btn-reg filed
            par.find(".number-condition-reg").removeClass("dn");

        }

        SetWizardStepId(par);
        SetWizardIndText(par);

    });
    $("#Lossstatus-select").change(function () {

        let v=$(this).val();
        let par=$("#wizard1");

        if (v==0) {

            par.find(".loss-condition").addClass("dn");

        }
        else {

            par.find(".loss-condition").removeClass("dn");

        }

    });

    $("#Number-date").change(function () {

        let nv=$(this).val();

        let t=nv.length;
        if  (t==0) {nv=" ";}

        $(this).attr("value",nv);

    });
    $("#End-date").change(function () {

        let nv=$(this).val();

        let t=nv.length;
        if  (t==0) {nv=" ";}

        $(this).attr("value",nv);

    });

    /* go to compare page */
    $("#wizard1 .wizard-finish").click(function () {

        let queryst="";

        let Brands=$("#Brands-select").val();
        queryst=queryst+"Brands="+Brands;
        let Models=$("#Model-select").val();
        queryst=queryst+"&Models="+Models;
        let UsingTypes=$("#UsingTypes-select").val();
        queryst=queryst+"&UsingTypes="+UsingTypes;
        let ProductionYears=$("#ProductionYears-select").val();
        queryst=queryst+"&ProductionYears="+ProductionYears;
        let InsuranceStatuses=$("#InsuranceStatuses-select").val();
        queryst=queryst+"&InsuranceStatuses="+InsuranceStatuses;
        let Numberdate=$("#Number-date").attr("value");
        queryst=queryst+"&Numberdate="+Numberdate;
        let Companies=$("#Companies-select").val();
        queryst=queryst+"&Companies="+Companies;
        let Durations=$("#Durations-select").val();
        queryst=queryst+"&Durations="+Durations;
        let Enddate=$("#End-date").attr("value");
        queryst=queryst+"&Enddate="+Enddate;
        let DriverDiscounts=$("#DriverDiscounts-select").val();
        queryst=queryst+"&DriverDiscounts="+DriverDiscounts;
        let ThirdPartyDiscounts=$("#ThirdPartyDiscounts-select").val();
        queryst=queryst+"&ThirdPartyDiscounts="+ThirdPartyDiscounts;
        let Lossstatus=$("#Lossstatus-select").val();
        queryst=queryst+"&Lossstatus="+Lossstatus;
        let LifeLosses=$("#LifeLosses-select").val();
        queryst=queryst+"&LifeLosses="+LifeLosses;
        let PropertyLosses=$("#PropertyLosses-select").val();
        queryst=queryst+"&PropertyLosses="+PropertyLosses;
        let DriverLosses=$("#DriverLosses-select").val();
        queryst=queryst+"&DriverLosses="+DriverLosses;

        window.open("http://bimitoo.com/third-party-insurance-compare?"+queryst,"_self");

    });

    /* -------------------------------------------------------------- my functions -----------------------------------------*/

    /* get information from api for insurance form */
    function  GetInfoFromApi() {

        const data = new FormData();
        data.append('_token', '{{csrf_token()}}');

        let Brands=[];
        let UsingTypes=[];
        let ProductionYears=[];
        let InsuranceStatuses=[];
        let Companies=[];
        let Durations=[];
        let DriverDiscounts=[];
        let ThirdPartyDiscounts=[];
        let LifeLosses=[];
        let PropertyLosses=[];
        let DriverLosses=[];

        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/third-party/basic-data', 'GET', data, {});
                const res = await response.json();
                Brands=res.Brands;
                UsingTypes=res.UsingTypes;
                ProductionYears=res.ProductionYears;
                InsuranceStatuses=res.InsuranceStatuses;
                Companies=res.Companies;
                Durations=res.Durations;
                DriverDiscounts=res.DriverDiscounts;
                ThirdPartyDiscounts=res.ThirdPartyDiscounts;
                LifeLosses=res.LifeLosses;
                PropertyLosses=res.PropertyLosses;
                DriverLosses=res.DriverLosses;

            }
            catch (e) {
                console.log(e);
            }

            let element;
            element=$("#Brands-select");
            AddInput(element,Brands);
            element=$("#UsingTypes-select");
            AddInput(element,UsingTypes);
            element=$("#ProductionYears-select");
            AddInput(element,ProductionYears);
            element=$("#InsuranceStatuses-select");
            AddInput(element,InsuranceStatuses);
            element=$("#Companies-select");
            AddInput(element,Companies);
            element=$("#Durations-select");
            AddInput(element,Durations);
            element=$("#DriverDiscounts-select");
            AddInput(element,DriverDiscounts);
            element=$("#ThirdPartyDiscounts-select");
            AddInput(element,ThirdPartyDiscounts);
            element=$("#LifeLosses-select");
            AddInput(element,LifeLosses);
            element=$("#PropertyLosses-select");
            AddInput(element,PropertyLosses);
            element=$("#DriverLosses-select");
            AddInput(element,DriverLosses);

        })();
    }
    /* put arrays of api in select inputs */
    function AddInput(el,arr) {

        let child="";

        for (let i=0;i<arr.length;i++) {

            child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

        }
        el.children().remove();
        let FirstOp="<option value='00'> انتخاب کنید  </option>";
        el.append(FirstOp);
        el.append(child);
    }
    /* get model of selected brand */
    function GetCarModel(CarBrandId) {


        const data = new FormData();
        data.append('_token', '{{csrf_token()}}');

        let address='api/insurance/insurance/third-party/car-model/'+CarBrandId;

        let CarModels=[];

        (async () =>{

            try {
                const response = await fn1(address, 'GET', data, {});
                const res = await response.json();
                CarModels=res.Models;

                console.log(CarModels);
            }
            catch (e) {
                console.log(e);
            }

            let element;
            element=$("#Model-select");
            AddInput(element,CarModels);

        })();

        Innerloading(1000);

    }
    /* put dynamic index in wizard steps */
    function SetWizardStepId(el){

        let i=1;
        el.find(".wizard-step").each(function(){

            let t=$(this).hasClass("dn");
            if (!t) {
                let strwid="step"+i.toString();
                $(this).attr("id",strwid);
                i=i+1;
            }
            else {
                $(this).removeAttr("id");
            }

        });

    }
    function SetWizardIndText(el){

        let i=1;
        el.find(".wizard-step-indicator").each(function(){

            let t=$(this).hasClass("dn");

            if (!t) {
                let strwid=i.toString();
                $(this).find("span").text(strwid);
                i=i+1;
            }
            else {
                $(this).find("span").text("");
            }

        });

    }
    /* for inner loading*/
    function Innerloading(time) {

        $(".Innerloading-box").removeClass("dn");
        $(".Innerloading-box .Innerloading-sq").addClass("opac-class");

        setTimeout(function () {
            $(".Innerloading-box").addClass("dn");
            $(".Innerloading-box .Innerloading-sq").removeClass("opac-class");

        },time);

    }

