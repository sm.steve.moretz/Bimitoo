/* ------------------------------------------------------ in first  -----------------------------------------------------------------*/

//to save send object data
var SendObj;

/* get from api and prev page and check are empty or not*/
GetInfoFromApiPrev();

/* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/
/* get model */
$("#AutoBrands-select").change(function () {

    let AutoBr=$(this).val();

    if (AutoBr !="00"){
        GetAutoModel(AutoBr);
    }

});

/* check active or not in steps of form */
$("#AutoInsuranceStatuses-select").change(function () {

    let v=$(this).val();
    //has prev insurance
    if (v=="1") {

        //show or hide conditional filed
        $(".auto-condition.conditional-box").removeClass("dn");

        //change req fileds
        $(".auto-condition.conditional-box > .form-group").addClass("req");


    }
    //dont not has prev insurance
    else {

        //show or hide conditional filed
        $(".auto-condition.conditional-box").addClass("dn");

        //change req fileds
        $(".auto-condition.conditional-box > .form-group").removeClass("req");

    }

});

/* check empty */
$("#wizard-compare3 .compare-input").change(function(){

    let el=$(this);

    let Newval=el.val();

    let NameInObj=el.attr("NameInObj");

     checkEmpty(el);

     let NewCount=CountEmptyBox();

     SendObj[NameInObj]=Newval;
     PostData(SendObj,NewCount);

});


/* -------------------------------------------------------------- my functions -----------------------------------------*/

/* get information from api for insurance form */
function  GetInfoFromApiPrev() {

    /* get list of selects box from api */

    const data = new FormData();
    data.append('_token', '{{csrf_token()}}');

    let AutoBrands=[];
    let AutoUsingTypes=[];
    let AutoProductionYears=[];
    let CarBodyNoDamageYears=[];
    let AutoCoverages=[];
    let AutoDiscounts=[];

    (async () =>{

        try {
            const response = await fn1('api/insurance/insurance/car-body/basic-data', 'GET', data, {});
            const res = await response.json();

            AutoBrands=res.Brands;
            AutoUsingTypes=res.UsingTypes;
            AutoProductionYears=res.ProductionYears;
            CarBodyNoDamageYears=res.CarBodyNoDamageYears;
            AutoCoverages=res.Coverages;
            AutoDiscounts=res.Discounts;
        }
        catch (e) {
            console.log(e);
        }


        /* get selected option from prev page */
        let searchParams = new URLSearchParams(window.location.search);
        let SAutoBrands= searchParams.get('AutoBrands');
        let SAutoModels= searchParams.get('AutoModels');
        let SAutoUsingTypes= searchParams.get('AutoUsingTypes');
        let SAutoProductionYears= searchParams.get('AutoProductionYears');
        let SAutoLife= searchParams.get('AutoLife');
        let SAutoPrice= searchParams.get('AutoPrice');
        let SAutoVaredat= searchParams.get('AutoVaredat');
        let SAutoInsuranceStatuses= searchParams.get('AutoInsuranceStatuses');
        let SAutoNoDisYear= searchParams.get('AutoNoDisYear');

        /* put select box lists and selected option-check empty or not - check count az empty or not */
        let element;
        element=$("#AutoBrands-select");
        AddInput(element,AutoBrands);
        AddSelcted(element,SAutoBrands);
        checkEmpty(element);
        if (SAutoBrands !="00"){
            GetAutoModel(SAutoBrands);
        }
        element=$("#AutoModel-select");
        AddSelcted(element,SAutoModels);
        checkEmpty(element);
        element=$("#AutoUsingTypes-select");
        AddInput(element,AutoUsingTypes);
        AddSelcted(element,SAutoUsingTypes);
        checkEmpty(element);
        element=$("#AutoProductionYears-select");
        AddInput(element,AutoProductionYears);
        AddSelcted(element,SAutoProductionYears);
        checkEmpty(element);
        element=$("#AutoLife-select");
        AddValue(element,SAutoLife);
        checkEmpty(element);
        element=$("#AutoPrice-select");
        AddValue(element,SAutoPrice);
        checkEmpty(element);
        element=$("#AutoVaredat-select");
        AddSelcted(element,SAutoVaredat);
        checkEmpty(element);
        element=$("#AutoInsuranceStatuses-select");
        AddSelcted(element,SAutoInsuranceStatuses);

        let v=SAutoInsuranceStatuses;
        //has prev insurance
        if (v=="1") {

            //show or hide conditional filed
            $(".auto-condition.conditional-box").removeClass("dn");

            //change req fileds
            $(".auto-condition.conditional-box > .form-group").addClass("req");


        }
        //dont not has prev insurance
        else {

            //show or hide conditional filed
            $(".auto-condition.conditional-box").addClass("dn");

            //change req fileds
            $(".auto-condition.conditional-box > .form-group").removeClass("req");

        }

        element=$("#AutoNoDisYear-select");
        AddInput(element,CarBodyNoDamageYears);
        AddSelcted(element,SAutoNoDisYear);
        checkEmpty(element);
        element=$("#AutoCoverages-box");
        AddCheckbox(element,AutoCoverages);
        /*
        element=$("#AutoDiscount-box");
        AddCheckbox(element,AutoDiscounts);
         */

        /* obj to post */

        let ModelId=parseInt(SAutoModels);
        let UsingTypeId=parseInt(SAutoUsingTypes);
        let ProductionYearID=parseInt(SAutoProductionYears);
        let MonthId=parseInt(SAutoLife);
        let Price=parseInt(SAutoPrice);
        let Imported=SAutoVaredat;
        let CarBodyNoDamageYearId=parseInt(SAutoNoDisYear);

        SendObj={ ModelId:ModelId,UsingTypeId:UsingTypeId,ProductionYearID:ProductionYearID,MonthId:MonthId,Price:Price,
            Imported:Imported ,CarBodyNoDamageYearId:CarBodyNoDamageYearId ,

            _token : '{{csrf_token()}}'
        };

        /* count neccessary empty fileds */
        let emptyCount=CountEmptyBox();

        PostData(SendObj,emptyCount);


    })();

}
/* put arrays of api in select inputs */
function AddInput(el,arr) {


    let child="";

    for (let i=0;i<arr.length;i++) {

        child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

    }

    el.children().remove();
    let FirstOp="<option value='00'> انتخاب کنید  </option>";
    el.append(FirstOp);
    el.append(child);
}
/* put arrays of checkbox in select inputs */
function AddCheckbox(el,arr) {

    let child="";

    for (let i=0;i<arr.length;i++) {

        child=child+"<div class='ExtraCheck-row'><input type='checkbox' class='compare-input' onchange='ToggleCheck(this)' value='00' id="+arr[i].Id +">" +
            "<span class='compare-label'>"+arr[i].Title +"</span></div>";
    }

    el.children().remove();
    el.append(child);
}
/* put selected to selected option in element */
function AddSelcted(el,sel) {

    el.find("option[value="+sel+"]").attr("selected","selected");
    el.val(sel);
}
/* put value */
function AddValue(el,vl) {
    el.attr("value",vl);
}
/* check empty fields */
function checkEmpty(el){

    let sv=el.val();

    let ty=el.attr("type");
    if (ty=="date") { sv=el.attr("value"); }

    //no select-empty
    if ( (sv == "00" ) ||(sv==" ") ) {
        //show error
        el.parent().addClass("empty-box");
    }
    // select-full
    else {
        //hide error
        el.parent().removeClass("empty-box");
    }

}
/* count empty box */
function CountEmptyBox() {

    let i=0;
    $("#wizard-compare3").find(".empty-box.req").each(function () {
        i=i+1;
    });
    return i;
}
/* post data to api */
function PostData(obj,emptyCount){


    //is empty
    if (emptyCount > 0 ) {

        $(".result-list").children().remove();

        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> توجه! برای نمایش بسته های پیشنهادی بیمه ، اطلاعات مورد نیاز را وارد نمایید</span></div>";

        setTimeout(function () {
            $(".result-list").append(str);
        },200);
    }
    //is full
    else {

        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/car-body/inquiry', 'POST', obj, {});
                const res = await response.json();

                console.log(res);

                var msg=res.Message;
                var ResArr=res.Inquiries;
                var CompArr=res.Companies;
                var DurArr=res.Durations;

                $(".result-list").children().remove();


                //has error message
                if (msg !=null) {

                    let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span>"+ msg +"</span></div>";
                    setTimeout(function () {
                        $(".result-list").append(str);
                    },200);
                }

                //no error message
                else {


                    if (ResArr.length==0) {

                        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> هیچ نتیجه ای برای این بیمه وجود ندارد</span></div>";
                        setTimeout(function () {
                            $(".result-list").append(str);
                        },200);
                    }
                    else {
                        loading(2000);
                        for (let i=0;i<ResArr.length;i++) {

                            var UniqueId=ResArr[i].UniqueId;

                            var TarhTitle=ResArr[i].Title;
                            var TarhSubTitle=ResArr[i].SubTitle;

                            var FirstAmount=ResArr[i].CashPrice.FirstAmount;
                            var FinalAmount=ResArr[i].CashPrice.FinalAmount;

                            var dis=false;

                            if (FirstAmount != FinalAmount ) {
                                dis=true;
                            }

                            var HasInstallments=ResArr[i].HasInstallments;


                            var CompanyId=ResArr[i].CompanyId;
                            var DurationId=ResArr[i].DurationId;

                            for (let j=0;j<CompArr.length;j++){
                                if ( CompArr[j].Id==CompanyId )  {
                                    var CompTitlt=CompArr[j].Title;
                                    var CompLogo=CompArr[j].LogoURL;
                                    var CompStar=CompArr[j].FinancialStrength;
                                    var CompBranch=CompArr[j].CompensationBranchCount;
                                }
                            }

                            for (let j=0;j<DurArr.length;j++){
                                if ( DurArr[j].Id==DurationId )  {
                                    var DurTitlt=DurArr[j].Title;
                                }
                            }

                            let  str="<div class='result-row' id="+UniqueId+">";

                            str=str+"<div class='result-row-box'>";

                            if (TarhTitle !=null) {
                                str=str+ "<div class='tarh-box'><span>"+TarhTitle+"</span>";
                            }
                            if (TarhSubTitle !=null) {
                                str=str+ "<span class='trah-sub'>"+TarhSubTitle+"</span>";
                            }
                            if (TarhTitle !=null) {
                                str=str+"</div>";
                            }

                            str=str+ "<div class='company-logo'><img src="+CompLogo+" alt="+CompTitlt+"></div>";

                            str=str+ "<div class='company-desc'><div class='desc-col'><span> سطح توانگری مالی </span>";
                            for (let k=0;k<CompStar;k++){
                                str=str+" <span class='fa fa-star star'></span>";
                            }
                            str=str+ "</div><div class='desc-col'><span class='branchCount'>"+CompBranch+"</span><span> شعبه پرداخت خسارت </span></div></div>";


                            str=str+ "<div class='company-dur'>"+DurTitlt+"</div>" ;

                            str=str+ "<div class='company-money'>";
                            if (dis) {
                                str=str+"<div class='company-col discount'>"+FirstAmount+"</div>" ;
                            }
                            str=str+"<div class='company-col'>"+FinalAmount+"</div>" ;
                            str=str+ "<p class='rial'> ریال </p></div>";


                            str=str+ "<div class='pay-box'> <button class='btn'> خرید نقدی </button>" ;
                            if (HasInstallments !=null) {
                                str=str+ "<button class='btn aghsat'> اقساطی </button>" ;
                            }
                            str=str+ "</div>";

                            str=str+ "</div>";

                            str=str+ "</div>";

                            setTimeout(function () {
                                $(".result-list").append(str);
                            },200);

                        }
                    }
                }


            }
            catch (e) {
                console.log(e);
            }

        })();
    }

}
/* get model of selected brand */
function GetAutoModel(AutoBr) {

    const data = new FormData();
    data.append('_token', '{{csrf_token()}}');

    let address='api/insurance/insurance/car-body/car-model/'+AutoBr;

    let AutoModels=[];

    (async () =>{

        try {
            const response = await fn1(address, 'GET', data, {});
            const res = await response.json();
            AutoModels=res.Models;
        }
        catch (e) {
            console.log(e);
        }

        let element;
        element=$("#AutoModel-select");
        AddInput(element,AutoModels);

    })();

    Innerloading(1000);
}
/* for loading*/
function loading(time) {

    $(".loading-box").removeClass("dn");
    $(".loading-box .loading-sq").addClass("rotate-class");

    setTimeout(function () {
        $(".loading-box").addClass("dn");
        $(".loading-box .loading-sq").removeClass("rotate-class");
    },time);

}
/* for update cover checkbox*/
function ToggleCheck(th) {

    let vl=th.value;
    if (vl=="00") { th.value="1"; }
    else          { th.value="00"; }

    let par=$(th).parents(".ExtraCheck-box");

    PostNewCoverList(par);
}
/* for Post New CoverList*/
function PostNewCoverList(pr) {

    let temp=[];

    pr.find(".ExtraCheck-row .compare-input").each(function () {

        let cv=$(this).attr("value");
        let ci=$(this).attr("id");

        if (cv=="1")  { temp.push(parseInt(ci));}
    });

    let NameInObj=pr.attr("NameInObj");

    SendObj[NameInObj]=temp;
    let NewCount=CountEmptyBox();
    PostData(SendObj,NewCount);
}
