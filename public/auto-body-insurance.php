
<?php 
 include('layout-page/header.php');
?>

    <!-- this-filter session -->
    <div class="content">

  <section>
    <div class="search-section">
        <div class="aboutus">
            <span>بی می تو </span>
                 مقایسه، مشاوره و خرید آنلاین بیمه  
        </div>          
        <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-filter" >
                        <li class="nav-item lastitem">
                            <a class="nav-link " data-toggle="tab" href="#part3">
                                <i class="fas fa-user-shield icon fa-1x">
                                </i>
                                <span>  بیمه بدنه  </span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
    <div class="tab-pane active fade" id="part3">
        <form id="wizard3" class="wizard slider-form">          
            <div class="wizard-content">
                <div id="step1" class="wizard-step">
                    <div class="col-sm-10 col-xs-12 content-step">
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">             
                                                            <label class="slider-label"> نوع خودرو :  </label>
                                                             <select class="slider-input form-control " id="">                                       
                                                                    <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> مدل خودرو  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> سال ساخت خودرو : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> کاربری  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12  btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>                                                  
                </div>
                <div id="step2" class="wizard-step">
                    <div class="col-sm-2 col-xs-12  btns-step  prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step ">
                                                    <div class="col-xl-6 col-md-6 col-sm-12 tab-col">
                                                       <button class="btn slider-inner-btn btn-true">
                                                        <i class="fa fa-user"></i>
                                                            بیمه  بدنه دارم 
                                                       </button>
                                                    </div>   
                                                    <div class="col-xl-6 col-md-6 col-sm-12 tab-col">
                                                       <button class="btn slider-inner-btn btn-false">
                                                        <i class="fa fa-user"></i>
                                                         بیمه بدنه ندارم 
                                                       </button>
                                                    </div>       
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step3" class="wizard-step">
                    <div class="col-sm-2 col-xs-12  btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12  content-step">
                                                    <div class="col-md-6 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> تخفیف عدم خسارت  : </label>
                                                             <select class="slider-input form-control  " id="">                                       
                                                                    <option value=""> انتخاب </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label">  ارزش روز خودرو  : </label>
                                                            <input type="text" name="" placeholder="تومان " class="form-control slider-input">
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12  btns-step next-par reg-par">
                        <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                            <span> مقایسه  </span>
                        </button>
                    </div>   
                </div>
            </div>
            <div class="wizard-header">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">1</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">2</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">3</span>
                    </a></li>
                </ul>
                <div class="slider-line"></div>
            </div> 
        </form>
    </div>
                    </div>
   
    </div>
 </section>
</div>

    <!-- content session -->
    <section id="third-party-insurance-content">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#explain">

                                معرفی بیمه بدنه
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#question">
                                سوالات متداول
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#counciler">
                                درخواست مشاوره
                            </a>
                        </li>

                    </ul>
                    <!-- tab-content -->
                    <div class="tab-content shadow">
                        <!-- explanatin tab -->
                        <div class="tab-pane container active" id="explain">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="third-title">
                                            <h4>بیمه بدنه چیست؟</h4>
                                        </div>
                                        <div class="row ">
                                            <div class="col-md-12 third-content">
                                                <p class="paragraph">
                                                    بیمه بدنه یکی از بیمه‌‌های مربوط به خودرو است که در آن، خسارت‌‌های
                                                    وارد شده به اتومبیل جبران می‌شود. خرید این بیمه برخلاف بیمه شخص ثالث
                                                    اجباری نیست اما به دلیل مزایای فراوانی که پوشش‌های مختلف این بیمه
                                                    برای خودرو فراهم می‌کند، باعث شده است که خرید بیمه بدنه متقاضیان
                                                    بسیاری داشته باشد.
                                                </p>

                                                <div class="third-title">
                                                    <h4> بیمه بدنه چه مواردی را پوشش می‌دهد؟</h4>
                                                </div>
                                                <div class="third-content">
                                                    <p class="paragraph">
                                                        پوشش‌‌های بیمه بدنه به دو دسته پوشش‌‌های اصلی و پوشش‌‌های اضافی
                                                        تقسیم‌ می‌شوند.
                                                    </p>
                                                </div>


                                                <div class="third-title">
                                                    <h4>
                                                        پوشش‌‌های اصلی بیمه بدنه
                                                    </h4>
                                                </div>
                                                <div class="third-content">
                                                    <p class="paragraph">
                                                        منظور از پوشش‌‌های اصلی، خسارت‌هایی است که به صورت عادی و بدون
                                                        خرید پوشش اضافی توسط بیمه بدنه جبران‌ می‌شود. پوشش‌‌های اصلی
                                                        بیمه بدنه شامل موارد زیر است:
                                                    </p>
                                                    <ul>
                                                        <li>
                                                            هر گونه حادثه و تصادف با خودروی دیگر یا جسم ثابت و متحرک
                                                        </li>
                                                        <li>
                                                            سقوط، واژگونی، آتش سوزی، صاعقه و انفجار
                                                        </li>
                                                        <li>
                                                            سرقت کلی خودرو (مربوط به دزدیده شدن خودرو و آسیب های
                                                            وارد شده به وسایل اصلی خودرو یا وسایل اضافی درج شده در
                                                            بیمه نامه در حین سرقت)
                                                        </li>
                                                        <li>
                                                            خسارتی كه در جریان نجات و یا انتقال خودرو خسارت دیده به آن
                                                            وارد شود.
                                                        </li>
                                                        <li>
                                                            در صورت بروز خطرات اصلی، خسارت باطری و لاستیک های خودرو
                                                            نیز تا 50 درصد قیمت نو قابل پرداخت است.
                                                        </li>

                                                    </ul>


                                                    <h4 class="sub-title">
                                                        پوشش‌های اضافی بیمه بدنه
                                                    </h4>
                                                    <p>
                                                        در کنار پوشش‌های اصلی یک سری پوشش اضافی نیز توسط شرکت‌های بیمه
                                                        ارائه می‌شوند که خرید آن‌ها به انتخاب مشتری صورت می‌گیرد و برای
                                                        هرکدام مبلغ جداگانه‌ای به قیمت اصلی بیمه‌نامه اضافه خواهد شد.
                                                    </p>
                                                    <p>
                                                        این پوشش‌ها خسارت مواردی را جبران می‌کنند که در تعهد پوشش اصلی
                                                        بیمه بدنه نیست.
                                                    </p>
                                                    <p>
                                                        مهم‌ترین پوشش‌های اضافی بیمه بدنه عبارتند از:
                                                    </p>
                                                    <div class="paragraph">
                                                        <b>پوشش سرقت در جای قطعات:</b>
                                                        خسارت ناشی از دزدی وسایل و قطعات اتومبیل در حالت توقف، تحت پوشش
                                                        می باشد.

                                                        <br>

                                                        <b>پوشش ایاب و ذهاب:</b>
                                                        اگر خودرو به دلیل حادثه قابل استفاده نبوده و برای تعمیر در
                                                        تعمیرگاه متوقف باشد، روزانه مبلغی به
                                                        عنوان هزینه ایاب و ذهاب به بیمه گذار پرداخت می شود. در
                                                        این پوشش حداکثر مدت زمان پرداخت هزینه
                                                        <b>30</b>
                                                        روز می باشد.

                                                        <br>

                                                        <b>پوشش شکست شیشه:</b>
                                                        از طریق این پوشش خسارت ناشی از شکست شیشه به هر علت جبران خواهد
                                                        شد.

                                                        <br>

                                                        <b>پوشش نوسانات قیمت:</b>
                                                        در بیمه بدنه پرداخت خسارت بر اساس ارزش واقعی خودرو در زمان
                                                        خرید بیمه نامه انجام می شود. در
                                                        صورتی که ارزش خودروی شما در مدت بیمه افزایش پیدا کرده و از قیمت
                                                        درج شده در بیمه نامه بیشتر شده باشد، با استفاده
                                                        از
                                                        پوشش نوسانات قیمت می توانید خسارت را به ارزش روز
                                                        خودرو دریافت کنید.

                                                        <br>

                                                        <b>پوشش بلایای طبیعی:</b>
                                                        در این پوشش خسارت های ناشی از خطرات طبیعی مانند زمین
                                                        لرزه، سیل، آتشفشان توسط شرکت بیمه جبران
                                                        می شود.

                                                        <br>

                                                        <b> پوشش رنگ - مواد شیمیایی - اسید پاشی:</b>
                                                        با داشتن این پوشش، همه خسارت های ناشی از پاشیده شدن
                                                        مواد اسیدی و یا رنگ و مواد
                                                        شیمیایی بر روی بدنه خودرو تحت پوشش قرار می گیرد.

                                                        <br>

                                                        <b>پوشش ترانزیت (خارج از کشور):</b>
                                                        این پوشش مربوط به خطراتی است که خارج از مرزهای کشور به وسایل
                                                        نقلیه وارد می شود.

                                                        <br>

                                                        <b>پوشش حذف فرانشیز:</b>
                                                        همانطور که می دانید پرداخت بخشی از خسارت به
                                                        عهده بیمه گذار است که به آن فرانشیز می گویند.
                                                        با
                                                        خرید پوشش حذف فرانشیز، می توانید خسارت های به
                                                        وجود آمده را به صورت کامل از شرکت بیمه دریافت کنید.

                                                        <br>

                                                        <b>پوشش کشیدن میخ:</b>
                                                        در این پوشش خسارات ناشی از کشیدن میخ و ایجاد خط و خش بر روی
                                                        خودرو با اجسام تیز جبران می گردد. البته
                                                        باید توجه داشت که این پوشش فقط در بیمه بدنه ایران ارائه می شود.

                                                        <br>

                                                        <b>پوشش هزینه توقف خودروهای بارکش و اتوکار:</b>
                                                        این پوشش مختص به خودروهای بارکش و اتوکار است و توسط بعضی از شرکت
                                                        های
                                                        بیمه
                                                        ارائه می شود. در این پوشش در صورتی که خودرو به علت خطرات
                                                        تحت پوشش بیمه بدنه قابل استفاده نباشد و در تعمیرگاه متوقف
                                                        بماند، خسارتی به صورتی روزانه به آن پرداخت می شود.
                                                        میزان هزینه پرداختی بیمه به نوع خودرو و ظرفیت آن بستگی دارد.

                                                    </div>
                                                </div>

                                                <div class="third-title">
                                                    <h4>قیمت بیمه بدنه</h4>
                                                </div>
                                                <div class="third-content">
                                                    <p class="paragraph">
                                                        قیمت بیمه بدنه برخلاف بیمه شخص ثالث مبلغی نسبتا ثابت نیست و در
                                                        شرکت‌‌های مختلف بیمه تفاوت‌‌های بسیاری در نرخ بیمه بدنه شاهد
                                                        هستیم. دلیل این تفاوت‌ها این است که شرکت‌‌های بیمه برای اعمال
                                                        تخفیف روی قیمت پایه بیمه بدنه اختیار بیشتری دارند. </p>
                                                    <p class="paragraph">
                                                        نرخ پایه بیمه بدنه نیز به ارزش خودرو بستگی دارد و به صورت مبلغی
                                                        ثابت توسط بیمه مرکزی اعلام‌ نمی‌شود.
                                                    </p>
                                                    <p class="paragraph">
                                                        همچنین یکی دیگر از دلایل تفاوت قیمت بیمه بدنه این است که هزینه
                                                        هر کدام از پوشش‌‌های اضافی این بیمه‌ می‌تواند در شرکت‌‌های مختلف
                                                        بیمه متفاوت باشد.
                                                    </p>
                                                    <p class="paragraph">
                                                        اگر بخواهید برای استعلام قیمت بیمه بدنه به صورت حضوری اقدام
                                                        کنید، باید ساعت‌ها زمان صرف کنید و قیمت بیمه بدنه خودروی خود را
                                                        از شرکت‌‌های بیمه استعلام بگیرید. اما در حال حاضر این کار در عرض
                                                        چند دقیقه و با چند کلیک از طریق سایت بی می تو امکان پذیر است.
                                                    </p>

                                                </div>

                                                <div class="third-title">
                                                    <h4>محاسبه بیمه بدنه</h4>
                                                </div>
                                                <div class="third-content">
                                                    <p class="paragraph">
                                                        در محاسبه بیمه بدنه عوامل مختلفی تأثیرگذار هستند. عواملی مثل:

                                                        <br>

                                                        <b>پوشش های اضافی بیمه بدنه:</b>
                                                        همان طور که گفتیم به ازای انتخاب هر کدام از پوشش های اضافی،
                                                        مبلغی به بیمه نامه بدنه
                                                        شما افزوده خواهد شد. پس هرچه در زمان خرید بیمه بدنه پوشش های
                                                        اضافی بیشتری را انتخب کنید، قیمت بیمه بدنه شما بیشتر
                                                        می شود.

                                                        <br>

                                                        <b>ویژگی های خودرو:</b>
                                                        منظور از ویژگی های خودرو فاکتورهایی مثل ارزش خودرو، سال ساخت
                                                        خودرو، نوع خودرو و کاربری خودرو است که
                                                        هر کدام از این فاکتورها به صورت مستقیم بر قیمت بیمه بدنه
                                                        تأثیرگذار هستند.

                                                        <br>

                                                        <b>تخفیف های بیمه بدنه:</b>
                                                        تخفیف هایی مثل تخفیف عدم خسارت، تخفیف خودروی صفر کیلومتر، تخفیف
                                                        خرید نقدی، تخفیف ارزش خودرو و
                                                        ...
                                                    </p>
                                                </div>
                                                <div class="third-title">
                                                    <h4>تخفیف بیمه بدنه</h4>
                                                </div>
                                                <div class="third-content">
                                                    <div class="paragraph">
                                                        تخفیفات بیمه بدنه به دو دسته اصلی و فرعی تقسیم می شوند؛

                                                        <br>

                                                        تخفیفات اصلی شامل موارد زیر است:

                                                        <br>

                                                        <b>تخفیف خودرو صفر کیلومتر :</b>
                                                        تخفیف خودروی صفر کیلومتر بین
                                                        <b>۲۰ تا ۳۰</b>
                                                        درصد در نظر گرفته می شود.

                                                        <br>

                                                        <b>تخفیف عدم خسارت :</b>
                                                        این تخفیف به ازای تعداد سال هایی که از بیمه بدنه خود استفاده
                                                        نکرده باشید، به شما تعلق می گیرد. تخفیف عدم خسارت در
                                                        اکثر شرکت ها
                                                        <b>۴</b>
                                                        ساله و در برخی شرکت های بیمه
                                                        <b>۵</b>
                                                        ساله اعمال می شود. ترتیب تخفیف عدم خسارت بیمه بدنه در جدول زیر
                                                        قابل مشاهده است:

                                                        <br>
                                                        <br>

                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered car-table">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col">تخفیف بیمه بدنه</th>
                                                                    <th scope="col">سال اول</th>
                                                                    <th scope="col">سال دوم</th>
                                                                    <th scope="col">سال سوم</th>
                                                                    <th scope="col">سال چهارم</th>
                                                                    <th scope="col">سال پنجم</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td>اغلب شرکت های بیمه</td>
                                                                    <td>۲۵٪</td>
                                                                    <td>۳۵٪</td>
                                                                    <td>۴۵٪</td>
                                                                    <td>۶۰٪</td>
                                                                    <td >بیشتر شرکت های
                                                                        بیمه تخفیف را ۴ ساله اعمال می کنند.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>برخی شرکت های بیمه</td>
                                                                    <td>۳۰٪</td>
                                                                    <td>۴۰٪</td>
                                                                    <td>۵۰٪</td>
                                                                    <td>۶۰٪</td>
                                                                    <td>۷۰٪</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <br>

                                                        <b>تخفیف نقدی:</b>
                                                        این تخفیف مربوط به پرداخت نقدی حق بیمه است و در تمام شرکت ها 10
                                                        درصد در نظر گرفته می شود.

                                                        <br>

                                                        تخفیفات فرعی شامل موارد زیر است:

                                                        <ul>
                                                            <li>
                                                                تخفیف ارزش خودرو
                                                            </li>
                                                            <li>
                                                                تخفیفات گروهی
                                                            </li>
                                                            <li>
                                                                تخفیف خودروهای وارداتی
                                                            </li>
                                                            <li>
                                                                تخفیف برخی از مشاغل
                                                            </li>
                                                            <li>
                                                                طرح های تشویقی به مناسبت اعیاد و زمان های خاص و ...
                                                            </li>
                                                        </ul>

                                                        این گروه از تخفیفات نسبت به هر شرکت بیمه متفاوت است.
                                                    </div>
                                                </div>

                                                <div class="third-title">
                                                    <h4>بهترین بیمه بدنه</h4>
                                                </div>
                                                <div class="third-content">
                                                    <p class="paragraph">
                                                        همانطور که‌ می‌دانید تعداد زیادی از شرکت‌‌های بیمه به ارائه بیمه
                                                        بدنه‌ می‌پردازند. اما اگر قصد خرید این بیمه را دارید احتمالا این
                                                        سوال برایتان ایجاد شده که کدام شرکت، بیمه بهتری ارائه‌ می‌کند و
                                                        در واقع بهترین بیمه بدنه کدام است.
                                                    </p>
                                                    <p class="paragraph">
                                                        تعریف بهترین بیمه بدنه به شرایط و نیازهای هر فرد بستگی دارد.
                                                        مثلا برای یک نفر فقط ارزان بودن بیمه مهم است و برای شخص دیگر
                                                        پوشش‌‌های بیمه‌‌ای کامل و مناسب.
                                                    </p>
                                                    <p class="paragraph">
                                                        به طور کلی برای انتخاب بهترین بیمه بدنه باید معیارهای مختلفی را
                                                        در نظر داشته باشید. معیارهایی مثل: پوشش‌‌های کامل بیمه ای، نحوه
                                                        پرداخت خسارت، کیفیت ارائه خدمات، تخفیف‌‌های ارائه شده و ...
                                                    </p>
                                                    <p class="paragraph">
                                                        مشاوران متخصص بی می تو با ارائه مشاوره‌‌ای دقیق شما را در انتخاب
                                                        بهترین بیمه بدنه راهنمایی خواهند کرد.
                                                    </p>
                                                </div>
                                                <div class="third-title">
                                                    <h4>ارزان ترین بیمه بدنه</h4>
                                                </div>
                                                <div class="third-content">
                                                    <p class="paragraph">
                                                        یکی از معیارهایی که بسیاری از افراد در زمان خرید بیمه بدنه به آن
                                                        توجه می کنند، پایین بودن قیمت بیمه نامه است. اگر
                                                        شما هم به دنبال ارزان ترین بیمه بدنه هستید باید شرکت بیمه ای را
                                                        انتخاب کنید که تخفیف های بیشتری ارائه می دهد.

                                                        <br>

                                                        اطلاع از تمامی تخفیف های ارائه شده و درصد آن ها کار سختی نیست،
                                                        اگر از سایت بی می تو یا مشاوره کارشناسان بی می تو استفاده
                                                        کنید.

                                                        <br>

                                                        همچنین یکی از تخفیف های بیمه بدنه که توسط همه شرکت های بیمه
                                                        ارائه می شود، تخفیف خرید نقدی است. پس در صورت پرداخت
                                                        نقدی حق بیمه بدنه، می توانید بیمه نامه خود را 10 درصد ارزان تر
                                                        خریداری کنید.
                                                    </p>

                                                </div>
                                            </div>
                                            <div class="third-title">
                                                <h4>خسارت هایی که بیمه بدنه پوشش نمی دهد</h4>
                                            </div>
                                            <div class="third-content">
                                                <div class="paragraph">
                                                    خسارت هایی که طبق قوانین بیمه بدنه توسط شرکت های بیمه پوشش داده نمی
                                                    شوند شامل خسارت هایی است که در اثر ریسک های زیر
                                                    به وجود می آید:

                                                    <ul>
                                                        <li>
                                                            جنگ، شورش، حمله یا اعتصاب
                                                        </li>
                                                        <li>
                                                            خسارت های مستقیم یا غیر مستقیم به دلیل انفجارهای هسته ای
                                                        </li>
                                                        <li>
                                                            خسارت های عمدی توسط بیمه گذار، ذی نفع یا راننده خودرو
                                                        </li>
                                                        <li>
                                                            تعقیب و گریز از پلیس (مگر آنکه گریز از پلیس توسط فرد دیگری
                                                            به صورت غیرقانونی و بدون اجازه مالک خودرو انجام
                                                            شود)
                                                        </li>
                                                        <li>
                                                            رانندگی بدون گواهینامه / گواهینامه باطل شده / گواهینامه غیر
                                                            مرتبط با وسیله نقلیه (مثلا راندن اتومبیل با
                                                            گواهینامه موتور)
                                                        </li>

                                                        نکته: تمام شدن اعتبار گواهی نامه به معنای باطل شدن آن نیست.

                                                        <li>
                                                            مستی و استفاده از مشروبات الکلی، مواد روان گردان یا مخدرها
                                                        </li>
                                                        <li>
                                                            بکسل کردن وسیله نقلیه دیگر (به جز خودروهایی که مجاز به بکسل
                                                            کردن باشند)
                                                        </li>
                                                        <li>
                                                            حمل بار بیشتر از حد مجاز
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="third-title">
                                            <h4>مراحل دریافت خسارت بیمه بدنه خودرو</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                اگر در تصادفی شما مقصر حادثه باشید و یا خسارت واردشده به خودروی شما
                                                بیشتر از میزان سقف تعهد مالی بیمه شخص ثالث مقصر
                                                حادثه باشد، می توانید برای دریافت خسارت از بیمه بدنه خودروی خود اقدام
                                                کنید. مراحل دریافت خسارت بیمه بدنه خودرو به
                                                شرح زیر است:

                                                <ul>
                                                    <li>
                                                        بیمه گذار موظف است حداکثر
                                                        <b> 5 </b>
                                                        روز بعد از تاریخ حادثه، خسارتی که به وسیله نقلیه خود وارد شده
                                                        است را به شرکت بیمه اعلام کند.
                                                    </li>
                                                    <li>
                                                        بیمه گذار باید با مراجعه به یکی از مراکز نیروی انتظامی و یا
                                                        راهور اقدام به اخذ کروکی نماید.
                                                    </li>
                                                    <li>
                                                        زیان دیده موظف به مراجعه به شرکت های بیمه و انجام پیگیری های
                                                        لازم می باشد.
                                                    </li>
                                                    <li>
                                                        بیمه گذار باید خودروی خود را به یکی از تعمیرگاه های مجاز و یا
                                                        یکی از پارکینگ های مخصوص به شرکت بیمه منتقل
                                                        کند.
                                                    </li>
                                                    <li>
                                                        پس از انتقال خودرو به تعمیرگاه و یا پارکینگ شرکت بیمه، کارشناس
                                                        شرکت بیمه برای ارزیابی میزان خسارت از خودرو
                                                        بازدید می نماید.
                                                    </li>
                                                    <li>
                                                        مرحله بعدی بازسازی و تعمیر کامل خودرو می باشد.
                                                    </li>
                                                    <li>
                                                        پس تکمیل و اتمام تعمیرات، کارشناس بیمه گر برای اطمینان از سلامت
                                                        خودرو برای بار دوم از خودرو بازدید می کند.
                                                    </li>
                                                    <li>
                                                        چک خسارت توسط شرکت بیمه به مالک خودرو پرداخت می شود.
                                                    </li>
                                                    <li>
                                                        در نهایت بیمه گذار باید اقدام به ترمیم سرمایه اتومبیل خود نماید.
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                        <div class="third-title">
                                            <h4>وظایف و تعهدات ‌بیمه‌گذار در بیمه بدنه</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                <b>رعایت اصل حد اعلای حسن نیت:</b>
                                                دقت و صداقت در ارائه اطلاعاتی که در ارزیابی خطر مورد بیمه موثر هستند، از
                                                شروط اصلی و
                                                اساسی تنظیم قرارداد بیمه بدنه است. عدم رعایت این اصل موجب باطل شدن بیمه
                                                نامه می شود؛ در این صورت حق بیمه پرداخت شده،
                                                مسترد نخواهد شد.

                                                <br>

                                                <b>پرداخت حق بیمه بدنه:</b>
                                                در صورتی که پرداخت حق بیمه به صورت قسطی باشد و بیمه گذار یک یا چند قسط
                                                از اقساط حق بیمه
                                                را پرداخت نكند، بیمه گر می تواند بیمه نامه را فسخ نماید.

                                                <br>

                                                <b>اعلام تشدید خطر:</b>
                                                تغییر کاربری خودرو حتما باید به اطلاع بیمه گر برسد. زیرا تغییر کاربری
                                                ممکن است منجر به افزایش خطر و
                                                ریسک برای خودروی بیمه شده گردد. و در صورت عدم اطلاع رسانی، اگر حادثه به
                                                دلیل ریسک های تشدید شده باشد، تحت پوشش قرار
                                                نمی گیرد و بیمه گر صرفا خسارت خودرو را به نسبت حق بیمه اولیه پرداخت می
                                                نماید.

                                                <br>

                                                <b>اعلام نمودن خسارت:</b>
                                                بیمه گذار موظف است حداكثر ظرف مدت 5 روز كاری از تاریخ وقوع حادثه، حادثه
                                                را به بیمه گر اعلام
                                                نماید، و موظف است مدارک مورد نیاز و سایر اطلاعات مربوط به حادثه و تعیین
                                                میزان خسارت را در اختیار بیمه گر قرار دهد.
                                                در صورتی كه بیمه گذار مدارک مورد نیاز را ارائه نکند بیمه گر می تواند
                                                ادعای خسارت را رد نماید.

                                                <br>

                                                <b>عدم اظهارات غیر واقعی:</b>
                                                در صورتی که بیمه گذار به قصد تقلب، اطلاعات نادرستی در خصوص خسارت و كیفیت
                                                وقوع حادثه به شرکت
                                                بیمه ارائه دهد و یا مدارک جعلی عرضه كند، بیمه گر می تواند هیچ گونه
                                                خسارتی به بیمه گذار پرداخت ننماید.

                                                <br>

                                                <b>اجتناب از وقوع حادثه یا توسعه خسارت:</b>
                                                بیمه گذار باید تمام اقدامات و احتیاط های لازم را برای حفاظت مورد بیمه
                                                (خودرو)
                                                انجام دهد. اگر ثابت شود كه بیمه گذار عمدا از انجام این اقدامات خودداری
                                                کرده، بیمه گر می تواند خسارت کمتری پرداخت
                                                نماید.

                                                <br>

                                                <b>خودداری از جابه جایی خودرو:</b>
                                                در صورت بروز حادثه بیمه گذار باید از جابجایی خودرو خودداری نماید؛ مگر
                                                اینکه به حكم
                                                مقررات یا دستور مقامات انتظامی باشد و یا اینکه جابجایی برای نجات خودرو و
                                                کاهش خسارت صورت بگیرد.

                                                <br>

                                                <b>عدم تعمیر خودرو بدون هماهنگی با بیمه گر:</b>
                                                بیمه گذار نباید بدون هماهنگی با بیمه گر و پیش از نهایی شدن مراحل پرداخت
                                                خسارت، خودروی خود را تعمیر کند.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4>نحوه خرید بیمه بدنه از سایت بی می تو</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                برای خرید بیمه شخص ثالث از سایت بی می تو 4 مرحله پیش رو دارید:

                                                <ul>
                                                    <li>
                                                        مشخصات خودروی خود را در فیلدهای مشخص شده وارد کنید. مشخصاتی مثل:
                                                        نوع خودرو، مدل خودرو، سال ساخت خودرو، ارزش
                                                        خودرو و...
                                                    </li>
                                                    <li>
                                                        در مرحله بعد می توانید نرخ و شرایط بیمه بدنه خودروی خود را در
                                                        برترین شرکت های بیمه کشور مشاهده و با هم
                                                        مقایسه کنید و بر اساس نیازها و اولویت های خود یکی از بیمه ها را
                                                        انتخاب نمایید.
                                                    </li>
                                                    <li>
                                                        پس از تأیید اطلاعات در مرحله بعد باید اطلاعات شخصی مثل نام و نام
                                                        خانوادگی، کد ملی، آدرس و ... را وارد نمایید
                                                        و سپس عکس پشت و روی کارت خودرو و بیمه نامه قبلی خود را ارسال
                                                        کنید.
                                                    </li>
                                                    <li>
                                                        در مرحله نهایی می توانید نحوه پرداخت حق بیمه (پرداخت آنلاین یا
                                                        پرداخت در محل) را انتخاب کرده و هزینه بیمه
                                                        نامه خود را به یکی از این دو روش پرداخت کنید.
                                                    </li>
                                                    <li>
                                                        بیمه نامه شما در کمترین زمان ممکن صادر شده و به صورت رایگان به
                                                        آدرس مورد نظرتان ارسال خواهد شد.
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end explanatin tab -->

                        <!-- question tab-->
                        <div class="tab-pane container fade" id="question">
                            <div class="accordion" id="accordionExample">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                                    data-target="#question1" aria-expanded="true"
                                                    aria-controls="question1">
                                                بیمه بدنه چیست؟
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="question1" class="collapse show" aria-labelledby="headingOne"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                بیمه بدنه نوعی از بیمه خودرو است که خسارت‌های وارد شده به خودروی
                                                بیمه‌گذار را تحت پوشش قرار می‌دهد. خرید بیمه بدنه برای وسیله نقلیه
                                                اجباری نیست.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question2" aria-expanded="false"
                                                    aria-controls="question2">
                                                بیمه بدنه چه خطراتی را تحت پوشش قرار می‌دهد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question2" class="collapse" aria-labelledby="headingTwo"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p>خطرات
                                                    اصلی</p>
                                                <ul style="margin-left: 240px;">
                                                    <li>بر خورد دو خودرو</li>
                                                    <li>برخورد خودرو با یک جسم ثابت
                                                        یا متحرک
                                                    </li>
                                                    <li>برخورد اشیا به خودرو (خودرو
                                                        در حال حرکت یا ساکن)
                                                    </li>
                                                    <li>واژگون شدن خودرو</li>
                                                    <li>سقوط خودرو</li>
                                                </ul>
                                                <p>خطرات فرعی</p>
                                                <ul>
                                                    <li>بلایای طبیعی مانند سیل،
                                                        زلزله و آتش فشان
                                                    </li>
                                                    <li>استفاده از خودرو در مسابقه
                                                        اتومبیل رانی یا آزمایش سرعت
                                                    </li>
                                                    <li>حمل مواد منفجره، سریع
                                                        الاشتعال و یا اسیدی به خودرو وارد میشود (مگر آن که کاربری خودرو
                                                        حمل مواد اشتعال زا و … باشد)
                                                    </li>
                                                    <li>پاشیده شدن رنگ، اسید و سایر
                                                        مواد شیمیایی روی بدنه خودرو
                                                    </li>
                                                    <li>سرقت لوازم و قطعات خودرو که
                                                        به عنوان سرقت جزئی
                                                    </li>
                                                    <li>خسارت های ناشی از کاهش ارزش
                                                        خودرو
                                                    </li>
                                                    <li>پوشش هزینه توقف خودرو</li>
                                                    <li>پوشش ترانزیت</li>
                                                    <li>نوسانات بازار</li>
                                                    <li>شکسته شدن شیشه</li>
                                                    <li>پوشش حذف فرانشیز</li>
                                                    <li>پوشش هزینه ایاب و ذهاب</li>
                                                    <li>کشیدن میخ و سایر اشیاء
                                                        مشابه روی بدنه
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question3" aria-expanded="false"
                                                    aria-controls="question3">
                                                از نظر شرکت بیمه چه موقع خودرو به طور کامل از بین رفته محسوب می‌شود؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question3" class="collapse" aria-labelledby="headingThree"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <ul>
                                                <li>هنگامی که هزینه تعمیر و تعویض
                                                    قسمت های آسیب دیده از 75 درصد مبلغ بیمه شده بیشتر باشد.
                                                </li>
                                                <li>هنگامی که خودروی سرقت شده تا 60
                                                    روز پس از اعلام سرقت به مراجع قضایی و انتظامی پیدا نشده باشد.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFour">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question4" aria-expanded="false"
                                                    aria-controls="question4">

                                                آیا بیمه‌گذار می‌تواند بیمه‌نامه بدنه خودروی خود را به خودروی دیگری
                                                انتقال دهد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question4" class="collapse" aria-labelledby="headingFour"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                بله، بیمه‌گذار می‌تواند بیمه‌نامه را از هر خودرویی، به خودرو دیگر که به
                                                نام خود او و یا همسرش باشد، انتقال دهد و تخفیفات آن را نیز منتقل کند.
                                                ولی به خودروی دیگری که به نامش نیست قابل انتقال نمی‌باشد. </p></div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFive">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question5" aria-expanded="false"
                                                    aria-controls="question5">
                                                منظور از فرانشیز در بیمه بدنه چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question5" class="collapse" aria-labelledby="headingFive"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL">در کل منظور از فرانشیز بخشی از خسارت است
                                                    که به عهده بیمه گذار است. و در حوادث به شرح زیر است:</p>
                                                <ul>
                                                    <li>فرانشیز خسارت های جزیی
                                                        ناشی از حادثه و آتش سوزی جهت کلیه وسایط نقلیه در مورد خسارت
                                                        اول 10% مبلغ خسارت و در مورد خسارت دوم و خسارت های بعد 20%
                                                        مبلغ خسارت تعیین می گردد.
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li>در صورتی که سابقه رانندگی
                                                        راننده اتومبیل موضوع بیمه نامه بدنه در تاریخ حادثه کمتر از 3سال
                                                        باشد و یا سن راننده در تاریخ حادثه کمتر از 25 سال باشد، 10% به
                                                        فرانشیز های
                                                        اعلام شده فوق الاشاره اضافه میگردد.
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li>فرانشیز خسارت کلی ناشی
                                                        از حادثه و آتش سوزی 10% مبلغ خسارت تعیین می گردد<span dir="LTR">.</span>
                                                    </li>
                                                    <li>فرانشیز خسارت سرقت اعم
                                                        از جزیی و کلی 20% مبلغ خسارت تعیین می گردد<span
                                                                dir="LTR">.</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingSix">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question6" aria-expanded="false"
                                                    aria-controls="question6">

                                                منظور از ماده ده بیمه یا همان قاعده نسبی در بیمه بدنه چیست؟


                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question6" class="collapse" aria-labelledby="headingSix"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL">خودرو در زمان صدور بیمه نامه باید به
                                                    قیمت واقعی و ارزش روز بیمه شود. در غیر این صورت در زمان رسیدگی به
                                                    خسارت، مبلغ خسارت با توجه به نسبت سرمایه بیمه شده به قیمت واقعی روز
                                                    بازار در زمان صدور بیمه نامه پرداخت می شود و شامل اعمال قاعده
                                                    نسبی سرمایه می گردد<span dir="LTR">.</span></p>
                                                <p dir="RTL">قاعده نسبی، شرطی در بیمه نامه است که متقاضی بیمه را
                                                    از کم بیمه گی باز می دارد. نرخ حق بیمه را بیمه گر
                                                    معمولا، با این فرض تعیین می کند که مبلغ بیمه پیشنهاد شده
                                                    برای خودروی مورد نظر با ارزش واقعی آن خودرو برابر باشد. اما بیمه
                                                    گذاران،
                                                    معمولا مبلغ بیمه را کمتر از ارزش واقعی خودروی بیمه شده اعلام می
                                                    کنند.
                                                    در صورت نپرداختن حق بیمه متناسب با ارزش خودرو، ساز و کار بیمه درست
                                                    عمل نخواهد کرد.</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingSeven">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question7" aria-expanded="false"
                                                    aria-controls="question7">

                                                در خسارت‌های بیمه‌ای اختلاف بر سر میزان مبلغ خسارت چگونه حل و فصل میشود؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question7" class="collapse" aria-labelledby="headingSeven"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <ul>
                                                <li>معرفی به مراکز و نمایندگی های
                                                    مجاز
                                                </li>
                                                <li>در صورت عدم توافق مراجعه به
                                                    مراجع قضایی
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingEight">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question8" aria-expanded="false"
                                                    aria-controls="question8">

                                                مدت زمان لازم برای اعلام خسارت بیمه بدنه چه مدت است؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question8" class="collapse" aria-labelledby="headingEight"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                غیر از خسارت‌های جانی، زیان‌دیده حداکثر می‌تواند پس از پنج روز از وقوع
                                                حادثه بوسیله فکس یا تلفن به بیمه‌گر اطلاع دهد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingNine">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question9" aria-expanded="false"
                                                    aria-controls="question9">
                                                تخفیفات عدم خسارت در بیمه بدنه چگونه است؟
                                            </button>

                                        </h2>
                                    </div>
                                    <div id="question9" class="collapse" aria-labelledby="headingNine"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph">
                                                <ul>
                                                    <li>یکسال عدم خسارت ۲۵<span
                                                                dir="LTR">٪</span></li>
                                                    <li>دو سال عدم خسارت ۳۵<span
                                                                dir="LTR">٪</span></li>
                                                    <li>سه سال عدم خسارت ۴۵<span
                                                                dir="LTR">٪</span></li>
                                                    <li>چهار سال عدم خسارت و
                                                        بیشترشامل ۶۰<span dir="LTR">٪</span> تخفیف
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading11">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question11" aria-expanded="false"
                                                    aria-controls="question11">
                                                در صورت استفاده مکرر از بیمه‌نامه بدنه چه اتفاقی خواهد افتاد؟

                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question11" class="collapse" aria-labelledby="heading11"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                درصورت استفاده مکرر از بیمه‌نامه و عدم رعایت قوانین ضمن ابطال بیمه‌نامه
                                                از ارائه پوشش در کلیه شعب و نمایندگی‌های بیمه جلوگیری خواهد شد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading12">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question12" aria-expanded="false"
                                                    aria-controls="question12">
                                                حق بیمه بدنه به چه عواملی بستگی دارد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question12" class="collapse" aria-labelledby="heading12"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph">
                                                <ul>
                                                    <li>ارزش خودرو</li>
                                                    <li>حجم موتور (سیلندر)</li>
                                                    <li>شخصیت راننده (منظور رعایت
                                                        قوانین و خطرساز نبودن راننده است)
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading13">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question13" aria-expanded="false"
                                                    aria-controls="question13">
                                                در بیمه بدنه چه خطراتی تحت پوشش قرار نمی‌گیرد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question13" class="collapse" aria-labelledby="heading13"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph">
                                                <ul>
                                                    <li>جنگ، شورش، انقلاب، اعتصاب، تهاجم وخسارت های مستقیم و
                                                        غیرمستقیم ناشی از تشعشعات هسته ای
                                                    </li>
                                                    <li>زمین لرزه، سیل وآتشفشان</li>
                                                    <li>خسارت های ناشی از عمد بیمه گذار</li>
                                                    <li>نداشتن گواهینامه رانندگی متناسب با وسیله نقلیه</li>
                                                    <li>حادثه ناشی از شرب خمر و استعمال مواد مخدر</li>
                                                    <li>خسارت هایی که در جریان مسابقات اتومبیل رانی، شرط بندی،
                                                        آزمایش سرعت و یا آموزش رانندگی روی دهد
                                                    </li>
                                                    <li>خسارت هایی که متوجه کالاها یا محموله وسیله نقلیه شود</li>
                                                    <li>خسارت های ناشی از حمل مواد سریع الاشتعال</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading14">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question14" aria-expanded="false"
                                                    aria-controls="question14">
                                                در صورتی که مقصر حادثه مشخص نیست و یا خود بیمه‌گذار مقصر است چه باید
                                                کرد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question14" class="collapse" aria-labelledby="heading14"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                اولین کاری که بیمه‌گذار باید انجام دهد تماس با پلیس 110 جهت دریافت گزارش
                                                پلیس است. سپس مراجعه به مراجع حل اختلاف جهت تایید گزارش پلیس و سپس تهیه
                                                مدارک لازم خواهد بود.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading15">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question15" aria-expanded="false"
                                                    aria-controls="question15">
                                                مزایای بیمه بدنه چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question15" class="collapse" aria-labelledby="heading15"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph">
                                                <ul>
                                                    <li>شما می توانید با خرید
                                                        بیمه نامه بدنه، چنانچه مقصر حادثه باشید، خسارت وارده به
                                                        خودروی خود را جبران نمایید<span dir="LTR">.</span></li>
                                                    <li>شما می توانید حتی در
                                                        صورتی که مقصر حادثه نیستید و بیمه نامه شخص ثالث طرف حادثه
                                                        امکان پرداخت کل خسارت شما را ندارد، از بیمه نامه بدنه خود
                                                        برای دریافت مابقی خسارت استفاده نمایید<span dir="LTR">.</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading16">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question16" aria-expanded="false"
                                                    aria-controls="question16">
                                                آیا صدور بیمه‌نامه به صورت کوتاه مدت امکان پذیر می‌باشد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question16" class="collapse" aria-labelledby="heading16"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                خیر، متاسفانه صدور بیمه‌نامه بدنه کمتر از یکسال امکان پذیر نمی‌باشد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading17">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question17" aria-expanded="false"
                                                    aria-controls="question17">
                                                آیا در موارد تصادفی که بیمه بدنه شامل آن می‌باشد، کروکی افسر الزامی است؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question17" class="collapse" aria-labelledby="heading17"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                بله، حتما.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading18">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question18" aria-expanded="false"
                                                    aria-controls="question18">
                                                چنانچه بیمه بدنه سال قبل از بیمه دیگری باشد، اکنون می‌توان تخفیف عدم
                                                خسارت را انتقال داد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question18" class="collapse" aria-labelledby="heading18"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                بله، قطعا. برای این کار تنها کافیست از شرکت بیمه استعلام گرفته شود.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading19">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question19" aria-expanded="false"
                                                    aria-controls="question19">
                                                امکان صدرو بیمه بدنه برای خودروهای قدیمی وجود دارد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question19" class="collapse" aria-labelledby="heading19"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                خرید بیمه نامه برای خودروهایی که بیش از 20 سال از سال ساخت آنها می‌گذرد
                                                بدلیل فرسودگی بدنه امکان پذیر نیست؛ ضمن اینکه از خودروهایی که بیش از 10
                                                سال از سال ساخت آنها می‌گذرد بابت هر سال مازاد بر 10 سال 5% حق بیمه
                                                اضافی دریافت می‌شود.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading20">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question20" aria-expanded="false"
                                                    aria-controls="question20">
                                                در بیمه بدنه چه مبالغی از خسارت کسر می‌شود؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question20" class="collapse" aria-labelledby="heading20"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                فرانشیز و استهلاک </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading21">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question21" aria-expanded="false"
                                                    aria-controls="question21">
                                                در صورت فروش خودرو امکان فسخ بیمه‌نامه بدنه وجود دارد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question21" class="collapse" aria-labelledby="heading21"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                بله؛ از طرف بیمه‌گذار امکان فسخ بیمه‌نامه وجود دارد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading22">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question22" aria-expanded="false"
                                                    aria-controls="question22">
                                                اعلام تشدید خطر در بیمه‌نامه چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question22" class="collapse" aria-labelledby="heading22"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                یعنی اگر تغییر در کاربری خودرو ایجاد شود که ریسک موضوع بیمه را افزایش
                                                دهد مثلا خودرویی با کاربری شخصی بیمه شده و مالک آن تصمیم میگیرد از این
                                                به بعد در آژانس یا اسنپ یا ... مسافرکشی کند این مصداق تشدید خطر بوده و
                                                باید راننده موضوع را به شرکت بیمه اطلاع دهد که بدیهی است در صورت پذیرش
                                                موضوع از طرف شرکت بیمه، حق بیمه اضافی دریافت خواهد شد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading23">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question23" aria-expanded="false"
                                                    aria-controls="question23">
                                                مدارک لازم جهت دریافت خسارت بیمه بدنه چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question23" class="collapse" aria-labelledby="heading23"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph">
                                                <ul>
                                                    <li dir="RTL" style="text-align:right">اصل و کپی بيمه نامه بدنه
                                                        وسيله نقليه به همراه الحاقيه های&nbsp;صادره احتمالی
                                                    </li>
                                                    <li dir="RTL" style="text-align:right">اصل و کپی بيمه نامه شخص ثالث
                                                        وسيله نقليه
                                                    </li>
                                                    <li dir="RTL" style="text-align:right">ارائه گزارش مقامات انتظامی&nbsp;(کروکی)</li>
                                                    <li dir="RTL" style="text-align:right">اصل و کپی گواهينامه راننده
                                                    </li>
                                                    <li dir="RTL" style="text-align:right">اصل و کپی کارت ملی&nbsp;بيمه گذار
                                                        و راننده
                                                    </li>
                                                    <li dir="RTL" style="text-align:right">اصل و کپی شناسنامه مالکيت
                                                        خودرو و بنچاق
                                                    </li>
                                                    <li dir="RTL" style="text-align:right">اصل فاکتور هزينه های&nbsp;حمل
                                                        و نجات وسيله نقليه در صورت غير قابل حرکت بودن
                                                    </li>
                                                    <li dir="RTL" style="text-align:right">اصل فاکتور رسمی&nbsp;لوازم -
                                                        داغی&nbsp;قطعات تعويضی&nbsp;با نظر کارشناس ارزياب و رويت سلامت
                                                        وسيله نقليه
                                                    </li>
                                                </ul>
                                                <p dir="RTL">نکته مهم: امکان تغییر در مدارک مورد نیاز برای پرداخت خسارت،
                                                    بستگی به نوع خسارت و میزان آن و همچنین نظر کارشناسان بیمه خواهد
                                                    داشت.</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading24">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question24" aria-expanded="false"
                                                    aria-controls="question24">
                                                مراحل دریافت خسارت بیمه بدنه چگونه است؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question24" class="collapse" aria-labelledby="heading24"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph">
                                                <ul>
                                                    <li dir="RTL" style="text-align:right">مراجعه بيمه گذار / مالک /
                                                        نماينده قانونی&nbsp;به بخش پذيرش شرکت بیمه جهت اعلام خسارت و
                                                        تشکيل پرونده
                                                    </li>
                                                    <li dir="RTL" style="text-align:right">مراجعه به بخش ارزيابی&nbsp;خسارت
                                                        شرکت بیمه به همراه پرونده مربوطه جهت برآورد خسارت
                                                    </li>
                                                    <li dir="RTL" style="text-align:right">مراجعه به بخش پذيرش شرکت بیمه
                                                        جهت اقدامات بعدی&nbsp;و دريافت رسيد مربوطه
                                                    </li>
                                                    <li dir="RTL" style="text-align:right">اقدام جهت انجام تعميرات خودرو
                                                        توسط بيمه گذار
                                                    </li>
                                                    <li dir="RTL" style="text-align:right">دريافت پرونده از بايگانی&nbsp;و
                                                        مراجعه به بخش کارشناسی&nbsp;خسارت شرکت بیمه جهت رويت سلامت وسيله
                                                        نقليه و ارائه فاکتور و همچنین تحویل داغی&nbsp;لوازم در صورت نياز
                                                    </li>
                                                    <li dir="RTL" style="text-align:right">مراجعه به بخش محاسبه خسارت
                                                        شرکت بیمه جهت تعيين مبلغ نهايی&nbsp;خسارت قابل پرداخت
                                                    </li>
                                                    <li dir="RTL" style="text-align:right">مراجعه به بخش مالی شرکت بیمه گر
                                                        و دريافت خسارت
                                                    </li>
                                                    <li dir="RTL" style="text-align:right">در خصوص سرقت کلی خودرو دریافت
                                                        خسارت منوط به انتقال سند خودرو به شرکت بیمه گر می باشد.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading25">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question25" aria-expanded="false"
                                                    aria-controls="question25">
                                                مدارک لازم جهت دریافت خسارت بیمه بدنه‌ای که پوشش ترانزیت را خریداری
                                                نموده، کدام است؟

                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question25" class="collapse" aria-labelledby="heading25"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph">
                                                <ul>
                                                    <li>اصل و کپی بيمه نامه بدنه وسيله نقليه به همراه الحاقيه های
                                                        صادره احتمالی
                                                    </li>
                                                    <li>اصل و کپی بيمه نامه شخص ثالث وسيله نقليه</li>
                                                    <li>اصل و کپی گواهينامه راننده</li>
                                                    <li>اصل و کپی کارت ملی بيمه گذار و راننده</li>
                                                    <li>اصل و کپی شناسنامه مالکيت خودرو و بنچاق</li>
                                                    <li>اصل فاکتور هزينه های حمل و نجات وسيله نقليه در صورت غير قابل
                                                        حرکت بودن
                                                    </li>
                                                    <li>اصل فاکتور رسمی لوازم – داغی قطعات تعويضی با نظر کارشناس ارزياب
                                                        و رويت سلامت وسيله نقليه
                                                    </li>
                                                    <li>اصل و کپی الحاقيه مربوط به پوشش بيمه ای ترانزيت به همراه
                                                        شرايط خصوصی مربوطه
                                                    </li>
                                                    <li>برگ کاپيتاژ (خروج و ورود وسيله نقليه از پايانه های مرزی)
                                                    </li>
                                                    <li>ترجمه رسمی گزارشات پليس بين الملل (اينترپل)</li>
                                                    <li>تأیيد کنسولگری ايران در کشور محل حادثه وسيله نقليه</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading26">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question26" aria-expanded="false"
                                                    aria-controls="question26">
                                                امکان پرداخت خسارت بدنه بدون کروکی وجود دارد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question26" class="collapse" aria-labelledby="heading26"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                بله؛ بستگی به سقف آن دارد و فقط یک بار در طول مدت بیمه‌نامه.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading27">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question27" aria-expanded="false"
                                                    aria-controls="question27">
                                                بازدید سلامت خودرو به چه صورت انجام می‌شود؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question27" class="collapse" aria-labelledby="heading27"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                بازدید سلامت خودرو پس از هر بار دریافت خسارت برای ادامه اعتبار بیمه‌نامه
                                                الزامیست.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading28">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question28" aria-expanded="false"
                                                    aria-controls="question28">
                                                هدف از بازدید اولیه در بیمه بدنه چیست؟

                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question28" class="collapse" aria-labelledby="heading28"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                برای تعیین هویت و اصالت خودرو، تایید سلامت خودرو و ارزش گذاری خودرو
                                                می‌باشد. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading29">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question29" aria-expanded="false"
                                                    aria-controls="question29">
                                                آیا تخفیفات بیمه بدنه‌ای که گم شده باشد برای تمدید آن اعمال می‌گردد؟

                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question29" class="collapse" aria-labelledby="heading29"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                بله؛ با استعلام از آن شرکت بیمه و همچنین در صورتی که مالک خودرو عوض نشده
                                                باشد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading30">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question30" aria-expanded="false"
                                                    aria-controls="question30">
                                                در تصادف هایی که به صورت ۵۰/۵۰ رخ داده است خسارت بیمه بدنه چگونه اعمال
                                                می‌شود؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question30" class="collapse" aria-labelledby="heading30"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                فقط ۵۰٪ خسارت را بیمه بدنه جبران می‌کند. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading31">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question31" aria-expanded="false"
                                                    aria-controls="question31">
                                                آیا می‌توان بیمه بدنه را بازخرید کرد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question31" class="collapse" aria-labelledby="heading31"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                بله؛ ولی حق بیمه برگشتی به صورت کوتاه مدت حساب می‌شود. یعنی میزان حق
                                                بیمه برگشتی کمتر از روزشمار خواهد بود.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading32">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question32" aria-expanded="false"
                                                    aria-controls="question32">
                                                آیا در بیمه بدنه سند باید به نام باشد؟ قول‌نامه قابل قبول است؟
                                        </h2>
                                    </div>
                                    <div id="question32" class="collapse" aria-labelledby="heading32"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                بله؛ سند باید به نام باشد. قول‌نامه قابل قبول نیست. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading33">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question33" aria-expanded="false"
                                                    aria-controls="question33">
                                                اگر پس از دریافت خسارت بیمه بدنه خودروی سرقتی، خودرو پیدا شود، چه اتفاقی
                                                میفتد؟
                                    </div>
                                    <div id="question33" class="collapse" aria-labelledby="heading33"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                در صورتی‌که خودروی سرقت شده بعد از دریافت خسارت از شرکت بیمه پیدا شود،
                                                خودرو متعلق به شرکت بیمه یا همان بیمه‌گر است. شرکت بیمه بعد از فروش
                                                خودرو درصدی از ارزش خودرو که به دلیل فرانشیز، پرداخت آن بر عهده
                                                بیمه‌گذار بوده است را به وی باز می‌گرداند.
                                            </p>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <!-- end question tab-->

                        <!-- counciler tab-->
                        <div class="tab-pane container fade" id="counciler">


                            <section class="form-elegant">

                                <div class="third-title">
                                    <h4>درخواست مشاوره</h4>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">


                                        <form>
                                            <div class="form-group col-md-8">
                                                <label for="nameInput">نام و نام خانوادگی</label>
                                                <input type="text" class="form-control none-border" id="nameInput"
                                                       placeholder="">
                                            </div>

                                            <div class="form-group col-md-8">
                                                <label for="phoneInput">شماره تماس </label>
                                                <input type="text" class="form-control none-border" id="phoneInput"
                                                       placeholder="">
                                            </div>
                                            <div class="form-group col-md-8">
                                                <label for="description">متن درخواست مشاوره</label>
                                                <textarea id="description" class="form-control none-border"
                                                          style="float: right;">

                                            </textarea>
                                            </div>

                                            <div class="col-md-8  btn ">
                                                <div class="compare-btn btn ">
                                                    <a href="#">ارسال درخواست</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </section>

                        </div>
                        <!--  end counciler tab -->
                    </div>
                    <!--  end tab-content  -->
                </div>
            </div>
        </div>
        </div>
        </div>
        </div>
    </section>


<?php

 include('layout-page/footer.php');

 ?>
<script type="text/javascript">

        $(".top-nav-menu .nav-item").removeClass("active");
        $(".top-nav-menu #m3").addClass("active");
        
</script>


</body>

</html>
