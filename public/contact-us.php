<?php 

 include('layout/header.php');

?>

    <section style="margin-bottom: 30px;margin-top: 122px;" id="contact-us">
        <div class="container">
            <div class="row ">
                <div class="col-md-12 shadow ">
                    <div class="row" style="margin-right:37%; ">
                        <div class="third-title  col-md-4">
                            <h3>
                                تماس با بی می تو
                            </h3>
                        </div>
                    </div>

                    <div class="third-content" style="margin-top: 20px;padding: 1px 14px;">
                        <p class="paragraph">
                            مشتری گرامی برای پیگیری یا سؤال درباره سفارش خود، از فرم زیر استفاده کنید و یا به آدرس ایمیل
                            زیر ارسال کنید. برای تسریع در پاسخ گویی شماره سفارش (پیگیری) را در ایمیل خود ذکر کنید.
                        </p>
                        <div class="address col-md-">
                            <p class="paragraph">
                                آدرس : تهران
                            </p>
                            <p class="paragraph">
                                پست الکترونیکی :info@bimitoo.com
                            </p>
                            <p class="paragraph">
                                شماره تماس :
                                <span> ۲۲۱۲۰۱۱۴ - ۰۲۱  </span>
                            </p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row" style="margin-right:37%; ">
                                <div class="third-title  col-md-4" style="    padding-right: 34px;">
                                    <h4>
                                        فرم تماس با ما
                                    </h4>
                                </div>
                            </div>
                            <form style="margin-top: 20px;padding: 1px 14px;">
                                <div class="form-group col-md-5">
                                    <label for="nameInput">نام </label>
                                    <input type="text" class="form-control none-border" id="nameInput" placeholder=""
                                           required>
                                </div>
                                <div class="form-group col-md-5">
                                    <label for="lastInput">نام خانوادگی</label>
                                    <input type="text" class="form-control none-border" id="lastInput" placeholder=""
                                           required>
                                </div>

                                <div class="form-group col-md-5">
                                    <label for="phoneInput">شماره تماس </label>
                                    <input type="phone" class="form-control none-border" id="phoneInput" placeholder=""
                                           required>
                                </div>
                                <div class="form-group col-md-5">
                                    <label for="emailInput">ایمیل </label>
                                    <input type="email" class="form-control none-border" id="emailInput" placeholder=""
                                           required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="description">متن پیام </label>
                                    <textarea id="description" class="form-control none-border" style="float: right;"
                                              required></textarea>
                                </div>

                                <div class="col-md-10  btn " style="margin-bottom: 25px">
                                    <div class="compare-btn btn ">
                                        <a href="#">ثبت و ارسال فرم</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

<?php

 include('layout/footer.php');

 ?>

</body>

</html>
