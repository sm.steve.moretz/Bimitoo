@include('layouts.header')

<div class="content">

    <section>
        <div class="search-section">
            <div class="aboutus">
                <span>بی می تو </span>
                مقایسه، مشاوره و خرید آنلاین بیمه
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-filter" >
                <li class="nav-item lastitem">
                    <a class="nav-link " data-toggle="tab" href="#part6">
                        <i class="fas fa-clinic-medical icon fa-1x">
                        </i>
                        <span> بیمه  درمان تکمیلی  </span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active fade" id="part6">

                    <form id="wizard6" class="wizard slider-form" >
                        <div class="wizard-content">
                            <div  id="step1" class="wizard-step ">
                                <div class="col-sm-10 col-xs-12 content-step">
                                    <div class="col-lg-6 col-md-12 tab-col ">
                                        <div class="form-group">
                                            <label class="slider-label"> تاریخ تولد  : </label>
                                            <input type="date" class="slider-input form-control " id="HealthDate-input" value=" ">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 tab-col ">
                                        <div class="form-group">
                                            <label class="slider-label"> بیمه گر پایه  : </label>
                                            <select class="slider-input form-control " id="HealthBasicInsurers-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                                    <button type="submit" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                                        <span> مقایسه  </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-header">
                            <ul class="nav nav-tabs">
                            </ul>
                            <div class="slider-line"></div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </section>


</div>

<section id="health-insurance-content" class="insurans-box-log">
    <div class="insur-box">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#explain">

                                معرفی بیمه درمان تکمیلی
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#question">
                                سوالات متداول
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#counciler">
                                درخواست مشاوره
                            </a>
                        </li>

                    </ul>
                    <!-- tab-content -->
                    <div class="tab-content shadow">
                        <!-- explanatin tab -->
                        <div class="tab-pane container active" id="explain">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="third-title">
                                            <h4> هر آنچه درباره‌ی بیمه تکمیلی انفرادی سامان باید بدانید:</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                در حال حاضر تنها شرکت بیمه ای که به صورت مستقیم بیمه تکمیلی انفرادی
                                                ارائه می کند، شرکت بیمه سامان می باشد.

                                                <br>

                                                بیمه تکمیلی انفرادی سامان خدمات خود را در قالب 6 طرح ارائه کرده است که
                                                پوشش ها و سقف تعهدات هرکدام از آنها در مدت
                                                یک سال در جدول شماره یک، و شرح هریک از تعهدات در جدول شماره دو، بیان شده
                                                است.

                                                <br>

                                                همچنین توضیحات مربوط به خرید بیمه تکمیلی انفرادی سامان بدون داشتن بیمه
                                                گر پایه، دوره انتظار، فرانشیز، شرایط سنی،
                                                شرایط بیمه تکمیلی خانوادگی، روش های پرداخت حق بیمه تکمیلی و فرآیند
                                                دریافت هزینه های پزشکی از بیمه سامان را
                                                می توانید در ادامه ی جدول ها مطالعه کنید.

                                                <br>

                                                <h4 class="sub-title">جدول شماره یک: سقف تعهدات به تفکیک طرح ها در یک
                                                    سال (مبلغ به تومان)</h4>

                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered car-table">
                                                        <thead>
                                                        <tr>
                                                            <th>
                                                                تعهدات
                                                            </th>
                                                            <th>
                                                                طرح نسیم سامان
                                                            </th>
                                                            <th>
                                                                طرح مهر سامان
                                                            </th>
                                                            <th>
                                                                طرح سروش سامان
                                                            </th>
                                                            <th>
                                                                طرح شمیم سامان
                                                            </th>
                                                            <th>
                                                                طرح وصال سامان
                                                            </th>
                                                            <th>
                                                                طرح عقیق سامان
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                بیمارستانی
                                                            </td>
                                                            <td>
                                                                ۱۵٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۳٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۵٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۸٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۱۵٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۳۷٫۵۰۰٫۰۰۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                عمل های جراحی
                                                            </td>
                                                            <td>
                                                                ۳۰٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۶٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۱۰٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۱۶٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۳۰٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۷۵٫۰۰۰٫۰۰۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                پاراکلینیکی گروه ۱
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                ۳۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۵۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۸۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۱٫۵۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۲٫۰۰۰٫۰۰۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                پاراکلینیکی گروه 2
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                ۱۵۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۲۵۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۴۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۷۵۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۱٫۰۰۰٫۰۰۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                جراحی های مجاز سرپایی
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                ۱۵۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۲۵۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۵۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۱٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۲٫۰۰۰٫۰۰۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                خدمات آزمایشگاهی
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                ۱۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۲۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۵۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۷۵۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۱٫۰۰۰٫۰۰۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                هزینه ها ی آمبولانس شهری و بین شهری
                                                            </td>
                                                            <td>
                                                                ۱۵۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۲۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۳۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۴۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۵۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۵۰۰٫۰۰۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                زایمان
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                ۱٫۵۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۲٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۳٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۵٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۵٫۰۰۰٫۰۰۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                ویزیت و دارو
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                ۱۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۲۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۵۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۸۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۱٫۰۰۰٫۰۰۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                دندانپزشکی
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                ۱۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۲۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۵۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۱٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۱٫۰۰۰٫۰۰۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                نازایی
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                ۱٫۵۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۲٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۳٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۵٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۵٫۰۰۰٫۰۰۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                رفع عیوب انکساری دو چشم
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                ۶۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۱٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۱٫۶۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۲٫۰۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۳٫۰۰۰٫۰۰۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                سمعک
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                ۱۵۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۲۵۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۴۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۶۰۰٫۰۰۰
                                                            </td>
                                                            <td>
                                                                ۱٫۰۰۰٫۰۰۰
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <br>

                                                <h4 class="sub-title">جدول شماره دو: شرح هریک از تعهدات جدول شماره
                                                    یک</h4>
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered car-table">
                                                        <thead>
                                                        <tr>
                                                            <th width="16%">
                                                                تعهدات
                                                            </th>
                                                            <th width="83%">
                                                                شرح
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                بیمارستانی
                                                            </td>
                                                            <td>
                                                                جبران هزینه های بستری، جراحی، شیمی درمانی، رادیوتراپی،
                                                                آنژیوگرافی قلب، گامانایف و انواع سنگ شکن در
                                                                بیمارستان و مراکز جراحی محدود Daycare.

                                                                <br>

                                                                تبصره: عمل های جراحی Daycare، به جراحی هایی اطلاق می شود
                                                                که مدت زمان مورد نیاز برای مراقبت بعد از
                                                                عمل در
                                                                مراکز درمانی، کمتر از یک روز باشد.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                عمل های جراحی
                                                            </td>
                                                            <td>
                                                                عمل های جراحی مهم مربوط به سرطان، مغز و اعصاب مرکزی و
                                                                نخاع (به استثنای دیسک ستون فقرات)، گامانایف،
                                                                قلب،
                                                                پیوند ریه، کبد، مغز استخوان
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                پاراکلینیکی گروه ۱
                                                            </td>
                                                            <td>
                                                                جبران هزینه های سونوگرافی، ماموگرافی، انواع اسکن، انواع
                                                                آندوسکوپی، ام آر آی، اکوکاردیوگرافی، استرس
                                                                اکو،
                                                                دانستیومتری
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                پاراکلینیکی گروه ۲
                                                            </td>
                                                            <td>
                                                                جبران هزینه های مربوط به تست ورزش، تست آلرژی، تست تنفسی
                                                                اسپیرومتری (PFT)، نوار عضله (EMG)، نوار عصب
                                                                (NCV)، نوار مغز (EEG)، نوار مثانه (سیستومتری،
                                                                یاسیستوگرام)، شنوایی سنجی، بینایی سنجی ،
                                                                هولترمانیتورینگ
                                                                قلب، آنژیوگرافی چشم
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                جراحی های مجاز سرپایی
                                                            </td>
                                                            <td>
                                                                شکستگی و در رفتگی، گچ گیری، ختنه، بخیه و کشیدن بخیه،
                                                                کرایوتراپی، اکسیزیون لیپوم، بیوپسی، تخلیه کیست
                                                                و
                                                                لیزردرمانی
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                خدمات آزمایشگاهی
                                                            </td>
                                                            <td>
                                                                آزمایش های تشخیص پزشکی، پاتولوژی، آسیب شناسی و ژنتیک
                                                                پزشکی، انواع رادیوگرافی و نوار قلب و فیزیوتراپی
                                                                (به
                                                                استثنا چکاپ)
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                آمبولانس شهری و بین شهری
                                                            </td>
                                                            <td>
                                                                هزینه های آمبولانس شهری و بین شهری
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                زایمان
                                                            </td>
                                                            <td>
                                                                هزینه های زایمان اعم از طبیعی و سزارین
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                ویزیت و دارو
                                                            </td>
                                                            <td>
                                                                هزینه ویزیت و دارو
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                دندانپزشکی
                                                            </td>
                                                            <td>
                                                                هزینه های مربوط به دندانپزشکی (به استثنای ایمپلنت و
                                                                ارتودنسی و دندان مصنوعی و جراحی لثه)
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                نازایی
                                                            </td>
                                                            <td>
                                                                درمان های مربوط به نازایی
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                رفع عیوب انکساری دو چشم
                                                            </td>
                                                            <td>
                                                                هزینه ها ی مربوط به رفع عیوب انکساری دو چشم
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                سمعک
                                                            </td>
                                                            <td>
                                                                هزینه های مربوط به خرید سمعک
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="third-title">
                                            <h4>مبلغ حق‌بیمه تکمیلی انفرادی سامان</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                مبلغ حق بیمه هریک از این طرح ها به صورت سالیانه و با توجه به سن بیمه شده
                                                تعیین می شود که طبق جدول زیر می باشد.
                                                <br>
                                                <h4 class="sub-title">جدول حق بیمه سالیانه بر اساس سن بیمه شده (مبلغ به
                                                    تومان)</h4>
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered car-table">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: center">
                                                                سن
                                                            </th>
                                                            <th>
                                                                طرح نسیم سامان
                                                            </th>
                                                            <th>
                                                                طرح مهر سامان
                                                            </th>
                                                            <th>
                                                                طرح سروش سامان
                                                            </th>
                                                            <th>
                                                                طرح شمیم سامان
                                                            </th>
                                                            <th>
                                                                طرح وصال سامان
                                                            </th>
                                                            <th>
                                                                طرح عقیق سامان
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                ۰ تا ۱۵ سال
                                                            </td>
                                                            <td>
                                                                ۹۵,۰۰۰
                                                            </td>
                                                            <td>
                                                                ۳۴۸,۴۰۰
                                                            </td>
                                                            <td>
                                                                ۵۴۸,۶۰۰
                                                            </td>
                                                            <td>
                                                                ۷۶۹,۶۰۰
                                                            </td>
                                                            <td>
                                                                ۹۷۱,۱۰۰
                                                            </td>
                                                            <td>
                                                                ۱,۱۲۷,۱۰۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                ۱۶ تا ۵۰ سال
                                                            </td>
                                                            <td>
                                                                ۱۹۵,۰۰۰
                                                            </td>
                                                            <td>
                                                                ۶۹۶,۸۰۰
                                                            </td>
                                                            <td>
                                                                ۱,۰۹۷,۲۰۰
                                                            </td>
                                                            <td>
                                                                ۱,۵۳۹,۲۰۰
                                                            </td>
                                                            <td>
                                                                ۱,۹۴۲,۲۰۰
                                                            </td>
                                                            <td>
                                                                ۲,۲۵۴,۲۰۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                ۵۱ تا ۶۰ سال
                                                            </td>
                                                            <td>
                                                                ۱۹۵,۰۰۰
                                                            </td>
                                                            <td>
                                                                ۸۳۶,۱۶۰
                                                            </td>
                                                            <td>
                                                                ۱,۳۱۶,۶۴۰
                                                            </td>
                                                            <td>
                                                                ۱,۸۴۷,۰۴۰
                                                            </td>
                                                            <td>
                                                                ۲,۳۳۰,۶۴۰
                                                            </td>
                                                            <td>
                                                                ۲,۷۰۵,۰۴۰
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                ۶۱ تا ۷۰سال
                                                            </td>
                                                            <td>
                                                                ۱۹۵,۰۰۰
                                                            </td>
                                                            <td>
                                                                ۱,۰۴۵,۲۰۰
                                                            </td>
                                                            <td>
                                                                ۱,۶۴۶,۸۰۰
                                                            </td>
                                                            <td>
                                                                ۲,۳۰۸,۸۰۰
                                                            </td>
                                                            <td>
                                                                ۲,۹۱۳,۳۰۰
                                                            </td>
                                                            <td>
                                                                ۳,۳۸۱,۳۰۰
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>


                                        <div class="third-title">
                                            <h4>تخفیف بیمه تکمیلی سامان</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                متقاضیان خرید بیمه تکمیلی انفرادی یا خانوادگی سامان میتوانند از تخفیف
                                                های ویژه این بیمه استفاده کنند. به این صورت که با خرید نقدی بیمه تکمیلی
                                                سامان مشمول ۱۰ درصد تخفیف می شوید و یا در صورت داشتن بیمه عمر سامان، ۱۰
                                                درصد تخفیف به بیمه نامه شما تعلق می گیرد. همچنین خانواده های سه نفره و
                                                سه نفر به بالا که متقاضی خرید بیمه تکمیلی سامان هستند، می توانند این
                                                بیمه را با 15 درصد تخفیف خریداری کنند. البته سقف تخفیف های این بیمه 25
                                                درصد است. یعنی شما نمی توانید به صورت همزمان از تخفیف دارا بودن بیمه عمر
                                                و تخفیف خرید نقدی بهره مند شوید و یکی از این دو تخفیف به شما تعلق خواهد
                                                گرفت. </p>
                                            <h4 class="sub-title">جدول حق بیمه در بیمه تکمیلی سامان با احتساب تخفیف خرید
                                                نقدی (۱۰ درصد) (قیمت ها به
                                                تومان)</h4>
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered car-table">
                                                    <thead>
                                                    <tr>
                                                        <th style="text-align: center">
                                                            سن
                                                        </th>
                                                        <th>
                                                            طرح نسیم سامان
                                                        </th>
                                                        <th>
                                                            طرح مهر سامان
                                                        </th>
                                                        <th>
                                                            طرح سروش سامان
                                                        </th>
                                                        <th>
                                                            طرح شمیم سامان
                                                        </th>
                                                        <th>
                                                            طرح وصال سامان
                                                        </th>
                                                        <th>
                                                            طرح عقیق سامان
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            ۰ تا ۱۵ سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۳۱۳,۵۶۰
                                                        </td>
                                                        <td>
                                                            ۴۹۳,۷۴۰
                                                        </td>
                                                        <td>
                                                            ۶۹۲,۶۴۰
                                                        </td>
                                                        <td>
                                                            ۸۷۳,۹۹۰
                                                        </td>
                                                        <td>
                                                            ۱,۰۱۴,۳۹۰
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ۱۶ تا ۵۰ سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۶۲۷,۱۲۰
                                                        </td>
                                                        <td>
                                                            ۹۸۷,۴۸۰
                                                        </td>
                                                        <td>
                                                            ۱,۳۸۵,۲۸۰
                                                        </td>
                                                        <td>
                                                            ۱,۷۴۷,۹۸۰
                                                        </td>
                                                        <td>
                                                            ۲,۰۲۸,۷۸۰
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ۵۱ تا ۶۰ سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۷۵۲,۵۴۴
                                                        </td>
                                                        <td>
                                                            ۱,۱۸۴,۹۷۶
                                                        </td>
                                                        <td>
                                                            ۱,۶۶۲,۳۳۶
                                                        </td>
                                                        <td>
                                                            ۲,۰۹۷,۵۷۶
                                                        </td>
                                                        <td>
                                                            ۲,۴۳۴,۵۳۶
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ۶۱ تا ۷۰سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۹۴۰,۶۸۰
                                                        </td>
                                                        <td>
                                                            ۱,۴۸۱,۲۲۰
                                                        </td>
                                                        <td>
                                                            ۲,۰۷۷,۹۲۰
                                                        </td>
                                                        <td>
                                                            ۲,۶۲۱,۹۷۰
                                                        </td>
                                                        <td>
                                                            ۳,۰۴۳,۱۷۰
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <h4 class="sub-title">
                                                جدول حق بیمه در بیمه تکمیلی سامان با احتساب تخفیف دارا بودن بیمه عمر
                                                سامان (۱۰ درصد) (قیمت ها به تومان)
                                            </h4>
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered car-table">
                                                    <thead>
                                                    <tr>
                                                        <th style="text-align: center">
                                                            سن
                                                        </th>
                                                        <th>
                                                            طرح نسیم سامان
                                                        </th>
                                                        <th>
                                                            طرح مهر سامان
                                                        </th>
                                                        <th>
                                                            طرح سروش سامان
                                                        </th>
                                                        <th>
                                                            طرح شمیم سامان
                                                        </th>
                                                        <th>
                                                            طرح وصال سامان
                                                        </th>
                                                        <th>
                                                            طرح عقیق سامان
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            ۰ تا ۱۵ سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۳۱۳,۵۶۰
                                                        </td>
                                                        <td>
                                                            ۴۹۳,۷۴۰
                                                        </td>
                                                        <td>
                                                            ۶۹۲,۶۴۰
                                                        </td>
                                                        <td>
                                                            ۸۷۳,۹۹۰
                                                        </td>
                                                        <td>
                                                            ۱,۰۱۴,۳۹۰
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ۱۶ تا ۵۰ سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۶۲۷,۱۲۰
                                                        </td>
                                                        <td>
                                                            ۹۸۷,۴۸۰
                                                        </td>
                                                        <td>
                                                            ۱,۳۸۵,۲۸۰
                                                        </td>
                                                        <td>
                                                            ۱,۷۴۷,۹۸۰
                                                        </td>
                                                        <td>
                                                            ۲,۰۲۸,۷۸۰
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ۵۱ تا ۶۰ سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۷۵۲,۵۴۴
                                                        </td>
                                                        <td>
                                                            ۱,۱۸۴,۹۷۶
                                                        </td>
                                                        <td>
                                                            ۱,۶۶۲,۳۳۶
                                                        </td>
                                                        <td>
                                                            ۲,۰۹۷,۵۷۶
                                                        </td>
                                                        <td>
                                                            ۲,۴۳۴,۵۳۶
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ۶۱ تا ۷۰سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۹۴۰,۶۸۰
                                                        </td>
                                                        <td>
                                                            ۱,۴۸۱,۲۲۰
                                                        </td>
                                                        <td>
                                                            ۲,۰۷۷,۹۲۰
                                                        </td>
                                                        <td>
                                                            ۲,۶۲۱,۹۷۰
                                                        </td>
                                                        <td>
                                                            ۳,۰۴۳,۱۷۰
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <h4 class="sub-title">
                                                جدول حق بیمه در بیمه تکمیلی سامان با احتساب تخفیف خانواده های سه نفر به
                                                بالا (۱۵ درصد) (قیمت ها به تومان)
                                            </h4>
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered car-table">
                                                    <thead>
                                                    <tr>
                                                        <th style="text-align: center">
                                                            سن
                                                        </th>
                                                        <th>
                                                            طرح نسیم سامان
                                                        </th>
                                                        <th>
                                                            طرح مهر سامان
                                                        </th>
                                                        <th>
                                                            طرح سروش سامان
                                                        </th>
                                                        <th>
                                                            طرح شمیم سامان
                                                        </th>
                                                        <th>
                                                            طرح وصال سامان
                                                        </th>
                                                        <th>
                                                            طرح عقیق سامان
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            ۰ تا ۱۵ سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۲۹۶,۱۴۰
                                                        </td>
                                                        <td>
                                                            ۴۶۶,۳۱۰
                                                        </td>
                                                        <td>
                                                            ۶۵۴,۱۶۰
                                                        </td>
                                                        <td>
                                                            ۸۲۵,۴۳۵
                                                        </td>
                                                        <td>
                                                            ۹۵۸,۰۳۵
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ۱۶ تا ۵۰ سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۵۹۲,۲۸۰
                                                        </td>
                                                        <td>
                                                            ۹۳۲,۶۲۰
                                                        </td>
                                                        <td>
                                                            ۱,۳۰۸,۳۲۰
                                                        </td>
                                                        <td>
                                                            ۱,۶۵۰,۸۷۰
                                                        </td>
                                                        <td>
                                                            ۱,۹۱۶,۰۷۰
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ۵۱ تا ۶۰ سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۷۱۰,۷۳۶
                                                        </td>
                                                        <td>
                                                            ۱,۱۱۹,۱۴۴
                                                        </td>
                                                        <td>
                                                            ۱,۵۹۶,۹۸۴
                                                        </td>
                                                        <td>
                                                            ۱,۹۸۱,۰۴۴
                                                        </td>
                                                        <td>
                                                            ۲,۲۹۹,۲۸۴
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ۶۱ تا ۷۰سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۸۸۸,۴۲۰
                                                        </td>
                                                        <td>
                                                            ۱,۳۹۸,۹۳۰
                                                        </td>
                                                        <td>
                                                            ۱,۹۶۲,۴۸۰
                                                        </td>
                                                        <td>
                                                            ۲,۴۷۶,۳۰۵
                                                        </td>
                                                        <td>
                                                            ۲,۸۷۴,۱۰۵
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <h4 class="sub-title">
                                                جدول حق بیمه در بیمه تکمیلی سامان با احتساب تخفیف خانواده های سه نفر به
                                                بالا به اضافه تخفیف خرید نقدی یا تخفیف
                                                بیمه عمر سامان (۲۵ درصد) (قیمت ها به تومان)
                                            </h4>
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered car-table ">
                                                    <thead>
                                                    <tr>
                                                        <th style="text-align: center">
                                                            سن
                                                        </th>
                                                        <th>
                                                            طرح نسیم سامان
                                                        </th>
                                                        <th>
                                                            طرح مهر سامان
                                                        </th>
                                                        <th>
                                                            طرح سروش سامان
                                                        </th>
                                                        <th>
                                                            طرح شمیم سامان
                                                        </th>
                                                        <th>
                                                            طرح وصال سامان
                                                        </th>
                                                        <th>
                                                            طرح عقیق سامان
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            ۰ تا ۱۵ سال
                                                        </td>
                                                        <td>
                                                            ۱۰۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۲۶۱,۳۰۰
                                                        </td>
                                                        <td>
                                                            ۴۱۱,۴۵۰
                                                        </td>
                                                        <td>
                                                            ۵۷۷,۲۰۰
                                                        </td>
                                                        <td>
                                                            ۷۲۸,۳۲۵
                                                        </td>
                                                        <td>
                                                            ۸۴۵,۳۲۵
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ۱۶ تا ۵۰ سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۵۲۲,۶۰۰
                                                        </td>
                                                        <td>
                                                            ۸۲۲,۹۰۰
                                                        </td>
                                                        <td>
                                                            ۱,۱۵۴,۴۰۰
                                                        </td>
                                                        <td>
                                                            ۱,۴۵۶,۶۵۰
                                                        </td>
                                                        <td>
                                                            ۱,۶۹۰,۶۵۰
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ۵۱ تا ۶۰ سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۶۲۷,۱۲۰
                                                        </td>
                                                        <td>
                                                            ۹۸۷,۴۸۰
                                                        </td>
                                                        <td>
                                                            ۱,۳۵۸,۲۸۰
                                                        </td>
                                                        <td>
                                                            ۱,۷۴۷,۹۸۰
                                                        </td>
                                                        <td>
                                                            ۲,۰۲۸,۷۸۰
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ۶۱ تا ۷۰سال
                                                        </td>
                                                        <td>
                                                            ۱۹۵,۰۰۰
                                                        </td>
                                                        <td>
                                                            ۷۸۳,۹۰۰
                                                        </td>
                                                        <td>
                                                            ۱,۲۳۴,۳۵۰
                                                        </td>
                                                        <td>
                                                            ۱,۷۳۱,۶۰۰
                                                        </td>
                                                        <td>
                                                            ۲,۱۸۴,۹۷۵
                                                        </td>
                                                        <td>
                                                            ۲,۵۳۵,۹۷۵
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                        <div class="third-title">
                                            <h4>خرید بیمه تکمیلی انفرادی سامان بدون داشتن بیمه‌گر پایه</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                درگذشته برای خرید بیمه تکمیلی، داشتن بیمه‌گر پایه (تامین اجتماعی، حوادث
                                                درمانی، خدمات درمانی نیروهای مسلح و سلامت ایرانیان) در اکثر شرکت‌های
                                                بیمه الزامی بوده‌است. اما هم‌اکنون بعضی از شرکت‌های بیمه از جمله بیمه
                                                سامان برای رفاه حال خریداران بیمه تکمیلی خود، لزوم داشتن بیمه‌گر پایه را
                                                حذف نموده‌اند و در صورتی که خریدار بیمه تکمیلی، بیمه‌گر پایه
                                                نداشته‌باشد، تنها 18 درصد به مبلغ حق‌بیمه او افزوده خواهدشد و تغییر
                                                دیگری در بیمه‌نامه تکمیلی او به علت نداشتن بیمه‌گر پایه رخ نخواهد
                                                داد. </p>


                                        </div>
                                        <div class="third-title">
                                            <h4>دوره انتظار بیمه تکمیلی سامان</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                به مدت زمانی گفته می‌شود که پس از گذشت آن بیمه‌گذار می‌تواند از پوشش‌های
                                                بیمه‌نامه خود استفاده نماید.
                                            </p>
                                            <p class="paragraph">
                                                هریک از طرح‌های بیمه تکمیلی انفرادی سامان دارای دوره‌ی انتظار 3 ماهه
                                                برای خدمات بستری و جراحی و دوره انتظار 9 ماهه برای زایمان می‌باشند.
                                            </p>
                                            <p class="paragraph">
                                                به این معنا که برای استفاده از پوشش بستری و جراحی بیمه‌نامه تکمیلی، باید
                                                حداقل ۳ ماه و برای استفاده از پوشش زایمان باید حداقل ۹ ماه از خرید آن
                                                گذشته‌باشد، بنابراین می‌بایست قبل از بارداری برای خرید بیمه تکمیلی با
                                                پوشش زایمان اقدام کنید.
                                            </p>
                                        </div>

                                        <div class="third-title">
                                            <h4>فرانشیز بیمه تکمیلی سامان</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                فرانشیز به مقداری از هزینه‌های درمانی گفته می‌شود که پرداخت آن برعهده
                                                خود بیمه‌گذار است و بقیه هزینه‌های درمان را شرکت بیمه پرداخت
                                                می‌کند. </p>

                                            <p class="paragraph">
                                                مقدار فرانشیز بیمه تکمیلی سامان ۱۰ درصد می‌باشد و هیچ ارتباطی با داشتن
                                                بیمه‌گر پایه و یا نداشتن آن ندارد. به این معنا که هزینه‌های درمانی
                                                بیمه‌گذار هرمقداری که باشد حداقل ۱۰ درصد آن باید توسط بیمه‌گذار پرداخت
                                                شود. </p>
                                            <p class="paragraph">
                                                برای مثال هزینه‌های پزشکی فردی ۱۰,۰۰۰,۰۰۰ تومان شده‌است، از این مقدار
                                                پرداخت ۱۰ درصد یعنی ۱,۰۰۰,۰۰۰ تومان برعهده‌ی شخص بیمه‌گذار و پرداخت ۹۰
                                                درصد مابقی یعنی ۹,۰۰۰,۰۰۰ تومان تا سقف تعهدات بیمه سامان، برعهده او
                                                می‌باشد.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4>شرایط سنی بیمه تکمیلی سامان</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                بازه‌ی سنی بیمه تکمیلی انفرادی سامان از بدو تولد تا ۷۰ سالگی می‌باشد و
                                                افراد بیشتر از ۷۰ سال امکان خرید بیمه تکمیلی انفرادی سامان را نخواهند
                                                داشت.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4>بیمه تکمیلی خانوادگی سامان</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                عده‌ای تصور می‌کنند که در بیمه تکمیلی خانوادگی با پرداخت یک حق‌بیمه توسط
                                                سرپرست خانواده تمام اعضای آن خانواده تحت پوشش قرار می‌گیرند. اما این
                                                تصور کاملا اشتباه است. </p>
                                            <p class="paragraph">
                                                بیمه تکمیلی خانوادگی در واقع همان بیمه تکمیلی انفرادی است که با دریافت
                                                حق‌بیمه به تعداد اعضای خانواده برای همه‌ی آن‌ها بیمه تکمیلی خریداری
                                                می‌شود. با این تفاوت که به جای صدور بیمه‌نامه به تعداد اعضای خانواده، یک
                                                بیمه‌نامه که در آن نام سرپرست خانواده و افراد تحت تکفل او نوشته شده‌باشد
                                                صادر خواهدشد. </p>
                                            <p class="paragraph">
                                                همچنین در این بیمه‌نامه برای هر یک از اعضای خانواده می‌توان طرح
                                                جداگانه‌ای یانتخاب و خریداری کرد و لزومی ندارد که همه‌ی افراد خانواده
                                                فقط از یک طرح استفاده نمایند.
                                                در حال حاضر علاوه بر بیمه تکمیلی انفرادی، بیمه تکمیلی خانوادگی سامان نیز
                                                توسط بی می تو صادر و به متقاضیان ارائه می‌گردد.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4>روش‌های پرداخت حق‌بیمه‌ تکمیلی</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                حق بیمه تکمیلی به صورت سالیانه و براساس سن بیمه گذار تعیین می شود
                                                و بیمه گذار می تواند به یکی از دو روش
                                                <b>نقدی</b>
                                                و یا
                                                <b>اقساطی</b>
                                                حق بیمه خود را پرداخت نماید.

                                                <br>

                                                پرداخت حق بیمه به روش
                                                <b>نقدی و یکجا</b>
                                                ، شامل ۱۰ درصد تخفیف خواهدشد و درصورتی که بیمه گذار مایل به پرداخت
                                                حق بیمه خود به صورت
                                                <b>اقساطی</b>
                                                باشد، در ابتدا باید ۳۰ درصد آن را به عنوان پیش پرداخت بپردازد
                                                و مابقی را در ۴ فقره چک ۱ ماهه پرداخت
                                                نماید.

                                                <br>

                                                به این نکته باید توجه داشت که در بین تمام طرح های بیمه تکمیلی
                                                انفرادی سامان، فقط در طرح نسیم امکان پرداخت حق بیمه به
                                                صورت اقساطی وجود ندارد.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4>فرآیند دریافت هزینه‌های پزشکی از شرکت بیمه سامان</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                دو روش برای دریافت هزینه های پزشکی مورد تعهد بیمه سامان وجود دارد
                                                که در ادامه به شرح آن ها خواهیم پرداخت:

                                                <br>

                                                روش یک: بیمه گذاران به مراکز درمانی طرف قرارداد بیمه سامان مراجعه
                                                کنند.

                                                <ul>
                                                    <li>
                                                        در صورت مراجعه بیمه گذار به مراکز درمانی طرف قرارداد بیمه
                                                        سامان که امکان صدور معرفی نامه به صورت آنلاین را
                                                        داشته باشند، در همان محل معرفی نامه به صورت آنلاین
                                                        برای آن ها صادر می شود.

                                                        <br>

                                                        در این حالت پرداخت هزینه های درمانی بیمه گذار تا سقف
                                                        تعهدات پوشش های بیمه سامان برعهده ی خود شرکت می باشد
                                                        و
                                                        بیمه گذار فقط باید ۱۰ درصد هزینه های پزشکی را در همان
                                                        محل به عنوان فرانشیز و سهم خود پرداخت کند.
                                                    </li>
                                                    <li>
                                                        در صورتی که بیمه گذار بخواهد به مراکز درمانی طرف قرارداد
                                                        بیمه سامان که امکان صدور معرفی نامه به صورت
                                                        آنلاین را ندارند، مراجعه کند، باید از قبل به نمایندگی های
                                                        بیمه سامان رفته و برای مرکز درمانی مورد نظر خود
                                                        معرفی نامه دریافت نماید. در این حالت نیز مانند حالت قبل
                                                        بیمه گذار فقط باید ۱۰ درصد هزینه های پزشکی را در
                                                        همان جا، به عنوان فرانشیز و سهم خود پرداخت نماید.
                                                    </li>
                                                </ul>

                                                <br>

                                                روش دوم: بیمه گذاران به مراکزی به غیر از مراکز طرف قرارداد بیمه
                                                سامان مراجعه کنند.

                                                <ul>
                                                    <li>
                                                        • در این صورت اگر بیمه گذار بیمه گر پایه داشته باشد
                                                        و بخواهد از آن استفاده کند در ابتدا باید با فاکتور
                                                        هزینه ها و مدارک درمانی به بیمه گر پایه خود مراجعه
                                                        نماید و مبلغ مورد تعهد بیمه گر اول را دریافت کند. پس از
                                                        آن بیمه گذار باید به یکی از نمایندگی های بیمه سامان
                                                        مراجعه نماید و با تحویل مدارک خود مابقی هزینه ها به
                                                        استثنای ۱۰ درصد فرانشیز را دریافت کند.
                                                    </li>
                                                    <li>
                                                        • در غیر این صورت اگر بیمه گذار، بیمه گر پایه نداشته
                                                        باشد و یا نخواهد از آن استفاده کند، می تواند از همان
                                                        ابتدا به یکی از نمایندگی های بیمه سامان مراجعه نماید و
                                                        هزینه های پزشکی خود را تا سقف تعهدات بیمه سامان از آن
                                                        دریافت کند.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="third-title">
                                            <h4>فرآیند خرید بیمه تکمیلی انفرادی/خانوادگی</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                شما می توانید در ۶ مرحله سفارش خرید بیمه تکمیلی انفرادی/خانوادگی خود را
                                                در سایت بی می تو ثبت کنید و پس از بررسی خرید
                                                توسط کارشناسان بی می تو، در اولین فرصت با شما تماس گرفته خواهدشد.

                                                <br>

                                                مراحل خرید بیمه تکمیلی
                                                <b>انفرادی/خانوادگی</b>
                                                به شرح زیر می باشد:

                                                <br>

                                                مرحله اول: در ابتدا خریدار بیمه پس از انتخاب گزینه انفرادی، اطلاعات
                                                اولیه از جمله سن بیمه شده و نام بیمه گر پایه خود
                                                را وارد کرده و پس از تایید اطلاعات وارد شده، به مرحله بعد هدایت می شود.

                                                <br>

                                                در صورتی که خریدار بیمه تکمیلی، بیمه گر پایه نداشته باشد می بایست گزینه
                                                " بیمه گر پایه ندارم " را انتخاب نماید.

                                                <br>

                                                مرحله دوم: با توجه به اطلاعات ورودی خریدار بیمه، فهرستی از طرح های بیمه
                                                تکمیلی نمایش داده می &zwj;شود که با مقایسه
                                                قیمت ها، پوشش ها و سقف تعهدات هرکدام از آن ها، خریدار بیمه می تواند طرح
                                                موردنظر خود را سفارش دهد.

                                                <br>

                                                مرحله سوم: در این مرحله مشخصات اولیه خریدار بیمه و سقف هریک از تعهدات
                                                طرح موردنظر او نمایش داده می شود که در صورت
                                                تایید اطلاعات، خریدار بیمه به مرحله بعدی وارد خواهدشد. همچنین می تواند
                                                برای تغییر طرح خود به مرحله قبل بازگردد.

                                                <br>

                                                مرحله چهارم: خریدار بیمه اطلاعات کامل خود را اعم از نام، نام خانوادگی،
                                                شماره تماس، آدرس محل سکونت، کدملی و کدپستی
                                                خود را در این مرحله وارد می کند.

                                                <br>

                                                مرحله پنجم: پس از ورود به مرحله پنجم خریدار بیمه باید دو فرم
                                                <b>پرسشنامه گواهی سلامت و پیشنهاد بیمه تکمیلی</b>
                                                را دانلود کرده و گزینه تایید اطلاعات را انتخاب نماید.

                                                <br>

                                                مرحله ششم: در مرحله آخر با دریافت شماره، مبلغ و تاریخ ثبت سفارش، فرآیند
                                                خرید به پایان و تایید نهایی خواهدرسید و در
                                                اولین فرصت کارشناسان بی می تو برای پیگیری سفارش خریدار بیمه، با ایشان
                                                تماس
                                                خواهندگرفت.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4>بیمه تکمیلی چیست؟</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                یكی از مهم‌ترین و پرکاربردترین انواع‌ پوشش‌های بیمه‌‌ای، بیمه‌ تکمیلی
                                                درمان است‌. باتوجه‌ به‌ اینكه‌ انسان همواره‌ در معرض‌ خطر انواع‌ مختلف‌
                                                بیماری‌ها بوده‌ و هست‌، به ناچار برای معالجه‌ بیماری و بهبودی، متقبل‌
                                                هزینه‌‌های سنگین‌ پزشكی، دارو، عمل‌ها‌ی جراحی و مخارج‌ بیمارستان‌
                                                می‌شود. به‌ منظور كمك‌ به‌ مردم‌ در پرداخت چنین‌ هزینه‌هایی، شركت‌های
                                                بیمه‌ طرح‌های مختلف بیمه‌های درمانی را ارائه‌ می‌نمایند. این‌ نوع‌ بیمه‌
                                                در كشور ایران‌ به‌ صورت‌ گروهی، خانوادگی و انفرادی عرضه‌ می‌گردد.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4>بیمه تکمیلی انفرادی</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                از آنجایی که تمام افراد نمی توانند تحت نظر ارگان یا سازمان خاصی به صورت
                                                گروهی بیمه تکمیلی درمان را تهیه نمایند، برخی
                                                شرکت های بیمه اقدام به ارائه طرح بیمه تکمیلی به صورت انفرادی نموده اند.
                                                این اقدام شرکت های بیمه کمک بسیاری به جبران
                                                هزینه های درمانی این افراد می نماید.

                                                <br>

                                                بیمه تکمیلی انفرادی در شرکت های مختلف دارای طرح ها و پوشش های متفاوتی
                                                است و با حق بیمه های مختلف ارائه می شود تا
                                                تمام اقشار جامعه قادر به استفاده از این طرح ها باشند.

                                                <br>

                                                در بیمه تکمیلی انفرادی، بیمه گر متعهد می شود که کلیه هزینه های درمانی و
                                                بیمارستانی بیمه گذار شامل هزینه های
                                                بیمارستانی، عمل های جراحی، پاراکلینیکی، جراحی های سرپایی، خدمات
                                                آزمایشگاهی، هزینه های آمبولانس، زایمان، ویزیت، دارو و
                                                دندانپزشکی را براساس شرایط بیمه نامه تکمیلی درمان انفرادی و فرانشیز
                                                توافق شده پرداخت نماید.
                                            </p>
                                        </div>


                                        <div class="third-title">
                                            <h4>مزایای بیمه تکمیلی انفرادی</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                با خرید بیمه تکمیلی انفرادی، می توان از مزایای زیر نیز بهره مند شد:

                                                <ul>
                                                    <li>
                                                        عدم تعلق مالیات به بیمه تکمیلی
                                                    </li>
                                                    <li>
                                                        معرفی پزشک، بیمارستان و مراکز تشخیصی
                                                    </li>
                                                    <li>
                                                        استفاده از طرح های متناسب با نیاز بیمه ای و توانمندی مالی هر فرد
                                                    </li>
                                                    <li>
                                                        پرداخت هزینه های بستری در بیمارستان
                                                    </li>
                                                    <li>
                                                        پرداخت هزینه عمل های جراحی خاص
                                                    </li>
                                                    <li>
                                                        پرداخت هزینه های تشخیصی، پاراکلینیکی و هزینه های جراحی های
                                                        سرپایی
                                                    </li>
                                                    <li>
                                                        جبران هزینه های زایمان
                                                    </li>
                                                    <li>
                                                        پرداخت هزینه های دندان پزشکی
                                                    </li>
                                                    <li>
                                                        پرداخت هزینه درمان عیوب انکساری چشم
                                                    </li>
                                                    <li>
                                                        و...
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="third-title">
                                            <h4>بیمه تکمیلی گروهی (سازمانی)</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                هر مجموعه ای می تواند تمام کارکنان رسمی، پیمانی یا قراردادی خود را که
                                                مایل به دریافت پوشش بیمه تکمیلی درمان باشند،
                                                بیمه نماید. همچنین تمام اعضای خانواده پرسنل که از طرف بیمه گذار به عنوان
                                                بیمه شده معرفی شوند، تحت پوشش بیمه قرار
                                                می گیرند.

                                                <br>

                                                در بیمه تکمیلی گروهی، بیمه گذار می تواند کارکنان بازنشسته خود را به
                                                همراه تمام افراد خانواده تحت تکفل آنان بیمه کند.
                                                بیمه گذار با قرار دادن نام و مشخصات این افراد در لیست بیمه شدگان، در
                                                ابتدای قرارداد و یا در زمان تمدید قرارداد، آنان
                                                را تحت پوشش بیمه تکمیلی قرار می دهد.

                                                <br>

                                                در بیمه تکمیلی گروهی منظور از اعضای خانواده، همسر، فرزندان، پدر، مادر و
                                                افراد تحت تکفل بیمه شدگان است.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4>فرآیند خرید بیمه تکمیلی سازمانی</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                برای خرید بیمه تکمیلی سازمانی فقط لازم است که بیمه گذار پس از انتخاب
                                                گزینه سازمانی، اطلاعات اولیه خود از جمله نام
                                                شرکت یا سازمان، تعداد اعضا، نام متقاضی و شماره تماس خود را وارد نماید.
                                                پس از تایید اطلاعات ورودی، سفارش بیمه گذار در
                                                سایت ثبت خواهدشد و کارشناسان بی می تو پس از بررسی درخواست خرید بیمه
                                                تکمیلی در اولین فرصت جهت تکمیل فرآیند خرید، با ایشان
                                                تماس خواهندگرفت.

                                                <br>

                                                مراحل خرید بیمه تکمیلی سازمانی به شرح زیر می باشد:

                                                <br>

                                                مرحله یک: در ابتدا با انتخاب گزینه سازمانی، بیمه گذار باید اطلاعات اولیه
                                                خود از جمله نام شرکت یا سازمان، تعداد اعضا،
                                                نام متقاضی و شماره تماس خود را وارد نماید.

                                                <br>

                                                مرحله دوم: پس از تایید اطلاعات ورودی، سفارش بیمه گذار در سایت ثبت
                                                خواهدشد.

                                                <br>

                                                مرحله سوم: کارشناسان بی می تو پس از بررسی درخواست خرید بیمه تکمیلی در
                                                اولین فرصت جهت تکمیل فرآیند خرید، با بیمه گذار
                                                تماس خواهند گرفت.
                                            </p>
                                        </div>

                                        <div class="third-title">
                                            <h4>بیمه تکمیلی گروهی برای اصناف، اتحادیه‌‌ها و انجمن‌ها</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                ارائه پوشش به اصناف و اتحادیه ها با شرایط زیر مجاز است:

                                                <ul>
                                                    <li>
                                                        این گروه ها با هدف دریافت پوشش بیمه تکمیلی تشکیل نشده باشند.
                                                    </li>
                                                    <li>
                                                        پرداخت حق بیمه سالیانه توسط بیمه گذار تضمین شده باشد.
                                                    </li>
                                                    <li>
                                                        حداقل ۷۰ درصد اعضای هر گروه به طور هم زمان تحت پوشش قرار گیرند.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="third-title">
                                            <h4>هزینه‌های درمانی قابل پرداخت در بیمه تکمیلی گروهی</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                همانطور که می‌دانید شرکت‌های مختلف بیمه در ارائه پوشش‌ها مقداری تفاوت
                                                دارند اما به صورت کلی هزینه‌های درمانی قابل پرداخت بیمه تکمیلی درمان
                                                شامل موارد زیر است: </p>
                                            <h4 class="sub-title">
                                                تعهدات اصلی بیمه تکمیلی گروهی
                                            </h4>
                                            <div class="paragraph">
                                                <ul>
                                                    <li>
                                                        جبران هزینه های بستری ، جراحی، شیمی درمانی ، رادیوتراپی،
                                                        آنژیوگرافی قلب، گامانایف و انواع سنگ شکن در
                                                        بیمارستان و مراکز جراحی محدود و Day Care
                                                    </li>
                                                    <li>
                                                        عمل ها جراحی Day Care به جراحی هایی اطلاق می شود که مدت زمان
                                                        مورد نیاز برای مراقبت های بعد از عمل در مراکز
                                                        درمانی، کمتر از یک روز باشد.
                                                    </li>
                                                    <li>
                                                        هزینه همراه افراد زیر ۷ سال و بالاتر از ۷۰ سال (در بیمارستان ها)
                                                    </li>
                                                    <li>
                                                        هزینه آمبولانس و سایر فوریت های پزشکی مشروط به بستری شدن بیمه
                                                        شده در مراکز درمانی و یا نقل و انتقال
                                                        بیمار به سایر مراکز تشخیصی-درمانی طبق دستور پزشک معالج
                                                    </li>
                                                </ul>
                                            </div>

                                            <h4 class="sub-title">
                                                پوشش‌های اضافی بیمه تکمیلی گروهی
                                            </h4>
                                            <div class="paragraph">
                                                موارد زیر در صورتی که در قرارداد یا شرایط بیمه نامه درج شده باشند، قابل
                                                پوشش دهی می باشد:

                                                <ul>
                                                    <li>
                                                        افزایش سقف تعهد برای عمل های جراحی مربوط به سرطان، مغز و اعصاب
                                                        مرکزی و نخاع (به استثنای دیسک ستون فقرات) ،
                                                        گامانایف، قلب، پیوند ریه، پیوند کبد، پیوند کلیه و پیوند مغز
                                                        استخوان.
                                                    </li>
                                                    <li>
                                                        هزینه های زایمان اعم از طبیعی و سزارین، تا پنجاه درصد سقف تعهد
                                                        سالیانه مندرج در بند الف،۱ فوق سقف تعهد
                                                        بیمه گر در این پوشش نباید از بالاترین هزینه توافق شده با
                                                        بیمارستان های طرف قرارداد بیمه گر تجاوز کند.
                                                    </li>
                                                    <li>
                                                        در صورت اخذ پوشش زایمان، ارائه پوشش هزینه های مربوط به درمان
                                                        نازایی و ناباروری شامل عمل ها جراحی مرتبط، IUI
                                                        ، ITSC ،ZIFT ،GIFT ، میکرواینجکشن و IVF حداکثر معادل سقف تعهد
                                                        زایمان و به صورت یک پوشش مستقل از آن مجاز
                                                        است.
                                                    </li>
                                                </ul>
                                            </div>
                                            <h4 class="sub-title">
                                                پوشش هزینه‌های پاراکلینیکی بیمه تکمیلی گروهی
                                            </h4>
                                            <div class="paragraph">
                                                <ul>
                                                    <li>
                                                        جبران هزینه های مربوط به سونوگرافی، سونو غربالگری، ماموگرافی،
                                                        انواع اسکن، انواع آندوسکوپی، ام آر آی،
                                                        اکوکاردیوگرافی، استرس اکو، انواع آنژیوگرافی (بجز چشم و قلب) ،
                                                        هزینه های پزشکی هسته ای، سی تی آنژیوگرافی،
                                                        پنتاکم، فوندسکوپی، کانفواسکن، انتروپیون، پاکیمتری و تمام تست های
                                                        بینایی سنجی، ICG، IOL master ، HRT، انواع
                                                        آندوسکوبی با یا بدون بیهوشی.
                                                    </li>
                                                    <li>
                                                        جبران هزینه های مربوط به تست ورزش، تست آلرژی، تست تنفسی
                                                        (اسپیرومتری)، نوارعضله(EMG) ، نوارعصب (NCV)، نوارمغز
                                                        (EEG)، نوار مثانه (سیستومتری یا سیستوگرام) ، شنوایی سنجی، بینایی
                                                        سنجی ، آنژیوگرافی چشم، هولترمانیتورینگ قلب،
                                                        تست خواب دانستیومتری، تمپانومتری، بادی باکس، تیلت پلتیسموگرافی،
                                                        ارگواسپیرو متری، رینو ماتومتری، برونکوگرافی
                                                        و تست متاکولین (PFT)جبران هزینه های خدمات آزمایشگاهی شامل آزمایش
                                                        های تشخیص پزشکی، پاتولوژی یا آسیب شناسی و
                                                        ژنتیک پزشکی، انواع رادیوگرافی، نوار قلب، فیزیوتراپی.
                                                    </li>
                                                    <li>
                                                        جبران هزینه های ویزیت، دارو و خدمات اورژانس در موارد غیر بستری.
                                                    </li>
                                                    <li>
                                                        جبران هزینه های دندان پزشکی (چنانچه هزینه های دندان پزشکی صرفا
                                                        محدود به خدمات کشیدن، جرم گیری، بروساژ،
                                                        ترمیم، پرکردن، درمان ریشه و روکش شود.)
                                                    </li>
                                                    <li>
                                                        هزینه های دندان پزشکی بر اساس تعرفه خدمات دندان پزشکی ای محاسبه
                                                        و پرداخت می شود که سالیانه سندیکای
                                                        بیمه گران ایران با هماهنگی شرکت های بیمه، تنظیم و به شرکت های
                                                        بیمه ابلاغ می کند.
                                                    </li>
                                                    <li>
                                                        جبران هزینه های مربوط به خرید عینک طبی و لنز تماس طبی.
                                                    </li>
                                                    <li>
                                                        جبران هزینه های مربوط به خرید سمعک.
                                                    </li>
                                                    <li>
                                                        جبران هزینه های مربوط به رفع عیوب انکساری چشم در مواردی که به
                                                        تشخیص پزشک معتمد بیمه گر، جمع قدر مطلق نقص
                                                        بینایی هر چشم (درجه نزدیک بینی یا دوربینی به اضافه نصف آستیگمات)
                                                        ۴ دیوپتر یا بیشتر باشد.
                                                    </li>
                                                    <li>
                                                        جبران هزینه عمل های مجاز سرپایی مانند شکسته بندی، گچ گیری، ختنه،
                                                        بخیه، کرایوتراپی، اکسیزیون لیپوم، بیوپسی،
                                                        تخلیه کیست و لیزر درمانی.
                                                    </li>
                                                </ul>
                                                <strong>تمام این پوشش ها تا سقف مجاز مشخص شده در بیمه نامه قابل تأمین می
                                                    باشد.</strong>
                                                <br>
                                                <br>
                                                <ul>
                                                    <li>
                                                        سایر پوشش های بیمه درمانی بیمه تکمیلی گروهی
                                                    </li>
                                                    <li>
                                                        جبران هزینه تهیه اعضای بدن برای پیوند (طبیعی بدن) ، حداکثر به
                                                        میزان تعهد پایه سالیانه برای هر بیمه شده.
                                                    </li>
                                                    <li>
                                                        ارایه پوشش بیمه ای جهت خطرات طبیعی (به استثنای زلزله) مشروط به
                                                        دریافت حق بیمه اضافی.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="third-title">
                                            <h4>عمل‌های غیر‌مجاز در مطب</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                <p>
                                                    انجام تمام عمل‌های فهرست زیر در مطب غیر مجاز هستند و در صورت انجام
                                                    این عمل‌ها در مطب بیمه‌گر هیچ‌گونه تعهدی در پرداخت هزینه‌های مربوط
                                                    به آنها ندارد:
                                                </p>
                                                <img class="w-100 mt-4" src="images/health-table.png"
                                                     alt="لیست عمل های غیر مجاز در مطب">
                                            </div>
                                        </div>
                                        <div class="third-title">
                                            <h4>فرانشیز بیمه تکمیلی:</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                اگر بیمه شده از دفترچه درمانی بیمه گر پایه استفاده نکند، بین ۱۰ تا ۳۰
                                                درصد کل هزینه های درمانی بیمارستانی، جراحی،
                                                زایمان و سایر هزینه های تحت پوشش، تحت عنوان فرانشیز به عهده بیمه شده
                                                خواهد بود.

                                                <br>

                                                در غیر این صورت فرانشیز معادل سهم بیمه گر پایه و حداقل ۳۰ درصد خواهد
                                                بود.

                                                <br>

                                                بیمه گذار می تواند با پرداخت حق بیمه اضافی و خرید پوشش فرانشیز، صرفا
                                                فرانشیز هزینه های بیمارستانی، جراحی عمومی و
                                                تخصصی و زایمان را پرداخت نکند و فرانشیز این بخش از پوشش ها به عهده بیمه
                                                گر باشد.

                                                <br>

                                                فرانشیز می تواند طبق توافق بیمه گر و بیمه گذار تعیین گردد. توجه داشته
                                                باشید که در هر صورت حداقل فرانشیز ۱۰ درصد است
                                                که قابل بیمه شدن نیست.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4>استثنائات بیمه تکمیلی گروهی</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                در بیمه درمانی جبران هزینه موارد زیر جزو تعهدات بیمه گر نیست:

                                                <ul>
                                                    <li>
                                                        عمل های جراحی که به منظور زیبایی انجام می شود، مگر اینکه ناشی از
                                                        وقوع حادثه در طی مدت بیمه باشد.
                                                    </li>
                                                    <li>
                                                        عیوب مادر زادی مگر اینکه طبق تشخیص پزشک معالج و تأیید پزشک معتمد
                                                        بیمه گر، رفع این عیوب جنبه درمانی داشته
                                                        باشد.
                                                    </li>
                                                    <li>
                                                        سقط جنین مگر در موارد قانونی با تشخیص پزشک معالج
                                                    </li>
                                                    <li>
                                                        ترک اعتیاد
                                                    </li>
                                                    <li>
                                                        خودکشی و عمل ها مجرمانه
                                                    </li>
                                                    <li>
                                                        حوادث طبیعی مانند سیل، زلزله و آتشفشان
                                                    </li>
                                                    <li>
                                                        جنگ، شورش، اغتشاش، بلوا، اعتصاب، قیام، آشوب، کودتا و اقدامات
                                                        احتیاطی مقامات نظامی و انتظامی
                                                    </li>
                                                    <li>
                                                        خسارت های ناشی از تشعشعات و انفعالات هسته ای
                                                    </li>
                                                    <li>
                                                        هزینه اتاق خصوصی مگر در موارد ضروری به تشخیص پزشک معالج و تأیید
                                                        پزشک معتمد بیمه گر
                                                    </li>
                                                    <li>
                                                        هزینه همراه بیماران بین ۷ سال تا ۷۰ سال مگر در موارد ضروری به
                                                        تشخیص پزشک معالج و تأیید پزشک معتمد بیمه گر
                                                    </li>
                                                    <li>
                                                        جنون
                                                    </li>
                                                    <li>
                                                        جراحی لثه
                                                    </li>
                                                    <li>
                                                        زایمان برای فرزند چهارم و بیشتر
                                                    </li>
                                                    <li>
                                                        لوازم بهداشتی و آرایشی که جنبه دارویی ندارند.
                                                    </li>
                                                    <li>
                                                        جراحی فک مگر آنکه به علت وجود تومور و یا وقوع حادثه تحت پوشش
                                                        باشد.
                                                    </li>
                                                    <li>
                                                        هزینه های مربوط به معلولیت ذهنی و ازکار افتادگی کلی
                                                    </li>
                                                    <li>
                                                        لقاح مصنوعی
                                                    </li>
                                                    <li>
                                                        عقیم سازی مگر اینکه جنبه درمانی داشته باشد.
                                                    </li>
                                                    <li>
                                                        رفع عیوب انکساری چشم در مواردی که به تشخیص پزشک معتمد بیمه گر
                                                        جمع قدر مطلق نقص بینایی هرچشم (درجه نزدیک
                                                        بینی یا دوربینی به اضافه نصف آستیگمات) کمتر از ۴ دیوپتر باشد.
                                                    </li>
                                                    <li>
                                                        کلیه هزینه های پزشکی که در مراحل تحقیقاتی بوده و تعرفه درمانی آن
                                                        از سوی وزارت بهداشت، درمان و آموزش پزشکی
                                                        تدوین و اعلام نگردیده است.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="third-title">
                                            <h4>استثنائاتی که با پرداخت حق بیمه اضافی قابل بیمه شدن هستند:</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                <ul>
                                                    <li>
                                                        حوادث طبیعی مانند سیل، زلزله و آتشفشان
                                                    </li>
                                                    <li>
                                                        جنگ، شورش، اغتشاش، بلوا، اعتصاب، قیام، آشوب، کودتا و اقدامات
                                                        احتیاطی مقامات نظامی و انتظامی
                                                    </li>
                                                    <li>
                                                        هزینه اتاق خصوصی
                                                    </li>
                                                    <li>
                                                        هزینه همراه
                                                    </li>
                                                    <li>
                                                        جراحی لثه
                                                    </li>
                                                    <li>
                                                        جراحی فک
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="third-title">
                                            <h4>شرایط بیمه درمان گروهی برای گروه‌های کمتر از ۵۰ نفر</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                در صورتی که گروه متقاضی بیمه تکمیلی درمان کمتر از ۵۰ نفر باشند رعایت
                                                موارد و ضوابط زیر الزامی است:

                                                <ul>
                                                    <li>
                                                        فرم پرسش نامه سلامت توسط اعضای اصلی گروه و یا سرپرست خانواده به
                                                        دقت تکمیل شود.
                                                    </li>
                                                    <li>
                                                        در صورت تشخیص پزشک معتمد بیمه گر، پرداخت هزینه های معاینات پزشکی
                                                        به عهده متقاضی است.
                                                    </li>
                                                    <li>
                                                        بیمه گر می تواند با بررسی معاینات انجام شده و پرسش نامه سلامت
                                                        اعضای خانواده متقاضیان، از بیمه کردن فرد یا
                                                        افرادی از گروه یا خانواده خودداری نماید.
                                                    </li>
                                                    <li>
                                                        ارائه پوشش هزینه های رفع عیوب انکساری چشم مجاز نیست.
                                                    </li>
                                                    <li>
                                                        سقف تعهدات بیمه گر برای تمام اعضای گروه یا خانواده در تمام عمل
                                                        های جراحی مورد تعهد (اعم از جراحی های عمومی،
                                                        تخصصی و فوق تخصصی) یکسان است.
                                                    </li>
                                                    <li>
                                                        پرداخت هزینه های زایمان و هزینه درمان بیماری هایی که سابقه درمان
                                                        قبلی دارد، در سال اول قرارداد ممکن نیست.
                                                    </li>
                                                    <li>
                                                        بیمه شده در انتخاب هریک از بیمارستان های داخل کشور آزاد است، و
                                                        پس از پرداخت هزینه مربوط باید صورت حساب مرکز
                                                        درمانی را به همراه نظریه پزشک یا پزشکان معالج در خصوص علت بیماری
                                                        و شرح معالجات انجام شده دریافت و به
                                                        بیمه گر تسلیم کند.
                                                    </li>
                                                    <li>
                                                        در مواردی که بیمه شده با معرفی نامه بیمه گر از مراکز درمانی طرف
                                                        قرارداد استفاده کند، صورت حساب مرکز درمانی
                                                        اساس محاسبه هزینه های مورد تعهد خواهد بود. در غیر این صورت هزینه
                                                        های مربوط براساس شرایط قرارداد منعقد شده
                                                        توسط بیمه گر با بیمارستان های هم تراز پرداخت خواهد شد.
                                                    </li>
                                                    <li>
                                                        بیمه گذار و یا بیمه شده باید حداکثر ظرف مدت ۵ روز از زمان بستری
                                                        شدن هریک از بیمه شدگان در بیمارستان و قبل
                                                        از ترخیص، به بیمه گر اطلاع دهد.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="third-title">
                                            <h4>شرایط سنی بیمه تکمیلی گروهی</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                <ul>
                                                    <li>
                                                        بیمه تکمیلی درمان برای افراد با سن بیش از
                                                        <b>۶۰</b>
                                                        سال، با پرداخت حق بیمه اضافی، قابل پوشش است. (
                                                        <span> ۶۰ تا ۷۰</span>
                                                        سال
                                                        <b> ۲ </b>
                                                        برابر حق بیمه)
                                                    </li>
                                                    <li>
                                                        درصورتی که سن بیمه شده در شروع قرارداد کمتر از
                                                        <b>۶۰</b>
                                                        سال باشد، پوشش بیمه تکمیلی تا پایان مدت قرارداد ادامه خواهد
                                                        یافت.
                                                    </li>
                                                    <li>
                                                        درصورتی که بیمه شده اصلی در طول مدت بیمه فوت کند، پوشش بیمه
                                                        تکمیلی سایر اعضای خانواده بیمه شده متوفی ادامه
                                                        خواهد داشت. (البته به شرط پرداخت حق بیمه)
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="third-title">
                                            <h4>پوشش بیمه تکمیلی در خارج از کشور</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                <p>
                                                    در صورتی که بیمه‌شدگان به‌ دلیل عدم‌ امکان درمان در داخل کشور با
                                                    تشخیص پزشک معالج بیمه‌شده و با تأیید بیمه‌گر به خارج اعزام ‌گردند، و
                                                    یا هنگام سفر به خارج از کشور به‌ دلیل فوریت‏‌های پزشکی نیاز به تشخیص
                                                    و معالجه پیدا ‏کنند، صورت‌ حساب هزینه‌های پزشکی و بیمارستانی آنان
                                                    باید مورد تایید سفارت یا کنسولگری جمهوری اسلامی ایران در کشور مربوطه
                                                    باشد، در این حالت هزینه‌های مورد تعهد بیمه‌گر مندرج در قرارداد تا
                                                    سقف تعیین شده در بیمه‌نامه پرداخت خواهد شد؛ در صورت عدم دستیابی هریک
                                                    از موارد فوق، هزینه‌های انجام‌ شده با‌ توجه به بالاترین تعرفه مراکز
                                                    درمانی طرف قرارداد بیمه‌گر محاسبه و پرداخت می‌شود. میزان خسارت
                                                    براساس نرخ ارز اعلام ‌شده توسط بانک مرکزی جمهوری اسلامی ایران در
                                                    زمان ترخیص از بیمارستان محاسبه خواهد شد.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="third-title">
                                            <h4>کدام بیمه تکمیلی انفرادی بهتر است؟</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                بهترین بیمه تکمیلی انفرادی براساس معیارهایی مانند طرح‌ها و پوشش‌های
                                                ارائه شده توسط شرکت‌های بیمه، تعداد مراکز درمانی طرف قرارداد، گستردگی و
                                                تعداد شعب و نمایندگی‌‌های شرکت بیمه، قیمت، میزان دوره انتظار و همچنین با
                                                توجه به نیاز خریداران و بیمه‌گذاران بیمه تکمیلی مشخص می‌شود.
                                            </p>
                                            <p class="paragraph">
                                                بیشتر افراد برای خرید بیمه تکمیلی به صورت حرفه‌ای، بدون نقص و متناسب با
                                                نیاز خود به مشاوره تخصصی و راهنمایی نیاز دارند. بی می تو با ارائه مشاوره
                                                ی رایگان و تخصصی به شناخت و انتخاب بهترین بیمه تکمیلی با توجه به شرایط
                                                شما کمک خواهد کرد.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4>بهترین بیمه تکمیلی انفرادی برای زایمان</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                یکی از پوشش های فرعی بیمه تکمیلی انفرادی و گروهی، پوشش تامین مخارج
                                                زایمان اعم از سزارین و یا طبیعی می باشد.

                                                <br>

                                                بهترین بیمه تکمیلی برای زایمان، طبق طرح های متفاوتی که از طرف شرکت های
                                                بیمه معرفی شده است، متفاوت خواهدبود و عواملی
                                                مانند فرانشیز کمتر، پوشش مالی بیشتر و دوره ی انتظار کمتر و... بر آن موثر
                                                است.

                                                <br>

                                                برخی از طرح های شرکت های بیمه پوشش هایی مخصوص درمان نازایی و ناباروی نیز
                                                ارائه می دهند.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4>بیمه تکمیلی انفرادی sos</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                برخی شرکت های بیمه گر با همکاری شرکت بین المللی SOS اقدام به ارائه بیمه
                                                تکمیلی نموده اند.

                                                <br>

                                                از جمله شرکت هایی که با همکاری این شرکت بین المللی کمک رسان، بیمه نامه
                                                بیمه تکمیلی صادر میکند، شرکت بیمه آسماری و
                                                پاسارگاد می باشد.

                                                <br>

                                                حداکثر سن مجاز برای خرید بیمه تکمیلی sos ،
                                                <b>۶۵</b>
                                                سال است و افراد بیشتر از این سن قادر به خرید این بیمه نامه نمی باشند.

                                                <br>

                                                با خرید بیمه تکمیلی sos شما صاحب یک کارت عضویت خواهید شد که به همراه آن
                                                میتوانید از خدمات پزشکی مراکز مشخص و مجاز
                                                استفاده کنید.
                                            </p>
                                        </div>

                                        <div class="third-title">
                                            <h4>بیمه تکمیلی سامان</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                بیمه سامان یکی از شرکت های پیشرو در زمینه ارائه بیمه تکمیلی می باشد که
                                                در حال حاضر یکی از معدود شرکت هایی که
                                                بیمه تکمیلی انفرادی را ارائه می دهد، بیمه تکمیلی سامان است.

                                                <br>

                                                در حال حاضر بیمه تکمیلی انفرادی سامان در قالب ۶ طرح بیمه ای به نام های:
                                                طرح نسیم، طرح مهر، طرح سروش، طرح شمیم، طرح
                                                وصال و طرح عقیق ارائه می شود. هر کدام از این طرح ها حق بیمه، پوشش ها و
                                                شرایط متفاوت دارند و هر فردی با توجه به
                                                شرایط و نیازهای خود می تواند یکی از این طرح ها را انتخاب و خریداری
                                                نماید.

                                                <br>

                                                همچنین بیمه تکمیلی سامان به صورت خانوادگی و گروهی نیز به مخاطبان این
                                                بیمه ها ارائه می شود.
                                            </p>
                                        </div>


                                        <div class="third-title">
                                            <h4>بیمه تکمیلی پاسارگاد</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                یکی از شرکت های ارائه دهنده بیمه تکمیلی، بیمه پاسارگاد می باشد. بیمه
                                                تکمیلی پاسارگاد از طریق شرکت کمک رسان sos و
                                                به صورت گروهی (سازمانی) ارائه می شود.

                                                <br>

                                                یکی از شرایط بیمه تکمیلی گروهی سامان این است که هر سازمان برای استفاده
                                                از این بیمه باید حداقل ۷۰ نفر پرسنل داشته
                                                باشد.

                                                <br>

                                                همچنین بهتر است بدانید که بیمه پاسارگاد بیمه تکمیلی را به صورت خانوادگی
                                                یا انفرادی ارائه نمی دهد.
                                            </p>
                                        </div>

                                        <div class="third-title">
                                            <h4>بیمه تکمیلی آسماری</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                بیمه آسماری یکی از شرکت های تازه تأسیس در صنعت بیمه محسوب می شود که بیمه
                                                تکمیلی خود را از طریق شرکت کمک رسان sos
                                                ارائه می دهد. بیمه تکمیلی آسماری به صورت انفرادی، خانوادگی و گروهی و در
                                                قالب طرح های متفاوت صادر می شود.

                                                <br>

                                                بیمه تکمیلی آسماری برای گروه های حداقل
                                                <b>۱۵</b>
                                                نفری به منظور پوشش گروه ها، سازمان ها، شرکت ها و ... ارائه می گردد.
                                            </p>
                                        </div>

                                        <div class="third-title">
                                            <h4>بیمه تکمیلی ما</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                بیمه تکمیلی ما تنها به صورت گروهی و برای سازمان ها، شرکت ها، کارخانه ها،
                                                مؤسسات و ... که تعداد کارکنان آنها بیشتر از
                                                <b>۵۰</b>
                                                نفر باشد، ارائه می شود.

                                                <br>

                                                یکی از شرایط بیمه تکمیلی گروهی ما این است که حداقل
                                                <b>۷۰</b>
                                                درصد اعضاي گروه متقاضي بيمه بوده و تمامی بیمه شده ها باید بیمه پایه
                                                داشته باشند.
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end explanatin tab -->

                        <!-- question tab-->
                        <div class="tab-pane container fade" id="question">
                            <div class="accordion" id="accordionExample">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                                    data-target="#question1" aria-expanded="true"
                                                    aria-controls="question1">
                                                بیمه درمان چیست؟
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="question1" class="collapse show" aria-labelledby="headingOne"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                بیمه درمان شاخه‌ای از بیمه‌های اشخاص است که در آن افراد به صورت گروهی
                                                تحت پوشش بیمه‌ای قرار میگیرند تا در صورت بستری شدن و یا انجام عمل‌های
                                                جراحی، هزینه معالجات مازاد بر سهم بیمه‌گر اول (خدمات درمانی و یا تأمین
                                                اجتماعی)و تا مبلغ معین و توافق‌شده پرداخت شود.</div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question2" aria-expanded="false"
                                                    aria-controls="question2">
                                                موضوع بیمه در بیمه تکمیلی درمان چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question2" class="collapse" aria-labelledby="headingTwo"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph">
                                                <p>
                                                    مطابق آیین‌نامه ۷۴ بیمه مرکزی، موضوع بیمه درمان تکمیلی جبران بخشی از
                                                    هزینه‌های بیمارستانی و جراحی ناشی از بیماری، حادثه و سایر پوشش‌های
                                                    اضافی درمانی بیمه‌شدگان است که در تعهد بیمه‌گر پایه نیست و طی این
                                                    بیمه‌نامه در تعهد بیمه‌گر قرار گرفته است.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question3" aria-expanded="false"
                                                    aria-controls="question3">
                                                تعریف حادثه در بیمه تکمیلی درمان چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question3" class="collapse" aria-labelledby="headingThree"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                هر واقعه ناگهانی که بدون قصد و اراده بیمه‌شده و در اثر یک عامل خارجی
                                                اتفاق افتاده باشد و منجر به جرح، نقص عضو، ازکارافتادگی و یا فوت بیمه‌شده
                                                شود. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFour">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question4" aria-expanded="false"
                                                    aria-controls="question4">

                                                تعریف بیماری در بیمه تکمیلی درمان چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question4" class="collapse" aria-labelledby="headingFour"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                به هر گونه عارضه جسمی و اختلال در عملکرد طبیعی و اعضای مختلف بدن طبق
                                                تشخیص پزشک، بیماری گفته می‌شود.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFive">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question5" aria-expanded="false"
                                                    aria-controls="question5">
                                                پوشش‌ها بیمه تکمیلی درمان شامل چه مواردی است؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question5" class="collapse" aria-labelledby="headingFive"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                <div class="paragraph">
                                            <p dir="RTL"></p>
                                            <ul>
                                                <li>جبران هزینه های بستری و جراحی بیمه شدگان در بیمارستان و
                                                    مراکز جراحی محدود (Day Care) با بیشتر از 6 ساعت بستری.
                                                </li>
                                                <li>آنژیوگرافی قلب و انواع سنگ شکن</li>
                                                <li>جبران هزینه زایمان به صورت طبیعی یا سزارین</li>
                                                <li>پرداخت هزینه های مربوط به جابجایی بیمار با آمبولانس در موارد
                                                    شهری و بین شهری و ...
                                                </li>
                                            </ul>
                                            <p></p></div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSix">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question6" aria-expanded="false"
                                                aria-controls="question6">
                                            هزینه‌های پاراکلینیکی (تشخیصی) شامل چه مواردی است؟

                                        </button>
                                    </h2>
                                </div>
                                <div id="question6" class="collapse" aria-labelledby="headingSix"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="paragraph">
                                            انواع سونوگرافی، ماموگرافی، رادیوتراپی، اکوکاردیوگرافی، انواع اسکن، انواع
                                            آندوسکوپی، تست ورزش، نوارعضله، نوارعصب، نوار مغز، آنژیوگرافی چشم و ... </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSeven">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question7" aria-expanded="false"
                                                aria-controls="question7">

                                            جراحی های مجاز سرپایی شامل چه مواردی است؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question7" class="collapse" aria-labelledby="headingSeven"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="paragraph">
                                            گچگیری، بخیه، ختنه، شکستگی‌ها، کرایوتراپی، اکزیون لیپوم، تخلیه کیست و لیزر
                                            درمانی و ...گچگیری، بخیه، ختنه، شکستگی‌ها، کرایوتراپی، اکزیون لیپوم، تخلیه
                                            کیست و لیزر درمانی و ...
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingEight">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question8" aria-expanded="false"
                                                aria-controls="question8">

                                            هزینه عمل‌های جراحی مهم شامل چه جراحی‌هایی است؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question8" class="collapse" aria-labelledby="headingEight"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="paragraph"><p dir="RTL"></p>
                                            <ul>
                                                <li>جراحی قلب، مغز و اعصاب.</li>
                                                <li>پیوند کلیه و پیوند مغز استخوان، پیوند کبد و پیوند ریه</li>
                                                <li>هزینه رفع عیوب انکساری چشم.</li>
                                            </ul>
                                            <p></p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingNine">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question9" aria-expanded="false"
                                                aria-controls="question9">

                                            موارد خارج از تعهد بیمه تکمیلی چیست؟
                                        </button>

                                    </h2>
                                </div>
                                <div id="question9" class="collapse" aria-labelledby="headingNine"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="paragraph"><p dir="RTL"></p>
                                            <ul>
                                                <li>اعمال جراحی که به منظور زیبایی انجام می گیرد، مگر اینکه ناشی از
                                                    وقوع حوادث بیمه شده در طی مدت بیمه باشد.
                                                </li>
                                                <li>عیوب مادرزادی که قبل از انعقاد قرارداد بیمه مشخص بوده و بیمه گذار
                                                    از آن مطلع شده باشد.
                                                </li>
                                                <li>سقط جنین مگر در موارد ضروری با تشخیص پزشک.</li>
                                                <li>ترک اعتیاد.</li>
                                                <li>خودکشی، قتل و جنایت.</li>
                                                <li>حوادث طبیعی مانند سیل، زلزله و آتشفشان مگر اینکه در شرایط خصوصی به
                                                    نحو دیگری توافق شده باشد.
                                                </li>
                                                <li>جنگ، شورش، اغتشاش، بلوا، اعتصاب، قیام، آشوب، کودتا و اقدامات احتیاطی
                                                    مقامات نظامی و انتظامی.
                                                </li>
                                                <li>فعل و انفعالات هسته ای.</li>
                                                <li>اتاق خصوصی و همراه مگر در موارد ضروری به تشخیص پزشک معالج.</li>
                                                <li>بیماری روانی یا سایکوتیک؛ منظور از بیماری های سایکوتیک آن دسته
                                                    از بیماری های است که بیمار نسبت به بیماری خویش بینش نداشته
                                                    باشد.
                                                </li>
                                                <li>دندانپزشکی مگر جراحی فک به علت وقوع حادثه تحت پوشش.</li>
                                                <li>زایمان برای فرزند چهارم و بیشتر.</li>
                                                <li>وسایل کمک توانبخشی از قبیل جوراب واریس، لنز و سمعک، شکم بند و لوازم
                                                    بهداشتی و آرایشی که جنبه دارویی و درمانی نداشته باشد.
                                                </li>
                                                <li>کلیه موارد مازاد از تعهدات مندرج در بیمه نامه.</li>
                                            </ul>
                                            <p></p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading11">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question11" aria-expanded="false"
                                                aria-controls="question11">
                                            دوره انتظار بیمه تکمیلی درمان چیست؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question11" class="collapse" aria-labelledby="heading11"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="paragraph">
                                            به مدت زمانی از قرارداد بیمه درمان تکمیلی درمان که شرکت بیمه هیچ تعهدی ندارد
                                            و پوشش‌های بیمه‌ای غیرفعال است، دوره انتظار می گویند. </P>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading12">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question12" aria-expanded="false"
                                                aria-controls="question12">
                                            علت وجود دوره‌ انتظار در بیمه تکمیلی درمان چیست؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question12" class="collapse" aria-labelledby="heading12"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="paragraph">
                                            برای کنترل ضریب خسارت در سال اول قرارداد بیمه‌ تکمیلی درمان، دوره‌ انتظار
                                            اِعمال می‌شود. برای اینکه بین ارائه خدمات و حفظ منافع بیمه‌گر و بیمه‌گذار
                                            تعادلی بوجود آید، وجود دوره‌ انتظار، الزامی است. </P>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading13">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question13" aria-expanded="false"
                                                aria-controls="question13">
                                            دوره انتظار به کدام بخش از پوشش‌های بیمه تکمیلی تعلق می‌گیرد؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question13" class="collapse" aria-labelledby="heading13"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="paragraph">
                                            در بیمه‌ تکمیلی انفرادی و گروهی پوشش خدمات پاراکلینیکی سه ماه دوره‌ انتظار
                                            دارد.
                                            <br>
                                            دوره‌ انتظارِ پوشش پاراکلینیکی را هم می‌توان با افزایش تعداد نفرات بیمه‌شده
                                            در بیمه تکمیل درمان گروهی و با توافق با بیمه‌گر، به کم‌تر از 3 ماه کاهش داد.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading14">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question14" aria-expanded="false"
                                                aria-controls="question14">
                                            رابطه دوره انتظار و تعداد افراد گروه در بیمه تکمیلی گروهی چگونه است؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question14" class="collapse" aria-labelledby="heading14"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="paragraph">
                                            دوره انتظار و تعداد افراد گروه در بیمه تکمیلی گروهی رابطه عکس دارد؛ هرچه
                                            تعداد افراد در قرارداد تکمیلی بیشتر باشه دوره انتظار کاهش میابد. </P>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading15">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question15" aria-expanded="false"
                                                aria-controls="question15">
                                            دوره انتظار زایمان در بیمه درمان تکمیلی چقدر است؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question15" class="collapse" aria-labelledby="heading15"
                                     data-parent="#accordionExample">
                                    <div class="card-body">

                                        <div class="paragraph"><p dir="RTL"> از آنجایی که بیمه زایمان به تنهایی وجود
                                                ندارد، این پوشش بعنوان یکی از پوشش های بیمه تکمیلی درمان ( به صورت
                                                گروهی و انفرادی) ارائه می شود.<br></p>
                                            <ul>
                                                <li>این طرح بصورت گروهی دارای دوره انتطار به اَشکال زیر می باشد:
                                                </li>
                                                <li>در گروه های کمتر از پانصد نفر ۹ ماه</li>
                                                <li>در گروه های پانصد تا هزار نفر ۶ ماه</li>
                                                <li>در مورد گروه های بیشتر از هزار نفر فاقد دوره انتظار است.</li>
                                            </ul>
                                            <p></p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading16">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question16" aria-expanded="false"
                                                aria-controls="question16">
                                            مدت دوره انتظار برای بیماری‌های مزمن در بیمه تکمیلی چقدر است؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question16" class="collapse" aria-labelledby="heading16"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="paragraph">
                                            مدت دوره‌ انتظار بیماری‌های مزمن در حالت عادی 3 ماه است. منظور از بیماری‌های
                                            مزمن در بیمه‌ تکمیلی، آن دسته از بیماری‌هایی هستند که سابقه‌ آن‌ها به قبل از
                                            صدور بیمه‌نامه‌ تکمیلی درمان مربوط باشد. </P>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading17">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question17" aria-expanded="false"
                                                aria-controls="question17">
                                            هزینه درمان در بیمه تکمیلی به چه شکل پرداخت می‌شود؟ است؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question17" class="collapse" aria-labelledby="heading17"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="paragraph">
                                            <p class="paragraph">
                                                شرایط در شرکت‌های مختلف بیمه مقداری متفاوت است اما به صورت کلی شرایط
                                                بیمه تکمیلی درمان به شکل زیر است.
                                            </p>
                                            <p class="paragraph">
                                                اگر بیمه‌شده در مراکز و یا بیمارستان‌های طرف قرارداد بیمه‌گر بستری شود،
                                                باید در اسرع وقت به بیمه‌گر اطلاع دهد. بعد از محاسبه سهم بیمه‌گر اول
                                                (تأمین اجتماعی، خدمات درمانی، نیروهای مسلح و ...) و کسر فرانشیز، مابه
                                                التفاوت هزینه بیمارستانی مورد تعهد توسط بیمه‌گر پرداخت می‌شود.
                                            </p>
                                            <p class="paragraph">
                                                در صورتی که بیمه شده در مراکز و بیمارستان‌های طرف قرارداد با بیمه‌گر
                                                بستری نشود، تمام هزینه‌ها و فاکتورهای مربوطه، توسط کارشناسان و پزشک
                                                معتمد بیمه‌گر بررسی شده و بعد از تایید به حساب بیمه‌شده واریز می‌گردد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading18">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question18" aria-expanded="false"
                                                aria-controls="question18">
                                            آیا زالودرمانی تحت پوشش بیمه تکمیلی قرار می‌گیرد؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question18" class="collapse" aria-labelledby="heading18"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="paragraph">
                                            خیر؛ به دلیل اینکه درمان از طریق طب سنتی علی‌الخصوص زالودرمانی، معمولا در
                                            بیمارستان‌ها و درمانگاه‌های مجهز صورت نمی‌گیرد و عمدتا در مراکز غیررسمی و یا
                                            به صورت خانگی صورت می‌گیرد، متاسفانه تحت پوشش بیمه تکمیلی درمان قرار
                                            نمی‌گیرد. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading19">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question19" aria-expanded="false"
                                                aria-controls="question19">
                                            در بیمه تکمیلی منظور از بیمه‌گر اول یا بیمه‌گر پایه چیست؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question19" class="collapse" aria-labelledby="heading19"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="paragraph">
                                            مطابق ماده ۲ آیین‌نامه ۷۴ بیمه مرکزی در مورد بیمه درمان تکمیلی بیمه‌گر پایه
                                            یا بیمه‌گر اول سازمان‌هایی از قبیل سازمان بیمه خدمات درمانی، سازمان تأمین
                                            اجتماعی، سازمان بیمه نیروهای مسلح و... که طبق قانون بیمه درمان همگانی، موظف
                                            به ارائه خدمات درمان پایه‌اند را در بر می‌گیرد. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading20">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question20" aria-expanded="false"
                                                aria-controls="question20">
                                            در بیمه درمان تکمیلی بیمه‌گذار کیست؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question20" class="collapse" aria-labelledby="heading20"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="paragraph">
                                            <p>
                                                مطابق ماده ۲ آیین‌نامه ۷۴ بیمه مرکزی در بیمه تکمیلی درمان، بیمه‌گذار
                                                شخصی است که مشخصات وی در بیمه‌نامه درمان ذکر شده و متعهد به پرداخت
                                                حق‌بیمه است.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading21">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question21" aria-expanded="false"
                                                aria-controls="question21">
                                            پوشش دندانپزشکی بیمه تکمیلی شامل چه مواردی است؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question21" class="collapse" aria-labelledby="heading21"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="paragraph">
                                            پوشش‌های مختلف بیمه تکمیلی طبق توافق بین بیمه‌گذار و بیمه‌گر انتخاب می‌شوند؛
                                            از آنجایی که پوشش دندانپزشکی بیمه تکمیلی پوشش گران‌قیمتی است، ممکن است
                                            کارفرمایان تمایلی به خرید آن داشته باشند و این پوشش باید با توافق بیمه‌شدگان
                                            و بیمه‌گذار خریداری شود.
                                        </p>
                                        <p class="paragraph">
                                            اگر بیمه تکمیل درمان شما پوشش دندانپزشکی را هم شامل شود، شرایط آن منطبق با
                                            شرایط بیمه‌نامه و جدول هزینه‌های بیمه تکمیلی خواهد بود که بیمه‌گر در اختیار
                                            بیمه‌گذار و متعاقبا بیمه‌شدگان قرار می‌گیرد.
                                        </p>
                                        <p class="paragraph">
                                            پوشش دندانپزشکی شامل هزینه‌های دندانپزشکی (بجز ایمپلنت، ارتودنسی، جراحی لثه،
                                            دندان مصنوعی و هر عمل زیبایی بر روی دندان و لثه) است.
                                        </p>
                                        <p class="paragraph">
                                            نکته: در صورتی که جرم‌گیری در خلال درمان دندان صورت بگیرد جزو پوشش
                                            دندانپزشکی بیمه تکمیلی درمان است؛ اما اگر صرفا جرم‌گیری (scaling and
                                            polishing teeth) صورت گیرد زیرمجموعه زیبایی قرار گرفته و بیمه‌گر تعهدی در
                                            جبران آن ندارد.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading22">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question22" aria-expanded="false"
                                                aria-controls="question22">
                                            تعداد بیمه‌شدگان چه تاثیری بر حق بیمه درمان تکمیلی دارد؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question22" class="collapse" aria-labelledby="heading22"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="paragraph"> هرچه تعداد بیمه‌شدگان در یک گروه بیشتر باشد حق بیمه کمتری
                                            پرداخت می‌نمایند، این کاهش حق‌بیمه در گروه‌های بیشتر از ۱۰۰۰ نفر می‌تواند تا
                                            ۵۰ درصد کمتر از حق‌بیمه عادی باشد.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading23">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question23" aria-expanded="false"
                                                aria-controls="question23">
                                            تعداد بیمه شدگان چه تاثیری بر دوره انتظار و شرط سنی بیمه‌شدگان دارد؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question23" class="collapse" aria-labelledby="heading23"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="paragraph">
                                            تعداد بیمه‌شدگان در گروه‌های بیشتر از ۱۰۰۰ نفر می‌تواند دوره انتظار را به
                                            صفر برساند و شرط سنی بیمه‌شدگان را از بین ببرد.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading24">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question24" aria-expanded="false"
                                                aria-controls="question24">
                                            امکان خرید بیمه تکمیلی درمان به‌صورت انفرادی (فقط یک نفر) وجود دارد؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question24" class="collapse" aria-labelledby="heading24"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="paragraph">
                                            درصورتی‌که فردی از طریق ارگان و یا طرح خانوار تحت پوشش بیمه تکمیلی درمان
                                            قرار نگیرد. با ارائه معرفی‌نامه (حاوی تایید اشتغال به کار و پوشش بیمه‌گر
                                            پایه) از محل کار خود می‌تواند تحت پوشش بیمه تکمیلی درمان قرار گیرد.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading25">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question25" aria-expanded="false"
                                                aria-controls="question25">
                                            آیا بیمه تکمیلی هزینه سمعک را پرداخت می‌کند؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question25" class="collapse" aria-labelledby="heading25"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="paragraph">
                                            در صورتی که در طرح انتخابی بیمه‌گذار و در تعهدات بیمه‌گر مندرج در قرارداد
                                            ثبت شده باشد، قابل پرداخت است . </p></div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading26">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button"
                                                data-toggle="collapse"
                                                data-target="#question26" aria-expanded="false"
                                                aria-controls="question26">
                                            آیا هزینه ویزیت و دارو شامل بیمه تکمیلی می‌شود؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question26" class="collapse" aria-labelledby="heading26"
                                     data-parent="#accordionExample">
                                    <div class="card-body">

                                        <p class="paragraph">
                                            مطابق با مفاد قرارداد و با رعایت تعرفه‌های مصوب تحت پوشش است.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading27">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#question27" aria-expanded="false"
                                                aria-controls="question27">
                                            آیا یک نفر می‌تواند در ۲ دو بیمه تکمیلی مختلف داشته باشد؟ آیا هر دو بیمه
                                            خسارت درمان را پرداخت می‌کنند؟

                                        </button>
                                    </h2>
                                </div>
                                <div id="question27" class="collapse" aria-labelledby="heading27"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>
                                            می‌توان همزمان دو بیمه تکمیلی مختلف داشت، اما نمی‌توان همزمان از هر دو
                                            بیمه‌گر هزینه‌های یکسان را دریافت نمود.
                                        </p>
                                        <p>
                                            مثلا شما در ارگان خود تحت پوشش بیمه تکمیلی هستید که پوشش دندانپزشکی ندارد؛ و
                                            به صورت انفرادی اقدام به خرید بیمه تکمیلی با پوشش دندانپزشکی نموده‌اید، در
                                            این حالت هزینه‌های درمانی خود را از بیمه تکمیلی اول و هزینه‌های دندانپزشکی
                                            خود را از بیمه تکمیلی دوم دریافت می‌نمایید.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading28">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#question28" aria-expanded="false"
                                                aria-controls="question28">
                                            آیا دربیمه درمان تکمیلی هزینه جراحی دیسک بیرون زده شده ستون فقرات پرداخت
                                            می‌شود؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question28" class="collapse" aria-labelledby="heading28"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="paragraph"><p dir="RTL"> بله؛ </p>
                                            <ul>
                                                <li> چنانچه براثر حادثه باشد هزینه ها پرداخت می شود.</li>
                                                <li>چنانچه سابقه بیماری قبل از عقد قرارداد بیمه تکمیلی باشد و جزو بیماری
                                                    های
                                                    مزمن محسوب شود دارای دوره انتظار (حداقل 3 ماه) است.
                                                </li>
                                                <li>در بیمه های تکمیلی انفرادی، بیماری های مزمن در تعهد شرکت
                                                    بیمه نیست
                                                </li>
                                            </ul>
                                            <p></p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading29">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#question29" aria-expanded="false"
                                                aria-controls="question29">
                                            در شرایطی که اصل مدارک و صورت حساب‌های بیمارستانی به بیمه‌گر اول تحویل داده
                                            شده، برای دریافت مابه‌التفاوت هزینه‌ها، چه مدارکی را باید بیمه‌گر ارائه داد؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question29" class="collapse" aria-labelledby="heading29"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="paragraph"><p dir="RTL"></p>
                                            <ul>
                                                <li>فتوکپی برابر اصل شده مدارک و صورتحساب توسط بیمه گر اول</li>
                                                <li>رسید مبلغ دریافتی بابت هزینه درمان بیمه شده که به تایید بیمه گر
                                                    اول رسیده باشد یا تصویر چک دریافتی بابت هزینه درمان بیمه شده از
                                                    بیمه گر .
                                                </li>
                                            </ul>
                                            <p></p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading30">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#question30" aria-expanded="false"
                                                aria-controls="question30">
                                            آیا هزینه لوازم مصرفی (مانند انواع پروتزها) که توسط بیمه‌شده از خارج
                                            بیمارستان خریداری می‌شود، قابل پرداخت است؟ می‌شود؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question30" class="collapse" aria-labelledby="heading30"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>
                                            با ارائه فاکتور معتبر و تایید پزشک معالج بر لزوم استفاده از آن پس از تایید
                                            پزشک معتمد بیمه‌گر قابل پرداخت است. (تا سقف معادل قیمت عرف بازار)
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading31">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#question31" aria-expanded="false"
                                                aria-controls="question31">
                                            وظایف پزشک معتمد بیمه‌گر چیست؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question31" class="collapse" aria-labelledby="heading31"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="paragraph"><p dir="RTL"></p>
                                            <ul>
                                                <li>معاینه پزشکی متقاضیان پوشش بیمه ای.</li>
                                                <li>اظهار نظر در خصوص از کارافتادگی موقت و دائم بیمه شدگان.</li>
                                                <li>تعیین میزان نقص عضو بیمه شدگان.</li>
                                                <li>محاسبه و تایید هزینه های قابل پرداخت.</li>
                                                <li>اظهار نظر در خصوص تمام موارد ارجاع شده از سوی کارشناسان بیمه گر.
                                                </li>
                                            </ul>
                                            <p></p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading32">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#question32" aria-expanded="false"
                                                aria-controls="question32">
                                            تا چه زمانی می‌توانم هزینه‌های درمانی خود را از بیمه تکمیلی دریافت کنم؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question32" class="collapse" aria-labelledby="heading32"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>
                                            مدت اعتبار و مهلت دریافت هزینه‌های درمان، در شرکت‌های مختلف متفاوت است و
                                            نهایتا مطابق با شرایط اعلام شده در مفاد قرارداد اعمال خواهد شد.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading33">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#question33" aria-expanded="false"
                                                aria-controls="question33">
                                            نحوه دریافت هزینه عینک به چه صورت است؟
                                        </button>
                                    </h2>
                                </div>
                                <div id="question33" class="collapse" aria-labelledby="heading33"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>
                                            بیمه‌شدگان برای دریافت هزینه عینک با در دست داشتن عینک، فاکتور خرید عینک،
                                            دستور پزشک و نیز تاییدیه اپتومتریست معتمد به مراکز پرداخت خسارت بیمه‌گر
                                            مراجعه نمایند. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end question tab-->

                        <!-- counciler tab-->
                        <div class="tab-pane container fade" id="counciler">


                            <section class="form-elegant">

                                <div class="third-title">
                                    <h4>درخواست مشاوره</h4>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">


                                        <form>
                                            <div class="form-group col-md-8">
                                                <label for="nameInput">نام و نام خانوادگی</label>
                                                <input type="text" class="form-control none-border" id="nameInput"
                                                       placeholder="">
                                            </div>

                                            <div class="form-group col-md-8">
                                                <label for="phoneInput">شماره تماس </label>
                                                <input type="text" class="form-control none-border" id="phoneInput"
                                                       placeholder="">
                                            </div>
                                            <div class="form-group col-md-8">
                                                <label for="description">متن درخواست مشاوره</label>
                                                <textarea id="description" class="form-control none-border"
                                                          style="float: right;">

                                            </textarea>
                                            </div>

                                            <div class="col-md-8  btn ">
                                                <div class="compare-btn btn ">
                                                    <a href="#">ارسال درخواست</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </section>

                        </div>
                        <!--  end counciler tab -->
                    </div>
                    <!--  end tab-content  -->
    </div>
</section>

@include('layouts.footer')

<script type="text/javascript">


    $(".menu-box .menu-list .menu-list-item").removeClass("active");
    $(".menu-box .menu-list #m6").addClass("active");

</script>

<script src="/js/fetchServerPure.js" ></script>

</body>

</html>
