@include('layouts.header')
<div class="content">
    <section>
        <div class="search-section">
            <div class="aboutus">
                <span>بی می تو </span>
                مقایسه، مشاوره و خرید آنلاین بیمه
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-filter" >
                <li class="nav-item lastitem">
                    <a class="nav-link " data-toggle="tab" href="#part2">
                        <i class="fas fa-mobile-alt icon fa-1x">
                        </i>
                        <span> بیمه تجهیزات الکترونیک   </span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active" id="part8">

                    <form id="wizard8" class="wizard slider-form" >
                        <div class="wizard-content">
                            <div id="step1" class="wizard-step">
                                <div class="col-sm-10 col-xs-12 content-step">
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> نوع دستگاه : </label>
                                            <select class="slider-input form-control " id="Device-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> برند : </label>
                                            <select class="slider-input form-control " id="DeviceBrand-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> مدل : </label>
                                            <select class="slider-input form-control " id="DeviceModel-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par">
                                    <button type="button" class="wizard-btn wizard-next btn press-btn">
                                        <i class="fas fa-caret-left"></i>
                                        <span> بعد  </span>
                                    </button>
                                </div>
                            </div>
                            <div id="step2" class="wizard-step ">
                                <div class="col-sm-2 col-xs-12 btns-step prev-par">
                                    <button type="button" class="wizard-btn wizard-prev btn press-btn">
                                        <i class="fas fa-caret-right"></i>
                                        <span> قبلی  </span>
                                    </button>
                                </div>
                                <div class="col-sm-8 col-xs-12 content-step">
                                    <div class="col-lg-6 col-md-12 tab-col ">
                                        <div class="form-group">
                                            <label class="slider-label"> فرانشیز  :  </label>
                                            <select class="slider-input form-control "  id="DeviceFran-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 tab-col ">
                                        <div class="form-group">
                                            <label class="slider-label"> قیمت دستگاه (ریال)  :  </label>
                                            <input type="number" class="slider-input form-control " id="DevicePrice-input" value="00">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                                    <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                                        <span> مقایسه  </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-header">
                            <ul class="nav nav-tabs">
                                <li role="presentation" class="wizard-step-indicator "><a href="#!">
                                        <span class="badge slider-counter">1</span>
                                    </a></li>
                                <li role="presentation" class="wizard-step-indicator "><a href="#!">
                                        <span class="badge slider-counter">2</span>
                                    </a></li>
                            </ul>
                            <div class="slider-line"></div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>

    <div class="Innerloading-box dn">
        <div class="Innerloading-sq"></div>
    </div>

</div>
<section id="elec-insurance-content" class="insurans-box-log">
    <div class="insur-box">
                   <!-- Nav tabs -->
                   <ul class="nav nav-tabs justify-content-center">
                       <li class="nav-item">
                           <a class="nav-link active" data-toggle="tab" href="#explain">
                               معرفی بیمه تجهیزات الکترونیک
                           </a>
                       </li>
                       <li class="nav-item">
                           <a class="nav-link " data-toggle="tab" href="#question">
                               سوالات متداول
                           </a>
                       </li>
                       <li class="nav-item">
                           <a class="nav-link" data-toggle="tab" href="#counciler">
                               درخواست مشاوره
                           </a>
                       </li>
                   </ul>
                   <!-- tab-content -->
                   <div class="tab-content shadow">
                       <!-- explanatin tab -->
                       <div class="tab-pane active" id="explain">
                           <div class="container">
                               <div class="row">
                                   <div class="col-md-12">
                                       <div class="third-title">
                                           <h4>بیمه شخص ثالث چیست؟</h4>
                                       </div>
                                       <div class="row ">
                                           <div class="col-md-12 third-content">
                                               <p class="paragraph">
                                                   هرگونه خسارت مالی و جانی وارد شده به اشخاص ثالث در حوادث رانندگی به عهده
                                                   بیمه شخص ثالث است. بر اساس قانون، خرید بیمه
                                                   شخص ثالث برای تمام خودروها اجباری است و نداشتن این بیمه عواقب جبران
                                                   ناپذیری برای دارندگان وسائل نقلیه به همراه خواهد
                                                   داشت.
                                               </p>
                                               <p class="paragraph">
                                                   در قرارداد بیمه شخص ثالث:
                                               </p>
                                               <div class="paragraph">
                                                   <ul>
                                                       <li>شخص اول صاحب بیمه نامه و مالک خودرو است که به آن شخص بیمه گذار
                                                           گفته می شود.
                                                       </li>
                                                       <li>شخص دوم یا ثانی شرکت ارائه دهنده بیمه شخص ثالث است که
                                                           بیمه گر خوانده می شود.
                                                       </li>
                                                       <li>شخص سوم یا ثالث تمام افرادی هستند که در حوادث مرتبط با خودرو،
                                                           زیان جانی یا مالی دیده باشند.
                                                       </li>
                                                   </ul>
                                               </div>
                                               <p class="paragraph">
                                                   البته در تعریف این بیمه راننده مقصر حادثه، شخص ثالث محسوب نمی شود
                                                   و برای جبران خسارت های جانی راننده مقصر، بیمه
                                                   حوادث راننده به همراه بیمه نامه شخص ثالث ارائه می شود.
                                               </p>
                                               <p class="paragraph">
                                                   لازم به ذکر است که بیمه حوادث راننده فقط خسارت های جانی وارد شده به
                                                   راننده را جبران می کند و در برابر خسارت های مالی
                                                   هیچگونه پوششی ارائه نمی دهد.
                                               </p>
                                               <div class="third-title">
                                                   <h4> چه مواردی تحت پوشش بیمه شخص ثالث قرار می‌گیرد؟</h4>
                                               </div>
                                               <div class="third-content">
                                                   <p class="paragraph">
                                                       پوشش های بیمه شخص ثالث به دو دسته پوشش مالی و
                                                       جانی تقسیم می شود که پوشش جانی
                                                       این
                                                       بیمه شامل غرامت فوت، نقص
                                                       عضو و هزینه های درمانی افراد آسیب دیده در
                                                       حوادث
                                                       رانندگی است و در پوشش مالی آن خسارت های مالی
                                                       وارد
                                                       شده به خودرو
                                                       یا اموال افراد زیان دیده (به جز راننده مقصر) جبران
                                                       می شود.
                                                   </p>
                                                   <p class="paragraph">
                                                       سقف پرداخت هزینه ها در پوشش جانی بر اساس مبلغ دیه
                                                       در
                                                       ماه حرام تعیین می گردد. سقف پوشش
                                                       مالی
                                                       نیز در شرکت های
                                                       مختلف بیمه متفاوت است اما حداقل پوشش مالی تعیین شده توسط بیمه مرکزی
                                                       در
                                                       سال
                                                       <b>۹۸، ۹</b>
                                                       میلیون تومان می باشد.
                                                   </p>
                                                   <p class="paragraph">
                                                       حداکثر پوشش مالی در بیشتر شرکت های بیمه معادل
                                                       <b>۳۰ میلیون تومان</b>
                                                       است و در بعضی از شرکت ها مثل بیمه شخص ثالث ایران در
                                                       شرایط خاص تا
                                                       <b>۱۵۰ میلیون تومان</b>
                                                       هم می رسد.
                                                   </p>
                                               </div>
                                               <div class="third-title">
                                                   <h4>
                                                       مبلغ دیه کامل چقدر است؟ ماه‌های حرام کدامند؟
                                                   </h4>
                                               </div>
                                               <div class="third-content">
                                                   <p class="paragraph">
                                                       مبلغ دیه در ابتدای هر سال توسط بیمه مرکزی اعلام می گردد.
                                                       <br>
                                                       مبلغ دیه در سال 1398 و برای ماه های عادی 270 میلیون و برای ماه های
                                                       حرام 360 میلیون تومان اعلام شده است.
                                                       <br>
                                                       ماه های حرام به ترتیب محرم، رجب، ذی القعده و ذی الحجه
                                                       هستند.
                                                       <br>
                                                       سقف پوشش جانی به اندازه ی مبلغ دیه در ماه حرام (360 میلیون
                                                       تومان) است و حداقل پوشش مالی به اندازه یک چهلم مبلغ دیه
                                                       ماه حرام (9 میلیون تومان) در نظر گرفته می شود.
                                                       <br>
                                                       خسارت جانی راننده مقصر هم بر اساس دیه ماه غیر حرام (ماه عادی) پرداخت
                                                       می گردد.
                                                   </p>
                                               </div>
                                               <div class="third-title">
                                                   <h4>
                                                       نرخ بیمه شخص ثالث 98
                                                   </h4>
                                               </div>
                                               <div class="third-content">
                                                   <p class="paragraph">
                                                       همانطور که در مطالب فوق اشاره کردیم، حق بیمه پایه شخص ثالث هر ساله
                                                       توسط بیمه مرکزی تعیین و به شرکت های بیمه ابلاغ
                                                       می گردد. نرخ بیمه شخص ثالث سال 98 که انتهای سال گذشته اعلام شد،
                                                       نسبت به سال گذشته (سال
                                                       <span>۹۷</span>
                                                       ) افزایش قیمت
                                                       <span>۱۵</span>
                                                       درصدی داشته است که دلیل اصلی این افزایش قیمت را
                                                       می توان افزایش نرخ دیه سال
                                                       <span>۹۸</span>
                                                       دانست.

                                                       <br>
                                                       در جدول زیر نرخ بیمه شخص ثالث
                                                       <span>۹۸</span>
                                                       در وسایل نقلیه مختلف و بر اساس تعداد سیلندر آن نشان داده شده است.
                                                   </p>
                                               </div>
                                               <!--table-->
                                               <div style="overflow-x: auto;">
                                                   <table class="table table-striped  table-bordered ">
                                                       <thead>
                                                       <tr>
                                                           <th scope="col">نوع وسیله نقلیه</th>
                                                           <th scope="col">نرخ بیمه سال ۹۸ (ریال)</th>
                                                       </tr>
                                                       </thead>
                                                       <tbody>
                                                       <tr>
                                                           <td>سواری شخصی کمتراز ۴ سیلندر(بیمه ایران )</td>
                                                           <td>۱۴,۳۰۷,۳۴۰</td>
                                                       </tr>
                                                       <tr>
                                                           <td>سواری شخصی کمتراز ۴ سیلندر(بیمه کوثر )</td>
                                                           <td>۱۳,۹۴۹,۵۶۷</td>
                                                       </tr>
                                                       <tr>
                                                           <td>سواری شخصی سیلندر پیکان، پراید (بیمه ایران )</td>
                                                           <td>۱۶,۵۶۳,۶۴۰</td>

                                                       </tr>
                                                       <tr>
                                                           <td>سواری شخصی سیلندر پیکان، پراید (بیمه کوثر )</td>
                                                           <td>۱۶,۱۴۹,۵۴۹</td>
                                                       </tr>
                                                       <tr>
                                                           <td> سایر خودرو های سواری ۴ سیلندر (بیمه ایران )</td>
                                                           <td>۱۹,۱۰۹,۸۸۰</td>

                                                       </tr>
                                                       <tr>
                                                           <td>سایر خودرو های سواری ۴ سیلندر (بیمه کوثر )</td>
                                                           <td>۱۸,۶۳۲,۱۳۳</td>
                                                       </tr>
                                                       <tr>
                                                           <td> سواری شخصی بیش از ۴ سیلندر (بیمه ایران )</td>
                                                           <td>۲۱,۱۴۰,۵۵۰</td>

                                                       </tr>
                                                       <tr>
                                                           <td>سواری شخصی بیش از ۴ سیلندر (بیمه کوثر )</td>
                                                           <td>۲۰,۶۱۲,۰۳۶</td>
                                                       </tr>
                                                       <tr>
                                                           <td> موتور۱ سیلندر (بیمه ایران )</td>
                                                           <td>۴,۸۰۳,۶۳۰</td>
                                                       </tr>
                                                       <tr>
                                                           <td>موتور ۱ سیلندر (بیمه کوثر )</td>
                                                           <td>۴,۶۸۳,۵۳۹</td>
                                                       </tr>
                                                       <tr>
                                                           <td> بارکش ۱ تن (بیمه ایران )</td>
                                                           <td>۱۸,۵۳۵,۴۵۰</td>
                                                       </tr>
                                                       <tr>
                                                           <td>بارکش ۱ تن (بیمه کوثر )</td>
                                                           <td>۱۸,۰۷۲,۰۴۶</td>
                                                       </tr>
                                                       <tr>
                                                           <td> بارکش ۱ تا ۳ تن (بیمه ایران )</td>
                                                           <td>۲۱,۵۹۷,۲۶۰</td>
                                                       </tr>
                                                       <tr>
                                                           <td>بارکش ۱ تا ۳ تن (بیمه کوثر )</td>
                                                           <td>۲۱,۰۵۷,۳۲۹</td>
                                                       </tr>
                                                       <tr>
                                                           <td> بارکش ۳ تا ۵ تن (بیمه ایران )</td>
                                                           <td>۲۶,۳۹۹,۸۰۰</td>
                                                       </tr>
                                                       <tr>
                                                           <td>بارکش ۳ تا ۵ تن (بیمه کوثر )</td>
                                                           <td>۲۵,۷۳۹,۸۰۵</td>
                                                       </tr>
                                                       <tr>
                                                           <td> بارکش ۵ تا ۱۰ تن (بیمه ایران )</td>
                                                           <td>۳۲,۸۲۹,۷۱۰</td>
                                                       </tr>
                                                       <tr>
                                                           <td>بارکش ۵ تا ۱۰ تن (بیمه کوثر )</td>
                                                           <td>۳۲,۰۰۸,۹۶۷</td>
                                                       </tr>
                                                       <tr>
                                                           <td> بارکش ۱۰ تا ۱۰ تن (بیمه ایران )</td>
                                                           <td>۳۷,۶۲۳,۵۳۰</td>
                                                       </tr>
                                                       <tr>
                                                           <td>بارکش ۱۰ تا ۲۰ تن (بیمه کوثر )</td>
                                                           <td>۳۶,۶۸۲,۹۴۲</td>
                                                       </tr>
                                                       <tr>
                                                           <td> بارکش ۲۱ تن به بالا(بیمه ایران )</td>
                                                           <td>۳۹,۶۲۲,۹۲۰</td>
                                                       </tr>
                                                       <tr>
                                                           <td>بارکش ۲۱ تن به بالا (بیمه کوثر )</td>
                                                           <td>۳۸,۶۷۱,۳۴۷</td>
                                                       </tr>

                                                       </tbody>
                                                   </table>
                                               </div>
                                               <!--end table-->
                                               <div class="third-title">
                                                   <h4>محاسبه بیمه شخص ثالث</h4>
                                               </div>
                                               <div class="third-content">
                                                   <p class="paragraph">
                                                       برای تعیین قیمت نهایی بیمه شخص ثالث، عوامل و ملاک های مختلفی را باید
                                                       در نظر گرفت که در ادامه به بررسی این عوامل
                                                       خواهیم پرداخت:
                                                   </p>
                                                   <div class="paragraph">
                                                       <ol>

                                                           <li>
                                                               <h3 class="sub-title">
                                                                   <b>
                                                                       ۱- حق بیمه پایه ابلاغ شده توسط بیمه
                                                                       مرکزی</b>
                                                               </h3>
                                                           </li>
                                                           بیمه مرکزی هر ساله حداقل و حداکثر مبلغ حق بیمه شخص ثالث را به
                                                           شرکت های بیمه ابلاغ می کند که این مبلغ بر اساس
                                                           میزان دیه همان سال تعیین می شود.
                                                           <br>
                                                           میزان دیه کامل در ماه حرام و ماه غیر حرام نیز هر ساله توسط قوه
                                                           قضائیه اعلام می گردد که بر همین اساس در سال
                                                           ۹۸ دیه کامل در ماه حرام 360 میلیون تومان و در ماه غیر حرام ۲۷۰
                                                           میلیون تومان تعیین شده است.

                                                           <br>
                                                           <br>
                                                           <li><h3 class="sub-title">
                                                                   <b>۲- تخفیف بیمه شخص ثالث (تخفیف عدم
                                                                       خسارت)</b>
                                                               </h3></li>
                                                           یکی از عوامل مهم تاثیر گذار بر قیمت نهایی بیمه شخص ثالث، تخفیف
                                                           عدم خسارت بیمه است؛ به این معنی که به تعداد
                                                           سال هایی که بیمه گذار از بیمه ثالث خود استفاده نکرده باشد، به
                                                           بیمه نامه او تخفیف عدم خسارت تعلق
                                                           می گیرد. در این صورت، زمانی که شخص بیمه گذار اقدام به تمدید بیمه
                                                           نامه خود نماید، تخفیف تعداد سال های عدم خسارت
                                                           بر حق بیمه پرداختی جدید آن اعمال می شود و مبلغ آن کاهش پیدا می
                                                           کند.
                                                           <br>
                                                           برای مثال اگر شخصی ۱ سال از بیمه شخص ثالث خود استفاده نکرده باشد
                                                           و در تصادفی مقصر شناخته نشده باشد، برای تمدید
                                                           بیمه نامه شخص ثالث خود در سال آینده مشمول ۵% تخفیف حق بیمه می
                                                           شود. به همین ترتیب با افزایش تعداد سال های عدم
                                                           خسارت، درصد تخفیف ثالث نیز افزایش می یابد.
                                                           <br>
                                                           در قانون جدید تخفیف بیمه شخص ثالث این تخفیفات به جای ۸ سال، در
                                                           ۱۵ سال اعمال می شود. نحوه اعمال تخفیف عدم خسارت
                                                           در قانون جدید در جدول زیر قابل مشاهده است:

                                                           <br>
                                                           <br>
                                                       </ol>

                                                       <table class="table table-striped  table-bordered ">
                                                           <thead>
                                                           <th>
                                                               تخفیف عدم خسارت
                                                           </th>
                                                           <th>
                                                               میزان تخفیف به درصد
                                                           </th>
                                                           </thead>
                                                           <tr>
                                                               <td>
                                                                   سال اول تخفیف
                                                               </td>
                                                               <td>
                                                                   ۵٪
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td>
                                                                   سال دوم تخفیف
                                                               </td>
                                                               <td>
                                                                   ۱۰٪
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td>
                                                                   سال سوم تخفیف
                                                               </td>
                                                               <td>
                                                                   ۱۵٪
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td>
                                                                   سال چهارم تخفیف
                                                               </td>
                                                               <td>
                                                                   ۲۰٪
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td>
                                                                   سال پنجم تخفیف
                                                               </td>
                                                               <td>
                                                                   ۲۵٪
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td>
                                                                   سال ششم تخفیف
                                                               </td>
                                                               <td>
                                                                   ۳۰٪
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td>
                                                                   سال هفتم تخفیف
                                                               </td>
                                                               <td>
                                                                   ۳۵٪
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td>
                                                                   سال هشتم تخفیف
                                                               </td>
                                                               <td>
                                                                   ۴۰٪
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td>
                                                                   سال نهم تخفیف
                                                               </td>
                                                               <td>
                                                                   ۴۵٪
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td>
                                                                   سال دهم تخفیف
                                                               </td>
                                                               <td>
                                                                   ۵۰٪
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td>
                                                                   سال یازدهم تخفیف
                                                               </td>
                                                               <td>
                                                                   ۵۵٪
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td>
                                                                   سال دوازدهم تخفیف
                                                               </td>
                                                               <td>
                                                                   ۶۰٪
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td>
                                                                   سال سیزدهم تخفیف
                                                               </td>
                                                               <td>
                                                                   ۶۵٪
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td>
                                                                   سال چهاردهم تخفیف
                                                               </td>
                                                               <td>
                                                                   ۷۰٪
                                                               </td>
                                                           </tr>
                                                           </tbody>
                                                       </table>
                                                   </div>


                                                   <h4 class="sub-title m-0 mb-1">کاهش پلکانی تخفیف در صورت بروز
                                                       خسارت</h4>
                                                   در قانون قبلی بیمه ثالث با اولین خسارت و کنده شدن کوپن بیمه
                                                   نامه، تمام تخفیف سال های قبلی از بین می رفت؛ اما در
                                                   قانون جدید بیمه شخص ثالث شرایط تغییر کرده و کاهش تخفیفات به صورت
                                                   پلکانی است. طبق تبصره ۲ ماده ۶ آیین نامه جدید
                                                   تخفیف عدم خسارت، در صورت خسارت تمام تخفیفات بیمه گذار به یک باره
                                                   از بین نمی رود. در جدول زیر می توانید تاثیر
                                                   خسارت مالی و جانی را بر کاهش تخفیفات مشاهده کنید:


                                                   <br>
                                                   <br>

                                                   <div class="table-responsive">
                                                       <table class="table table-striped  table-bordered ">
                                                           <thead>
                                                           <tr>
                                                               <th>
                                                                   تعداد خسارت
                                                               </th>
                                                               <th>
                                                                   یک بار
                                                               </th>
                                                               <th>
                                                                   دو بار
                                                               </th>
                                                               <th>
                                                                   سه بار و بیشتر
                                                               </th>
                                                           </tr>
                                                           </thead>
                                                           <tbody>
                                                           <tr>
                                                               <td>
                                                                   مالی
                                                               </td>
                                                               <td>
                                                                   ۲۰ درصد
                                                               </td>
                                                               <td>
                                                                   ۳۰ درصد
                                                               </td>
                                                               <td>
                                                                   ۴۰ درصد
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td>
                                                                   جانی
                                                               </td>
                                                               <td>
                                                                   ۳۰ درصد
                                                               </td>
                                                               <td>
                                                                   ۷۰ درصد
                                                               </td>
                                                               <td>
                                                                   ۱۰۰ درصد
                                                               </td>
                                                           </tr>
                                                           </tbody>
                                                       </table>
                                                   </div>


                                                   <li><h4 class="sub-title"><b>
                                                               ۳- جریمه دیرکرد تمدید بیمه نامه ثالث
                                                           </b></h4>
                                                   </li>
                                                   یکی از عوامل تاثیرگذار بر نرخ حق بیمه ثالث، جریمه دیرکرد تمدید
                                                   بیمه نامه است؛ به این شکل که در صورت عدم تمدید به
                                                   موقع بیمه نامه ، حق بیمه شخص ثالث افزایش خواهد یافت.
                                                   <br>
                                                   جریمه دیرکرد به صورت روزانه محاسبه می شود و مبلغ آن به نوع خودرو
                                                   (تعداد سیلندر آن) بستگی دارد.
                                                   <br>
                                                   حداکثر میزان جریمه دیرکرد برای یک خودرو، معادل مبلغ جریمه دیرکرد
                                                   ۳۶۵ روز است؛ یعنی اگر خودرویی چند سال هم
                                                   بیمه نامه نداشته باشد فقط جریمه دیرکرد یک سال به حق بیمه او
                                                   اضافه خواهد شد.


                                                   <br>

                                                   <li><h4 class="sub-title"><b>
                                                               ۴- کاربری خودرو
                                                           </b></h4></li>
                                                   نوع کاربری خودرو یکی از عوامل مهم تاثیر گذار بر میزان ریسک خودرو
                                                   و در نهایت قیمت حق بیمه ثالث است. مثلا خودروهای
                                                   آموزش رانندگی یا تاکسی ها، نسبت به خودروهای شخصی در معرض خطرات و
                                                   حوادث رانندگی بیشتری هستند و باید حق بیمه
                                                   شخص ثالث بیشتری را پرداخت کنند. کاربری هایی که باعث افزایش حق
                                                   بیمه شخص ثالث می شوند شامل:

                                                   <ul>
                                                       <li>کاربری صنعتی</li>
                                                       <li>آمبولانس</li>
                                                       <li>آتش نشانی</li>
                                                       <li>تاکسی درون شهری و برون شهری</li>
                                                       <li>بارکش و …</li>
                                                   </ul>

                                                   <li><h4 class="sub-title">
                                                           <b>
                                                               ۵- میزان پوشش بیمه نامه شخص ثالث
                                                           </b>
                                                       </h4>
                                                   </li>
                                                   میزان پوشش مالی بیمه نامه، میزان تعهد مالی شرکت بیمه را در مقابل
                                                   بیمه گذار مشخص می نماید. بیمه مرکزی حداقل
                                                   میزان پوشش مالی را برای سال ۹۸، مبلغ ۹ میلیون تومان تعیین کرده
                                                   است. بیمه گذار با پرداخت حق بیمه بیشتر، می تواند
                                                   پوشش مالی بالاتری را خریداری کرده و از خدمات بیشتری بهره مند
                                                   شود.
                                                   <br>
                                                   حداکثر پوشش مالی بیمه نامه ثالث در بسیاری از شرکت های بیمه مبلغ
                                                   ۳۰ میلیون تومان است. البته بعضی از
                                                   شرکت های بیمه پوشش های مالی بالاتری نیز ارائه می دهند.

                                                   <br>
                                                   <li><h4 class="sub-title">
                                                           <b>
                                                               ۶- تخفیف شرکت بیمه
                                                           </b>
                                                       </h4></li>
                                                   شرکت های بیمه می توانند با استفاده از تخفیف های مجاز، قیمت پایین
                                                   تری را به بیمه گذاران ارائه دهند. اما حتی
                                                   با وجود اعمال تخفیف بیمه شخص ثالث، حق بیمه دریافتی نباید از
                                                   حداقل حق بیمه تعیین شده توسط بیمه مرکزی کمتر باشد.

                                                   <br>
                                                   <li><h4 class="sub-title"><b>
                                                               ۷- مدت اعتبار بیمه نامه
                                                           </b></h4></li>
                                                   مدت اعتبار بیمه نامه شخص ثالث می تواند از چند روز تا یک سال باشد
                                                   اما می توان گفت که خرید بیمه شخص ثالث به
                                                   صورت سالانه مقرون به صرفه تر است. به این دلیل که با توجه به بالا
                                                   بودن ریسک بیمه های کوتاه مدت برای
                                                   شرکت های بیمه، قیمت بیمه نامه کوتاه مدت از بیمه نامه یک ساله
                                                   بیشتر خواهد بود.

                                                   <br>
                                                   باید توجه داشت که بیمه نامه شخص ثالث از ساعت ۲۴ روز خریداری فعال
                                                   می شود. مثلا اگر روز شنبه اقدام به خرید یا
                                                   تمدید بیمه نامه کنید، بیمه نامه شما از ساعت ۲۴ همان روز یا به
                                                   عبارتی از ابتدای بامداد یکشنبه فعال خواهد بود.

                                                   <br>
                                                   <li><h4 class="sub-title">
                                                           <b>
                                                               ۸- سال ساخت و عمر خودرو
                                                           </b>
                                                       </h4></li>
                                                   با توجه به افزایش ریسک خودروهای کهنه، در تمام شرکت های بیمه نرخی
                                                   به عنوان ضریب کهنگی در نظر گرفته می شود. این
                                                   نرخ معمولا برای خودروهایی با عمر بیش از ۱۵ سال محاسبه شده و به
                                                   ازای هر سال ۲ درصد به مبلغ بیمه نامه اضافه
                                                   می گردد. حداکثر این ضریب برای ده سال کهنگی و معادل ۲۰ درصد خواهد
                                                   بود.
                                                   <br>
                                                   البته برخی شرکت های بیمه به طور کلی خودروهایی با سال ساخت بالا
                                                   را تحت پوشش بیمه شخص ثالث قرار نمی دهند و از فروش
                                                   بیمه نامه به این خودروها خودداری می کنند.


                                                   </ol>

                                               </div>
                                               <div class="third-title">
                                                   <h4>استعلام بیمه شخص ثالث</h4>
                                               </div>
                                               <div class="third-content">
                                                   <p class="paragraph">
                                                       اگر قصد خرید بیمه شخص ثالث را دارید، قبل از خرید بیمه نامه به
                                                       استعلام قیمت آن در شرکت های مختلف بیمه نیاز پیدا
                                                       خواهید کرد.
                                                       <br>
                                                       در حال حاضر برای مقایسه و استعلام قیمت بیمه شخص ثالث روش ساده ای پیش
                                                       روی شماست و دیگر نیازی نیست که به صورت
                                                       حضوری به نمایندگی ها یا شعب شرکت های بیمه مراجعه کنید.
                                                       <br>

                                                       در روش حضوری استعلام بیمه شخص ثالث، باید ساعت ها زمان صرف کنید تا
                                                       بتوانید نرخ و اطلاعات بیمه شخص ثالث را در
                                                       شرکت های مختلف بیمه جمع آوری کنید.
                                                       <br>

                                                       اما در روش اینترنتی می توانید با چند کلیک و در عرض چند دقیقه به
                                                       استعلام دقیق بیمه شخص ثالث خودروی خود دست پیدا
                                                       کنید.
                                                       <br>

                                                       به این صورت که با ورود به سایت بی می تو و ثبت اطلاعات خودروی خود،
                                                       قیمت بیمه شخص ثالث آن را در شرکت های مختلف بیمه
                                                       استعلام گرفته و پس از بررسی دقیق حق بیمه ها، پوشش ها و اطلاعات مورد
                                                       نیاز خود، بیمه شخص ثالث را خریداری کنید.

                                                   </p>
                                               </div>
                                               <div class="third-title">
                                                   <h4>بهترین بیمه شخص ثالث</h4>
                                               </div>
                                               <div class="third-content">
                                                   <p class="paragraph">
                                                       گفتیم که قیمت بیمه شخص ثالث در شرکت های مختلف بیمه تفاوت چندانی
                                                       ندارد اما قیمت این بیمه نباید تنها معیار شما برای
                                                       خرید بیمه ثالث باشد و اگر به دنبال بهترین بیمه شخص ثالث هستید باید
                                                       به عوامل بسیاری در انتخاب آن توجه کنید. عوامل
                                                       مختلفی مثل میزان پوشش مالی ارائه شده، توانگری مالی شرکت بیمه، کیفیت
                                                       پرداخت خسارت، تعداد شعب پرداخت خسارت، سهم از
                                                       بازار و...
                                                       <br>
                                                       برای انتخاب بهترین بیمه شخص ثالث نیاز دارید اطلاعات، شرایط و نرخ های
                                                       تمام شرکت های بیمه را در کنار یکدیگر ببینید و
                                                       بر اساس نیازها و الویت های خود مقایسه کنید. مرکز مشاوره و انتخاب
                                                       بیمه « بی می تو» به شما کمک می کند با چند مرحله ساده و
                                                       وارد نمودن مشخصات خودروی خود بدون هیچ زحمتی نرخ و شرایط برترین شرکت
                                                       های بیمه را در کنار هم ببینید و بهترین انتخاب را
                                                       داشته باشید.
                                                       <br>
                                                       همچنین مشاوران بی می تو در تمام مراحل خرید، شما را برای انتخاب
                                                       بهترین بیمه شخص ثالث راهنمایی خواهند کرد.

                                                   </p>
                                               </div>

                                               <div class="third-title">
                                                   <h4>قانون جدید بیمه شخص ثالث</h4>
                                               </div>
                                               <div class="third-content">
                                                   <p class="paragraph">
                                                       بیمه شخص ثالث در سال ۹۸ نسبت به سال های گذشته تغییر هایی
                                                       داشته است:
                                                   </p>
                                                   <div class="paragraph">
                                                       <ul>
                                                           <li>
                                                               بیمه گذارانی که بیمه نامه خودرا در ابتدای سال شمسی
                                                               دریافت نموده اند، لزومی به دریافت الحاقیه برای آن ها
                                                               وجود
                                                               ندارد.
                                                           </li>
                                                           در قانون جدید بیمه گذار می تواند تخفیف های متعلق
                                                           به خود را از یک خودرو به خودروی دیگر با تعداد سیلندر مشابه
                                                           منتقل کند.

                                                           <li>
                                                               طبق قانون جدید انجام تعهدات شرکت بیمه بدون توجه به جنسیت و
                                                               دین افراد بیمه گذار انجام می پذیرد.
                                                           </li>

                                                           <li>
                                                               برخلاف قانون قبل عواملی مانند سابقه رانندگی پر خطر و نمره
                                                               منفی در تعیین میزان حق بیمه نیز موثر خواهد بود.
                                                           </li>

                                                           <li>
                                                               در قانون بیمه جدید در صورت تقلب افراد و هرگونه اقدام
                                                               متقلبانه برای دریافت خسارت از شرکت بیمه، به حبس تعزیری
                                                               درجه شش و جزای نقدی معادل دو برابر وجوه دریافتی محکوم می
                                                               شوند.
                                                           </li>

                                                           <li>
                                                               در صورتی که طبق گزارش کارشناس تصادفات راهنمایی و رانندگی و
                                                               یا پلیس راه، حادثه به علت تخلف راننده مقصر باشد،
                                                               بیمه گر جایز است بخشی از خسارت را از راننده متخلف
                                                               دریافت کند.
                                                           </li>

                                                           <li>
                                                               شرکت بیمه در صورت تاخیر در پرداخت خسارت به بیمه گذار،
                                                               باید روزی معادل
                                                               <b>نیم هزارم</b>
                                                               مبلغ خسارت علاوه بر پرداخت کل خسارت، به عنوان جریمه پرداخت
                                                               کند.
                                                           </li>

                                                           <li>
                                                               طبق قانون جدید برای خسارت های کمتر از
                                                               <b>90 میلیون ریال</b>
                                                               ، اخذ کروکی برای دریافت خسارت از شرکت بیمه توسط بیمه گذار
                                                               لازم نیست.
                                                           </li>

                                                           <li>
                                                               در صورتی که اثبات شود علت حادثه مربوط به کوتاهی در کار ارگان
                                                               های
                                                               مربوطه مانند مشکلات راه، نبودن یا نقص علائم
                                                               رانندگی و... بوده است، شرکت بیمه می تواند مبلغ
                                                               خسارت را از ارگان مقصر دریافت نماید.
                                                           </li>

                                                           <li>
                                                               در قانون جدید پوشش هایی برای شخص راننده مقصر نیز در نظر
                                                               گرفته شده است.
                                                           </li>

                                                           <li>
                                                               مطابق قانون جدید سقف پرداخت خسارت وارد شده به خودروهای گران
                                                               قیمت
                                                               و لوکس تغییر یافته است.
                                                           </li>

                                                       </ul>
                                                   </div>
                                               </div>
                                               <div class="third-title">
                                                   <h4>حذف الحاقیه بیمه شخص ثالث</h4>
                                               </div>
                                               <div class="third-content">
                                                   <p>
                                                       در قوانین قبلی اگر شروع سال بیمه‌ای شما از ابتدای سال و همزمان با
                                                       تغییر میزان دیه نبود (مثلا سررسید بیمه در آذر ماه بود) باید برای مدت
                                                       بیمه‌شده بر اساس دیه سال قبل، الحاقیه صادر و حق‌بیمه متناسب با دیه
                                                       سال جدید اضافه می‌شد. اما در قوانین جدید نیازی به صدور الحاقیه ثالث
                                                       و پرداخت مبلغ اضافه نیست و پرداخت مابه‌التفاوت آن به عهده صندوق
                                                       تامین خسارت‌های جانی ثالث است.
                                                   </p>
                                               </div>
                                           </div>
                                           <div class="third-title">
                                               <h4>حذف کوپن بیمه ثالث از ابتدای سال 97</h4>
                                           </div>
                                           <div class="third-content">
                                               <p>
                                                   بر اساس قانون جدید از ابتدای سال ۹۷ کوپن بیمه‌نامه شخص ثالث حذف شد. برخی
                                                   شرکت‌های بیمه به صورت کامل آن را حذف نموده‌اند و برخی شرکت‌های بیمه
                                                   کوپن‌ها را شماره گذاری نمی‌کنند. با این وجود تعدادی از شرکت‌‌‌‌های بیمه
                                                   همچنان کوپن‌‌‌ها را به صورت قبل در بیمه‌نامه خود نگه داشته اند و احتمالا
                                                   در آینده‌‌‌‌‌‌ای نزدیک اقدام به حذف آنها خواهند نمود. در کل کوپن کاغذی
                                                   به صورت قانونی از بیمه ثالث حذف شده است و به زودی به جای بیمه‌نامه، کارت
                                                   هوشمندی صادر می‌شود که دارای QR کد است و تمام سوابق و اطلاعات بیمه‌نامه
                                                   از این طریق در دسترس خواهد بود.
                                               </p>
                                               <p>همچنین بهتر است بدانید که بیمه‌نامه ‌‌‌‌‌هایی که کوپن از آن‌‌‌ها حذف شده
                                                   است، برای دریافت خسارت باید از کپی بیمه‌نامه خود استفاده کنند.</p>
                                           </div>
                                       </div>
                                       <div class="third-title">
                                           <h4>تمدید بیمه‌نامه شخص ثالث با یک شرکت بیمه جدید</h4>
                                       </div>
                                       <div class="third-content">
                                           <p>در تمدید بیمه‌نامه شخص ثالث هیچ محدودیتی برای انتخاب شرکت بیمه وجود ندارد. به
                                               این معنی که خریدار می‌تواند بدون توجه به شرکتی که سال گذشته تحت پوشش آن بوده
                                               است در سال جدید بیمه‌نامه خود را (با حفظ تخفیفات عدم خسارت آن) از یک شرکت
                                               تازه خریداری نماید.</p></div>
                                       <div class="third-title">
                                           <h4>بیمه شخص ثالث قسطی</h4>
                                       </div>
                                       <div class="third-content">
                                           <p>
                                               اجباری بودن و پر هزینه بودن بیمه شخص ثالث، باعث شده تا شرکت‌های بیمه روش‌های
                                               متفاوتی را برای ارائه این بیمه در نظر بگیرند تا از این طریق امکان خرید بیمه
                                               شخص ثالث برای همه افراد با شرایط و توان مالی متفاوت فراهم شود. یکی از این
                                               روش‌ها ارائه بیمه شخص ثالث قسطی است.
                                           </p>
                                           <p>با خرید بیمه ثالث به صورت اقساطی، می توانید حق‌بیمه خود را به صورت تدریجی و
                                               با فاصله زمانی پرداخت کنید.
                                           </p>
                                           <p>
                                               به این نکته توجه داشته باشید که بیمه شخص ثالث قسطی با بیمه شخص ثالثی که به
                                               صورت نقدی خریداری می‌شود، از نظر شرایط، اعتبار بیمه‌نامه، کیفیت ارائه خدمات
                                               و نحوه پرداخت خسارت تفاوتی ندارد و تنها تفاوت آن در نحوه پرداخت حق بیمه است.
                                           </p>
                                           <p>در حال حاضر امکان خرید قسطی بیمه شخص ثالث به صورت آنلاین و از طریق سایت بی می
                                               تو
                                               فراهم شده است. شرایط خرید بیمه شخص ثالث قسطی از بی می تو به این صورت است که
                                               باید
                                               ابتدا ۵۰ درصد مبلغ بیمه‌نامه را به صورت نقد پرداخت کنید و مابقی آن را در
                                               قالب ۱ تا ۳ فقره چک و در بازه زمانی ۳ تا ۶ ماهه (به تناسب شرکت بیمه انتخابی)
                                               بپردازید.
                                               این نکته را در نظر داشته باشید که چک‌های ارائه شده باید حتما به صورت چک‌های
                                               صیاد (دسته چک‌های جدید) باشند.</p>
                                       </div>
                                       <div class="third-title">
                                           <h4>خرید اینترنتی بیمه شخص ثالث</h4>
                                       </div>
                                       <div class="third-content">
                                           <p>ورود و گسترش استفاده از اینترنت به صنعت بیمه و الکترونیکی شدن آن، باعث ایجاد
                                               تحولات گسترده و همچنین بهبود کیفیت ارائه خدمات در این صنعت شده است.
                                               خرید اینترنتی بیمه شخص ثالث نسبت به خرید حضوری آن مزایای مختلفی دارد از
                                               جمله: امکان مقایسه کامل و دقیق اطلاعات شرکت‌‌‌‌های بیمه قبل از خرید، قدرت
                                               انتخاب بالا در میان بیمه‌‌‌‌های مختلف، سرعت در مراحل خرید بیمه، صرفه جویی در
                                               زمان و هزینه، امکان دریافت مشاوره تخصصی در تمام مراحل خرید بیمه و ...
                                               در مجموع‌‌‌‌‌ می‌توان گفت که خرید اینترنتی انواع بیمه از جمله بیمه شخص ثالث،
                                               تجربه خریدی ساده، مطمئن و آگاهانه را برای شما به همراه خواهد داشت.</p>
                                       </div>
                                       <div class="third-title">
                                           <h4>نحوه خرید بیمه شخص ثالث از سایت بی می تو</h4>
                                       </div>
                                       <div class="third-content">
                                           <ul>
                                               <li>مشخصات خودروی خود را در فیلدهای مشخص شده وارد ‌کنید. مشخصاتی مثل: نوع
                                                   خودرو، مدل خودرو، سال ساخت خودرو، سابقه تخفیف عدم خسارت، تاریخ سررسید
                                                   بیمه‌نامه قبلی و...
                                               </li>
                                               <li>
                                                   در مرحله بعد‌‌‌‌‌ می‌توانید نرخ و شرایط بیمه شخص ثالث خودروی خود را در
                                                   برترین شرکت‌‌‌‌های بیمه کشور مشاهده و با هم مقایسه کنید و بر اساس نیازها
                                                   و اولویت‌‌‌‌های خود یکی از بیمه‌‌‌ها را انتخاب کنید.
                                                   <p>
                                                       پس از تأیید اطلاعات در مرحله بعد باید اطلاعات شخصی مثل نام و نام
                                                       خانوادگی، کد ملی، آدرس و ... را وارد کنید و سپس عکس پشت و روی کارت
                                                       خودرو و بیمه‌نامه قبلی خود را ارسال کنید.
                                                   </p></li>
                                               <li>
                                                   در مرحله نهایی‌‌‌‌‌ می‌توانید نحوه پرداخت حق بیمه (پرداخت آنلاین یا
                                                   پرداخت در محل) را انتخاب کرده و هزینه بیمه‌نامه خود را به یکی از این دو
                                                   روش پرداخت کنید.
                                               </li>
                                               <li>
                                                   بیمه‌نامه شما در کمترین زمان ممکن صادر شده و به صورت رایگان به آدرس مورد
                                                   نظرتان ارسال خواهد شد.
                                               </li>
                                           </ul>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <!-- question tab-->
                       <div class="tab-pane fade" id="question">
                           <div class="accordion" id="accordionExample">
                               <div class="card">
                                   <div class="card-header" id="headingOne">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link" type="button" data-toggle="collapse"
                                                   data-target="#question1" aria-expanded="true" aria-controls="question1">
                                               چرا باید بیمه شخص ثالث داشته باشیم؟
                                           </button>
                                       </h5>
                                   </div>

                                   <div id="question1" class="collapse show" aria-labelledby="headingOne"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               تصور کنید شما در هنگام رانندگی با اتومبیل و یا موتور سیکلت به شخص یا اشخاصی
                                               خسارت وارد کنید، و در آن حادثه مقصر شناخته شوید، طبق قانون شما مسئول جبران
                                               این خسارت به زیان‌دیدگان می‌باشید. بنابراین نیاز به بیمه ای دارید تا در
                                               هنگام بروز این حوادث در جبران خسارت پشتوانه شما باشد. ممکن است این خسارت
                                               مالی و یا خسارت بدنی باشد.
                                           </p>
                                           <p>بیمه شخص ثالث در واقع یک بیمه مسئولیت می‌باشد (بیمه مسئولیت مدنی دارندگان
                                               وسایل نقلیه موتوری زمینی) که زیر گروه بیمه های اتومبیل قرار دارد.</p>
                                           <p>بر اساس قانون مالک خودرو یا کسی که خودرو در اختیار اوست باید اقدام به تهیه
                                               بیمه شخص ثالث نماید. (در این صورت این شخص بیمه گذار نامیده میشود)</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="headingTwo">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question2" aria-expanded="false"
                                                   aria-controls="question2">
                                               منظور از خسارت بدنی در بیمه شخص ثالث چیست؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question2" class="collapse" aria-labelledby="headingTwo"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>به هر نوع دیه یا ارش (دیه جراحت) که درصورت بروز آسیب، شکستگی، نقص عضو و
                                               ازکارافتادگی (جزئی یا کلی ـ موقت یا دائم) یا فوت شخص ثالث به دلیل حوادث
                                               مشمول موضوع قانون بیمه اجباری شخص ثالث است، گفته می‌شود؛ بیمه شخص ثالث خودرو
                                               تمامی هزینه‌های درمانی و دیه را در صورتی که مشمول قانون دیگری نباشد، جبران
                                               خواهد کرد.</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="headingThree">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question3" aria-expanded="false"
                                                   aria-controls="question3">
                                               کدام حوادث مشمول قوانین بیمه شخص ثالث است؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question3" class="collapse" aria-labelledby="headingThree"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>حوادث شامل هرسانحه‌ای از قبیل تصادم، تصادف، سقوط، واژگونی، آتش سوزی و یا
                                               انفجار یا هر نوع سانحه است که در اثر وسایل نقلیه و محموله‌های آنها بر اثر
                                               حوادث غیرمترقبه رخ می‌دهد.</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="headingFour">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question4" aria-expanded="false"
                                                   aria-controls="question4">

                                               وظیفه بیمه‌گر هنگام تصادف ناشی از تخلف رانندگی چیست؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question4" class="collapse" aria-labelledby="headingFour"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               بیمه‌گر ملزم به جبران خسارت‌های وارد شده به اشخاص ثالث تا سقف تعهدات مندرج
                                               در بیمه‌نامه خواهد بود. در حوادث رانندگی منجر به جرح یا فوت که به استناد
                                               گزارش کارشناس تصادفات راهنمایی و رانندگی یا پلیس‌راه علت اصلی وقوع تصادف یکی
                                               از تخلفات رانندگی حادثه‌‌ساز باشد، بیمه‌گر موظف است خسارت زیان‌دیده را بدون
                                               هیچ شرطی پرداخت کند و پس از آن می‌تواند جهت بازیافت یک درصد از خسارت‌های
                                               بدنی و دو درصد از خسارت‌های مالی پرداخت شده به مسبب حادثه مراجعه کند؛ علاوه
                                               بر آن در این صورت گواهینامه راننده مسبب حادثه از یک تا سه ماه توقیف می‌شود و
                                               رانندگی در این مدت ممنوع و در حکم رانندگی بدون گواهینامه است.
                                           </p></div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="headingFive">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question5" aria-expanded="false"
                                                   aria-controls="question5">
                                               وظیفه بیمه‌گر هنگام طبیعی نبودن تصادف چیست؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question5" class="collapse" aria-labelledby="headingFive"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               در صورت اثبات عمد راننده در ایجاد حادثه توسط مراجع قضائی و یا رانندگی در
                                               حالت مستی یا استعمال مواد مخدر یا روانگردان مؤثر در وقوع حادثه، یا در صورتی
                                               که راننده مسبب فاقد گواهینامه رانندگی باشد یا گواهینامه او متناسب با نوع
                                               وسیله نقلیه نباشد شرکت بیمه موظف است بدون اخذ تضمین، خسارت زیان‌دیده را
                                               پرداخت کرده و پس از آن می‌تواند به نیابت از زیان‌دیده از طریق مراجع قانونی
                                               برای استرداد تمام یا بخشی از وجوه پرداخت شده به شخصی که موجب خسارت شده است
                                               مراجعه کند.</p></div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="headingSix">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question6" aria-expanded="false"
                                                   aria-controls="question6">

                                               چه خسارت‌هایی که تحت پوشش بیمه شخص ثالث نیستند؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question6" class="collapse" aria-labelledby="headingSix"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <ul>
                                               <li>خسارت وارد شده به وسیله نقلیه مسبب حادثه (راننده مقصر)</li>
                                               <li>خسارت وارد شده به محموله‌های وسیله نقلیه مسبب حادثه</li>
                                               <li>خسارت مستقیم و یا غیرمستقیم ناشی از تشعشعات اتمی و رادیواکتیو</li>
                                               <li>خسارت ناشی از محکومیت جزائی و یا پرداخت جرائم</li>
                                           </ul>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="headingSeven">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question7" aria-expanded="false"
                                                   aria-controls="question7">

                                               ارائه خدمات به وسایل نقلیه فاقد بیمه‌نامه چگونه است؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question7" class="collapse" aria-labelledby="headingSeven"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               ارائه هرگونه خدمات به دارندگان وسایل نقلیه موتوری زمینی فاقد بیمه‌نامه شخص
                                               ثالث معتبر، توسط راهنمایی و رانندگی، دفاتر اسناد رسمی و سازمان‌ها و نهادهای
                                               مرتبط با امر حمل و نقل ممنوع است.
                                           </p>
                                           <p>دفاتر اسناد رسمی مکلفند هنگام تنظیم هرگونه سند در مورد وسایل نقلیه موتوری
                                               زمینی موضوع این قانون، مشخصات بیمه‌نامه شخص ثالث آنها را در اسناد تنظیمی درج
                                               نمایند.</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="headingEight">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question8" aria-expanded="false"
                                                   aria-controls="question8">

                                               عواقب نداشتن و یا دیرکرد تمدید بیمه شخص ثالث چیست؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question8" class="collapse" aria-labelledby="headingEight"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               در صورت دیرکرد تمدید بیمه شخص ثالث، علاوه بر عواقب مالی، مشکلات دیگری نیز
                                               وجود خواهد داشت که موجب صرف زمان زیادی می شود. از عواقب نداشتن بیمه شخص ثالث
                                               می توان به موارد زیر اشاره کرد: </p>
                                           <ul>
                                               <li>در صورت شناسایی خودروی فاقد بیمه شخص ثالث توسط پلیس راهنمایی و رانندگی،
                                                   خودرو متوقف شده و به پارکینگ منتقل می‌شود. در این حالت شخص خاطی باید
                                                   بیمه‌ نامه شخص ثالث دارای اعتبار تهیه کند و باید علاوه بر حق بیمه جدید،
                                                   حق بیمه روزهایی را هم که وسیله نقلیه او بیمه نداشته (تا حداکثر یک سال)
                                                   را بپردازد، و همچنین علاوه بر پرداخت جریمه دیرکرد، هزینه پارکینگ را نیز
                                                   بپردازد.
                                               </li>
                                               <li>در صورت نداشتن بیمه شخص ثالث در حوادث، اگر مالک خودرو مقصر باشد، هیچ
                                                   شرکت بیمه ای خسارت را پرداخت نمی کند، باید کلیه هزینه ها را شخصاً
                                                   بپردازد. طبق قوانین جدید بیمه شخص ثالث، در شرایطی که راننده مقصر بیمه
                                                   شخص ثالث نداشته باشد و در صورت بروز حوادث منجر به فوت یا خسارات بدنی
                                                   دیگر، جبران این گونه خسارت‌ها به عهده صندوق تامین خسارت‌های بدنی است؛ و
                                                   پرداخت دیه را برای راننده مقصر حادثه به صورت قسطی خواهد بود. این قانون
                                                   برای حمایت از اشخاص ثالث زیان‌دیده است.
                                               </li>
                                               <li>
                                                   امکان خرید و فروش خودرویی که تحت پوشش بیمه شخص ثالث نیست وجود ندارد.
                                               </li>
                                           </ul>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="headingNine">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question9" aria-expanded="false"
                                                   aria-controls="question9">


                                               اگر خودرو متعلق به دوست من باشد و بیمه داشته باشد و من راننده آن باشم و
                                               تصادف کنم آیابیمه خسارت زیان‌دیده را پرداخت می‌کند؟
                                           </button>

                                       </h5>
                                   </div>
                                   <div id="question9" class="collapse" aria-labelledby="headingNine"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>بله؛ بیمه شخص ثالث وابسته به خودرو است و در صورتی که شما گواهینامه معتبر
                                               متناسب با وسیله نقلیه داشته باشید، حتی اگر خودرو متعلق به شما نباشد، بیمه
                                               خسارت وارد شده را جبران خواهد نمود.</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading11">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question11" aria-expanded="false"
                                                   aria-controls="question11">
                                               شخص ثالث در بیمه شخص ثالث به چه کسانی گفته می‌شود؟

                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question11" class="collapse" aria-labelledby="heading11"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               به همه کسانی (اشخاص حقیقی یا حقوقی) که توسط وسیله نقلیه موتوری دچار زیان
                                               مالی و یا بدنی شوند به جز خود راننده‌ی مقصر حادثه، شخص ثالث گفته می‌شود.
                                           </p>
                                           <p>اشخاص ثالث شامل جنین داخل رحم و همچنین تمام سرنشین‌های خودرو می‌شود حتی اگر
                                               بیشتر از تعداد نفرات مجاز درج شده بر روی کارت اتومبیل باشد.</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading12">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question12" aria-expanded="false"
                                                   aria-controls="question12">
                                               اگر در یک حادثه به بیشتر از یک نفر زیان جانی برسد چگونه است؟ آیا بیمه دیه
                                               افراد دیگر را هم پوشش میدهد؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question12" class="collapse" aria-labelledby="heading12"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               بله؛ بر اساس قانون شرکت بیمه‌گر مکلف به پرداخت تمام دیات است.</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading13">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question13" aria-expanded="false"
                                                   aria-controls="question13">
                                               آیا دیه زن و مرد، مسلمان و غیر مسلمان برابر است؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question13" class="collapse" aria-labelledby="heading13"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               بله؛ در قوانین جدید بیمه شخص ثالث دیه زن و مرد و همچنین شخص مسلمان و غیر
                                               مسلمان (اهل کتاب) برابر است.</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading14">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question14" aria-expanded="false"
                                                   aria-controls="question14">
                                               بیمه‌نامه شخص ثالث در حکم وثیقه است؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question14" class="collapse" aria-labelledby="heading14"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>دادگاه‌ها موظفند در حوادث رانندگی منجر به خسارت بدنی، بیمه‌نامه شخص ثالثی را
                                               که اصالت آن از سوی شرکت بیمه ذی‌ربط کتباً مورد تأیید قرار گرفته است تا میزان
                                               مندرج در بیمه‌نامه به عنوان وثیقه قبول کنند.</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading15">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question15" aria-expanded="false"
                                                   aria-controls="question15">
                                               چرا هر سال حق بیمه شخص ثالث تغییر می‌کند؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question15" class="collapse" aria-labelledby="heading15"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>حق بیمه شخص ثالث و میزان پوشش مالی و بدنی آن بر اساس دیه کامل یک انسان در ماه
                                               حرام تعیین می‌گردد. این مبلغ در ابتدای هر سال با انتشار بخشنامه‌ای توسط قوه
                                               قضائیه اعلام می‌شود؛ همین موضوع باعث تغییر حق بیمه در هر سال است.</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading16">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question16" aria-expanded="false"
                                                   aria-controls="question16">
                                               آیا حق بیمه شخص ثالث مطابق با قیمت خودرو تغییر می کند؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question16" class="collapse" aria-labelledby="heading16"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               خیر؛ در تعیین حق بیمه شخص ثالث ارزش خودرو تاثیری ندارد. ملاک‌های تعیین حق
                                               بیمه در بیمه‌نامه شخص ثالث عباتند از:
                                           </p>
                                           <ul dir="rtl">
                                               <li>حق بیمه پایه ابلاغ شده توسط بیمه مرکزی (تعداد سیلندر وسیله نقلیه)</li>
                                               <li>تخفیف عدم خسارت بیمه شخص ثالث</li>
                                               <li>کاربری خودرو</li>
                                               <li>جریمه دیرکرد بیمه شخص ثالث</li>
                                               <li>میزان پوشش مالی مورد درخواست</li>
                                               <li>مدت اعتبار بیمه نامه</li>
                                           </ul>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading17">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question17" aria-expanded="false"
                                                   aria-controls="question17">
                                               بیمه حادثه راننده که با بیمه شخص ثالث صادر می شود، چیست؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question17" class="collapse" aria-labelledby="heading17"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               منظور از شخص ثالث، هر شخصی است که به سبب حوادث وسایل نقلیه موتوری دچار
                                               زیان‌های بدنی و مالی شود به استثنای راننده مسبب حادثه. شرکت بیمه، هنگام صدور
                                               بیمه نامه شخص ثالث، با دریافت حق بیمه حوادث راننده، خود راننده را هم تا حد
                                               دیه ماه حرام تحت پوشش بیمه حوادث قرار می‌دهد؛ بیمه حوادث راننده مسئولیت
                                               جبران خسارت‌های فوت و نقص عضو راننده مقصر را به عهده دارد و همچنین شامل
                                               پرداخت هزینه‌‌های درمان راننده است .
                                           </p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading18">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question18" aria-expanded="false"
                                                   aria-controls="question18">
                                               حق بیمه حوادث راننده چقدر است؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question18" class="collapse" aria-labelledby="heading18"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               نرخ حق بیمه حوادث راننده در آیین‌نامه ابلاغ شده به شرکت‌های بیمه (بر اساس
                                               دیه در ماه عادی) تعیین شده است؛ حداکثر حق بیمه پوشش حوادث راننده در هر سال
                                               هنگام صدور بیمه نامه شخص ثالث در بیمه‌نامه درج و به حق بیمه اضافه می‌شود.
                                               این حق بیمه با توجه به نوع کاربری تغییر میابد:
                                           </p>
                                           <p>گروه سواری: ۱.۶۱۷.۰۰۰ ریال</p>
                                           <p>گروه بارکش: ۲.۷۷۲.۰۰۰ ریال</p>
                                           <p>گروه موتور سیکلت : ۸۸۴.۷۰۰ ریال</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading19">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question19" aria-expanded="false"
                                                   aria-controls="question19">
                                               اگر وسیله نقلیه بیمه‌نامه شخص ثالث نداشته باشد و تصادف هم نکند، مشکل چیست؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question19" class="collapse" aria-labelledby="heading19"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>داشتن بیمه نامه شخص ثالث برای همه وسایل نقلیه موتوری (موتور سیلکت و انواع
                                               خودرو) اجباری است؛ حرکت وسایل نقلیه موتوری بدون داشتن بیمه نامه شخص ثالث
                                               ممنوع است </p>
                                           <p>مأموران راهنمایی و رانندگی و پلیس راه وسایل نقلیه بدون بیمه‌نامه را متوقف و
                                               راننده متخلف را ملزم به پرداخت جریمه می‌کنند.</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading20">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question20" aria-expanded="false"
                                                   aria-controls="question20">
                                               در صورت دریافت خسارت از شرکت بیمه، بیمه‌نامه سال بعد به چه شکل تمدید می‌شود؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question20" class="collapse" aria-labelledby="heading20"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               بر روی بیمه نامه شخص ثالث آیتمی تحت عنوان تخفیف عدم خسارت (به اختصار ت.ع.خ)
                                               وجود دارد. خودرویی که در طول مدت بیمه‌نامه خود خسارتی دریافت نکرده باشد، در
                                               زمان تمدید خود شامل تخفیف تشویقی از سمت شرکت بیمه‌گر می‌شود. در صورتی که اگر
                                               خودرویی تصادف کند و از کوپن‌های بیمه‌نامه خود استفاده کند، تخفیف‌های عدم
                                               خسارت او بصورت پلکانی کاهش میابد.
                                           </p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading21">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question21" aria-expanded="false"
                                                   aria-controls="question21">
                                               آیا می‌توان هنگام فروش وسیله نقلیه، تخفیف‌های بیمه‌نامه شخص ثالث را به خودرو
                                               جدید انتقال داد؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question21" class="collapse" aria-labelledby="heading21"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               بله؛ در صورتی که تقاضای بیمه‌گذار قبل از تاریخ انتقال بیمه‌نامه باشد، امکان
                                               انتقال تخفیف‌ها به خودرو جدید وجود دارد. </p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading22">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question22" aria-expanded="false"
                                                   aria-controls="question22">
                                               آیا در بیمه‌نامه شخص ثالث هزینه‌های درمانی نیز پرداخت می‌شود؟

                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question22" class="collapse" aria-labelledby="heading22"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>بر اساس مصوبه مجلس شورای اسلامی، وزارت بهداشت، درمان و آموزش پزشکی مکلف است
                                               مصدومان ناشی از حوادث رانندگی را بدون دریافت وجه پذیرش کند. برای این منظور
                                               شرکت‌های بیمه از محل بیمه‌نامه شخص ثالث 10 درصد عوارض به وزارت بهداشت
                                               می‌پردازند.</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading23">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question23" aria-expanded="false"
                                                   aria-controls="question23">
                                               اگر راننده مقصر حادثه گواهی نامه رانندگی نداشته باشد آیا به راننده دیه تعلق
                                               می‌گیرد؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question23" class="collapse" aria-labelledby="heading23"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               با توجه به این که در مدارک لازم برای پرداخت خسارت داشتن گواهینامه رانندگی
                                               متناسب با نوع کاربری اتومبیل قید شده است امکان پرداخت خسارت وجود ندارد.
                                           </p></div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading24">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question24" aria-expanded="false"
                                                   aria-controls="question24">
                                               ماه‌های حرام كدامند؟
                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question24" class="collapse" aria-labelledby="heading24"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               چهار ماه رجب، ذی القعده، ذی الحجه و محرم ماه‌های حرام هستند. در این ماه‌ها
                                               دیه افزایش می‌یابد.</p></div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading25">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question25" aria-expanded="false"
                                                   aria-controls="question25">
                                               تفاوت ماه‌های حرام با ماه‌های عادی چيست؟

                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question25" class="collapse" aria-labelledby="heading25"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>چنانچه حادثه رانندگی و فوت زيان‌ديده هر دو در ماه‌های حرام اتفاق افتد، بر
                                               اساس قانون مجازات اسلامی، ميزان ديه يک سوم بيشتر از ماه‌های عادی خواهد
                                               بود.</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="card">
                                   <div class="card-header" id="heading26">
                                       <h5 class="mb-0">
                                           <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                   data-target="#question26" aria-expanded="false"
                                                   aria-controls="question26">
                                               آیا بیمه عمر را می‌توان به فرد دیگری انتقال داد؟


                                           </button>
                                       </h5>
                                   </div>
                                   <div id="question26" class="collapse" aria-labelledby="heading26"
                                        data-parent="#accordionExample">
                                       <div class="card-body">
                                           <p>
                                               طبق ماده 13 شرایط عمومی بیمه عمر، یمه‌گذار می‌تواند با موافقت کتبی بیمه‌شده
                                               و بیمه‌گر، با صدور الحاقیه توسط بیمه‌گر، بیمه‌نامه را به بیمه‌گذار دیگری
                                               منتقل نماید. کلیه دیون بیمه‌نامه تا تاریخ انتقال، به عهده انتقال دهنده
                                               میباشد.
                                           </p>
                                           <p>تبصره۱: در صورت انتقال بیمه نامه، بیمه شده قابل تغییر نمی باشد.</p>
                                           <p>تبصره۲: در شرایطی که بیمه گذار و بیمه شده شخص واحدی نباشند، در صورتی که بیمه
                                               نامه بیمه را به دیگری منتقل نماید، چنانچه منتقل الیه تعهدات بیمه گذار به
                                               موجب بیمه نامه بیمه را اجرا کند، بیمه نامه معتبر باقی می ماند. با این حال ،
                                               منتقل الیه می تواند بیمه نامه را طبق شرایط عمومی بیمه نامه فسخ نمایند.</p>
                                           <p>تبصره۳: در صورت انتقال بیمه نامه، اگر منتقل الیه متعدد باشند، هر یك از آنها
                                               نسبت به تمام وجه بیمه در مقابل بیمه گر مسئول خواهد بود.</p>
                                       </div>
                                   </div>
                               </div>


                           </div>
                       </div>
                       <!-- counciler tab-->
                       <div class="tab-pane fade" id="counciler">

                           <section class="form-elegant">

                               <div class="third-title">
                                   <h4>درخواست مشاوره</h4>
                               </div>
                               <div class="row">
                                   <div class="col-md-10 col-md-offset-1">
                                       <form>
                                           <div class="form-group col-md-8">
                                               <label for="nameInput">نام و نام خانوادگی  </label>
                                               <input type="text" class="form-control none-border" id="nameInput" placeholder="">
                                           </div>
                                           <div class="form-group col-md-8">
                                               <label for="phoneInput">شماره تماس   </label>
                                               <input type="text" class="form-control none-border" id="phoneInput" placeholder="">
                                           </div>
                                           <div class="form-group col-md-8">
                                               <label for="description">متن درخواست مشاوره</label>
                                               <textarea id="description" class="form-control none-border"  style="float: right;">
                                                </textarea>
                                           </div>
                                           <div class="col-md-8  btn ">
                                               <div class="compare-btn btn ">
                                                   <a href="#">ارسال درخواست</a>
                                               </div>
                                           </div>
                                       </form>
                                   </div>
                               </div>

                           </section>
                       </div>
                       <!--  end counciler tab -->
                   </div>
                   <!--  end tab-content  -->
    </div>
</section>
@include('layouts.footer')
<script type="text/javascript">


    $(".menu-box .menu-list .menu-list-item").removeClass("active");
    $(".menu-box .menu-list #m8").addClass("active");

</script>
<script src="/js/fetchServerPure.js" ></script>

</body>

</html>
