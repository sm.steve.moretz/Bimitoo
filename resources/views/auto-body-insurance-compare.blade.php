@include('layouts.header')

<!-- content session -->
<section id="auto-body-insurance-compare" class="compare-sec">
    <div class="compare-header">
        <span class="bread-crump"> بی می تو / بیمه بدنه </span>
        <ul class="advertisment">
        </ul>
    </div>
    <div class="compare-cont" id="wizard-compare3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                    <div class="form-box">
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header first-head">
                                    <h5 class="mb-0 card-header-box"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn ">
                                            اطلاعات خودرو
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse show"   data-parent="#accordion">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="form-group">
                                                <label class="compare-label"> نام  وسیله نقلیه : </label>
                                                <select class="compare-input form-control " id="AutoBrands-select" >
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>

                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> مدل وسیله نقلیه : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="AutoModel-select" NameInObj="ModelId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label">  نوع کاربری : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="AutoUsingTypes-select" NameInObj="UsingTypeId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> سال ساخت :  </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="AutoProductionYears-select" NameInObj="ProductionYearID">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="compare-label">عمر خودرو (ماه) :  </label>
                                                <input type="number" class="compare-input form-control " id="AutoLife-select" value="00" NameInObj="MonthId">
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> ارزش خودرو  </label>
                                                <span class="req-star"> * </span>
                                                <input type="number"class="compare-input form-control " id="AutoPrice-select" value="00" NameInObj="Price">
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label">  وارداتی ؟ :  </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="AutoVaredat-select" NameInObj="Imported" >
                                                    <option value="00" > انتخاب کنید  </option>
                                                    <option value="false" > داخلی  </option>
                                                    <option value="true" > وارداتی  </option>
                                                </select>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" >
                                    <h5 class="mb-0 card-header-box" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn " >
                                            اطلاعات بیمه نامه
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="form-group">
                                                <label class="compare-label"> بیمه نامه قبلی  :  </label>
                                                <select class="compare-input form-control "  id="AutoInsuranceStatuses-select">
                                                        <option value="0" > ندارم  </option>
                                                        <option value="1" > دارم  </option>
                                                </select>
                                            </div>
                                            <div class="auto-condition conditional-box dn">
                                                <div class="form-group">
                                                     <label class="compare-label"> سال های عدم خسارت بیمه بدنه  : </label>
                                                    <span class="req-star"> * </span>
                                                     <select class="compare-input form-control " id="AutoNoDisYear-select" NameInObj="CarBodyNoDamageYearId">
                                                            <option value="00" > انتخاب کنید  </option>
                                                     </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="compare-label"> پوشش مالی :  </label>
                                                <select class="compare-input form-control "  id="AutoCoverageTypes-select"  NameInObj="CoverageIds">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" >
                                    <h5 class="mb-0 card-header-box" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn " >
                                            پوشش های اضافی
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseThree" class="collapse"  data-parent="#accordion">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="form-group">
                                                <div id="AutoCoverages-box" class="ExtraCheck-box"  NameInObj="CoverageIds">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header last-head" >
                                    <h5 class="mb-0 card-header-box" data-toggle="collapse" data-target="#collapseFor" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn " >
                                            تخفیف ها
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseFor" class="collapse"  data-parent="#accordion">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="form-group">
                                                <div id="AutoDiscount-box" class="ExtraCheck-box"  NameInObj="DiscountIds">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-12">
                    <div class="result-list">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loading-box dn">
        <div class="loading-sq"></div>
    </div>
    <form action="/checkout" method="post">
        @csrf
        <input name="url" value="api/insurance/insurance/car-body/inquiry">
        <input name="data" value='{"ModelId":1083,"UsingTypeId":1,"ProductionYearID": 2015,"MonthId": 10,"Price": 12000000,"Imported": "false","CarBodyNoDamageYearId": 1}'>
        <input name="duration" value='365'>
        <input name="durationId" value='2'>
        <input name="companyId" value='1024'>
        <input name="price" value='10000'>
        <input name="description" value='بیمه شخص ثالث یکساله ...'>
        <button>Send.......................................</button>
    </form>
</section>
@include('layouts.footer')
<script type="text/javascript">


    $(".menu-box .menu-list .menu-list-item").removeClass("active");
    $(".menu-box .menu-list #m3").addClass("active");

</script>
<script src="/js/fetchServerPure.js" ></script>
<script src="/js/auto-body-compare.js"></script>

</body>

</html>
