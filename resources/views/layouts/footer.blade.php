﻿<!-- footer -->
<section id="footer">
        <div class="container-fluid">
                <div class="footer-box-row row">
                    <div class="col-md-6 col-sm-8 footer-box">
                        <div class="footer-title">
                            <h3>راه های ارتباطی  </h3>
                        </div>
                        <div class="footer-cont">
                            <p>
                                <i class="fa fa-map-marker"></i>
                                آدرس : تهران، سعادت آباد ، بوعلي ، 403 ، واحد 40
                            </p>
                            <p>
                                <i class="fa fa-envelope"></i>
                                info@bimitoo.com
                            </p>
                            <p>
                                <i class="fa fa-phone"></i>
                                021-22120114
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 footer-box">

                        <div class="footer-title">
                            <h3 class="text-right"> اطلاعات بیشتر   </h3>
                        </div>
                        <div class="footer-cont">
                            <p> <a href="/#"> مجله ما  </a> </p>
                        </div>
                        <div class="footer-cont">
                            <p> <a href="/#">  
                            <img id='jxlzesgtjxlzsizpwlaojzpewlao' style='cursor:pointer' onclick='window.open("https://logo.samandehi.ir/Verify.aspx?id=1019474&p=rfthobpdrfthpfvlaodsjyoeaods", "Popup","toolbar=no, scrollbars=no, location=no, statusbar=no, menubar=no, resizable=0, width=450, height=630, top=30")' alt='logo-samandehi' src='https://logo.samandehi.ir/logo.aspx?id=1019474&p=nbpdlymanbpdbsiyshwlyndtshwl'/>
                        </a> </p>
                        </div>

                    </div>
                    <div class="col-md-3 col-sm-12 footer-box">
                        <div class="footer-title">
                            <h3 class="text-right"> اطلاعات بیشتر   </h3>
                        </div>
                        <div class="footer-cont">
                            <p><a href="/about-us"> درباره با ما  </a></p>
                            <p><a href="/rules"> قوانین ما  </a></p>
                            <p><a href="/contact-us"> تماس با ما  </a></p>
                            <p><a href="/collaboration"> همکارای با ما  </a></p>
                        </div>
                    </div>
                </div>
                <div class="footer-text row">
                    <div class="col-12">
                        <h3>کلیه حقوق این سایت متعلق به وب سایت بی می تو می باشد</h3>
                        <h3>   طراحی  و پشتیبانی  : نیک  رایان  </h3>
                    </div>

                </div>
        </div>
</section>

<!-- jquery-3.4.1  -->
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>

<!-- Bootstrap js Ver 4.3.1 -->
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.bundle.min.js"></script>

<!-- form slider -->
<link href="/dist/jquery.simplewizard.css" rel="stylesheet" />
<script src="/dist/jquery.simplewizard.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
<script>
    $(function () {
        $(".wizard").simpleWizard({
            cssClassStepActive: "active",
            cssClassStepDone: "done",
            onFinish: function() {

            }
        });
    });
</script>

<!-- slick slider for insurance list -->
<script src="/js/slick.js"></script>
<script src="/js/slick.min.js"></script>
<script>

    let w=$(window).width();
    var countShow=5;

    if (w<768) {
        countShow=4;
    }
    if (w<550) {
        countShow=3;
    }
     if (w<400) {
        countShow=2;
    }

    $(".insurans-list .compare-bime").slick({
        rtl: true,
        slidesToShow: countShow,
        slidesToScroll: 1,

    });
</script>

<!-- my js code -->
<script src="/js/login.js"></script>
<script src="/js/slider-form.js"></script>
<script src="/js/all-insurance.js"></script>
<script src="/js/fetchServerPure.js" ></script>
<script src="/js/third-party.js"></script>
<script src="/js/fire.js"></script>
<script src="/js/travel.js"></script>
<script src="/js/motor.js"></script>
<script src="/js/auto-body.js"></script>
<script src="/js/health.js"></script>
<script src="/js/trav.js"></script>
<script src="/js/elec.js"></script>
