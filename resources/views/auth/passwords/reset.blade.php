@include('layouts.header')

<!--  login-box -->
<div class="login-box row reg">
    <div class="materialContainer row">
        <div class="log-box col-12">
            <div class="log-title">بازیابی رمز عبور</div>
            <form method="POST" action="{{ route('password.update') }}">
                @csrf
                @if (session('status'))
                    <div class="conftxt failed" style="color: #35b469 !important;">
                        {{ session('status') }}
                    </div>
                @endif
                @if($errors->any())
                    <div class="conftxt failed">
                        @foreach ($errors->all() as $error)
                            {{$error}}<br/>
                        @endforeach
                    </div>
                @endif
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="input">
                    <label for="email"> ایمیل</label>
                    <input type="text" name="email" id="email" value="{{ $email ?? old('email') }}" required="">
                    <span class="spin"></span>
                </div>
                <div class="input">
                    <label for="pass"> رمز عبور </label>
                    <input type="password" name="password" minlength="8" id="pass" class="" value="<?php echo $pass ?? ''; ?>"
                           required="">
                    <span class="spin"></span>
                </div>
                <div class="input">
                    <label for="repass"> تکرار رمز عبور </label>
                    <input type="password" name="password_confirmation" id="repass" class=""
                           value="<?php echo $repass ?? ''; ?>" required="">
                    <span class="spin"></span>
                </div>
                <div class="button regg">
                    <button type="submit" class="send-btn">
                        <span> ثبت رمز عبور جدید  </span>
                        <i class="fa fa-check"></i>
                    </button>
                </div>
            </form>
        </div>

    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<script type="text/javascript">
    /*
        function save(){

            var user=$("#username").val();
            var pass=$("#pass").val();
            var repass=$("#repass").val();

            var d={"user":user,"pass":pass,"repass":repass};

            $.ajax({
                url: 'save.php',
                type: 'POST',
                data:d;
                success:callback
            });

        }
        */
</script>
@include('layouts.footer')
