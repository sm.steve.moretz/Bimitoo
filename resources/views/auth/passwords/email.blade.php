@include('layouts.header')

<!--  login-box -->
<div class="login-box row reg">
    <div class="materialContainer row">
        <div class="log-box col-12">
            <div class="log-title">بازیابی رمز عبور</div>
            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                @if (session('status'))
                    <div class="conftxt failed" style="color: #35b469 !important;">
                        {{ session('status') }}
                    </div>
                @endif
                @if($errors->any())
                    <div class="conftxt failed">
                        @foreach ($errors->all() as $error)
                            {{$error}}<br/>
                        @endforeach
                    </div>
                @endif
                <div class="input">
                    <label for="email"> ایمیل</label>
                    <input type="text" name="email" id="email" value="<?php echo $user ?? ''; ?>" required="">
                    <span class="spin"></span>
                </div>
                <input hidden value="{{$code ?? ''}}" name="code">
                <input hidden value="{{$mobile ?? ''}}" name="phone">
                <div class="button regg">
                    <button type="submit" class="send-btn">
                        <span> بازیابی  </span>
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </form>
        </div>

    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<script type="text/javascript">
    /*
        function save(){

            var user=$("#username").val();
            var pass=$("#pass").val();
            var repass=$("#repass").val();

            var d={"user":user,"pass":pass,"repass":repass};

            $.ajax({
                url: 'save.php',
                type: 'POST',
                data:d;
                success:callback
            });

        }
        */
</script>
@include('layouts.footer')
