@include('layouts.header')

<!-- content session -->
<section id="elec-insurance-compare" class="compare-sec">
    <div class="compare-header">
        <span class="bread-crump"> بی می تو / بیمه تجهیزات الکترونیک </span>
        <ul class="advertisment">
        </ul>
    </div>
    <div class="compare-cont" id="wizard-compare8">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                    <div class="form-box">
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header first-head" >
                                    <h5 class="mb-0 card-header-box"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn ">
                                            اطلاعات دستگاه
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse show"   data-parent="#accordion">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="form-group req">
                                                <label class="compare-label"> نوع دستگاه : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="Device-select" NameInObj="DeviceId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> برند : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="DeviceBrand-select" NameInObj="BrandId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label">مدل : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="DeviceModel-select" NameInObj="ModelId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> فرانشیز : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="DeviceFran-select" NameInObj="FranchiseId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> قیمت دستگاه (ریال)  : </label>
                                                <span class="req-star"> * </span>
                                                <input type="number" class="compare-input form-control " id="DevicePrice-input" value="00" NameInObj="Price">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-12">
                    <div class="result-list">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loading-box dn">
        <div class="loading-sq"></div>
    </div>

</section>

@include('layouts.footer')
<script type="text/javascript">

    $(".top-nav-menu .nav-item").removeClass("active");
    $(".top-nav-menu #m8").addClass("active");

</script>
<script src="/js/fetchServerPure.js" ></script>
<script src="/js/elec-compare.js"></script>

</body>

</html>
