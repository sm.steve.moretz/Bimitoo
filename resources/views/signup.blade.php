@include('layouts.header')

<!--  login-box -->
<div class="login-box row reg">
    <div class="materialContainer row">
        <div class="log-box col-12">
            <div class="log-title"> ثبت نام</div>
            <form method="POST" action="{{url('register')}}">
                @csrf
                @if($errors->any())
                    <div class="conftxt failed">
                        @foreach ($errors->all() as $error)
                            {{$error}}<br/>
                        @endforeach
                    </div>
                @endif
                <div class="input">
                    <label for="username"> نام کاربری </label>
                    <input type="text" name="name" id="username" value="{{old('name')}}" >
                    <span class="spin"></span>
                </div>
                <div class="input">
                    <label for="pass"> رمز عبور </label>
                    <input type="password" name="password" minlength="8" id="pass" class="" value="{{old('password')}}"
                           >
                    <span class="spin"></span>
                </div>
                <div class="input">
                    <label for="repass"> تکرار رمز عبور </label>
                    <input type="password" name="password_confirmation" id="repass" class=""
                           value="{{old('password_confirmation')}}">
                    <span class="spin"></span>
                </div>
                <div class="input">
                    <label for="email"> ایمیل</label>
                    <input type="text" name="email" id="email" value="{{old('email')}}" >
                    <span class="spin"></span>
                </div>
                <input hidden value="{{$code}}" name="code">
                <input hidden value="{{$mobile}}" name="phone">
                <div class="button regg">
                    <button type="submit" class="send-btn">
                        <span> ثبت  نام  </span>
                        <i class="fa fa-check"></i>
                    </button>
                </div>
            </form>
            <p class="pass-forgot"> اگر قبلا در بی می تو ثبت نام کردید <a href="login"> ورود </a> را کلیک کنید </p>

        </div>

    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<script type="text/javascript">
    /*
        function save(){

            var user=$("#username").val();
            var pass=$("#pass").val();
            var repass=$("#repass").val();

            var d={"user":user,"pass":pass,"repass":repass};

            $.ajax({
                url: 'save.php',
                type: 'POST',
                data:d;
                success:callback
            });

        }
        */
</script>
@include('layouts.footer')
