@include('layouts.header')
<section style="margin-bottom: 30px;margin-top: 122px;" id="contact-us">
    <div class="container">
        <div class="row ">
            <div class="col-md-12 shadow " >

                <div class="row" style="margin-right:37%; ">
                    <div class="third-title  col-md-4" style="    padding-right: 50px;">
                        <h4>
                            فرم همکاری با ما
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 col-md-offset-1">
                        <form style="margin-top: 20px;padding: 1px 14px;">
                            <div class="form-group col-md-5">
                                <label for="nameInput">
                                    نوع نمایندگی
                                    <span style="color: red">*</span>              </label>
                                <select name="" id="" class="form-control none-border">
                                    <option value=""></option>
                                    <option value="">حقیقی</option>
                                    <option value="">حقوقی</option>
                                </select>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="lastInput">نام مدیر مجموعه  <span style="color: red">*</span></label>
                                <input type="text" class="form-control none-border" id="lastInput" placeholder=""
                                       required>
                            </div>

                            <div class="form-group col-md-5">
                                <label for="phoneInput">کدنمایندگی <span style="color: red">*</span></label>
                                <input type="phone" class="form-control none-border" id="phoneInput" placeholder=""
                                       required>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="emailInput">تعداد پرسنل  <span style="color: red">*</span></label>
                                <input type="text" class="form-control none-border" id="emailInput" placeholder=""
                                       required>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="emailInput">سال ثبت نمایندگی  <span style="color: red">*</span></label>
                                <input type="text" class="form-control none-border" id="emailInput" placeholder=""
                                       required>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="emailInput">ایمیل  <span style="color: red">*</span></label>
                                <input type="email" class="form-control none-border" id="emailInput" placeholder=""
                                       required>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="emailInput">نام شرکت بیمه </label>
                                <select name="" id="" class="form-control none-border">
                                    <option value=""></option>
                                    <option value="">بیمه ایران</option>
                                    <option value="">بیمه آسیا</option>
                                    <option value="">بیمه البرز</option>
                                    <option value="">بیمه سامان </option>
                                    <option value="">بیمه پاسارگاد</option>
                                    <option value="">بیمه پارسیان</option>
                                    <option value="">بیمه معلم</option>
                                    <option value="">بیمه نوین</option>
                                    <option value="">بیمه رازی</option>
                                    <option value="">بیمه دی</option>
                                    <option value="">بیمه ملت</option>
                                    <option value="">بیمه آسماری</option>
                                    <option value="">بیمه تعاون </option>
                                    <option value="">بیمه میهن</option>

                                </select>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="emailInput">پیک در اختیار <span style="color: red">*</span></label>
                                <select name="" id="" class="form-control none-border">
                                    <option value=""></option>
                                    <option value="">دارد</option>
                                    <option value="">ندارد</option>
                                </select>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="emailInput">تلفن ثابت  <span style="color: red">*</span></label>
                                <input type="tel" class="form-control none-border" id="emailInput" placeholder=""
                                       required>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="emailInput">تلفن همراه  <span style="color: red">*</span></label>
                                <input type="tel" class="form-control none-border" id="emailInput" placeholder=""
                                       required>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="emailInput">استان<span style="color: red">*</span>   </label>
                                <input type="text" class="form-control none-border" id="emailInput" placeholder=""
                                       required>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="emailInput">کار در روزهای تعطیل<span style="color: red">*</span>  </label>
                                <select name="" id="" class="form-control none-border">
                                    <option value=""></option>
                                    <option value="">دارد</option>
                                    <option value="">ندارد</option>
                                </select>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="emailInput">ساعت شروع کار<span style="color: red">*</span></label>
                                <select name="" id="" class="form-control none-border">
                                    <option value=""></option>
                                    <option value="">۷ صبح</option>
                                    <option value="">۸ صبح</option>
                                    <option value="">۹ صبح</option>
                                    <option value="">۱۰ صبح</option>
                                    <option value="">۱۱ صبح</option>
                                    <option value="">۱۲ بعداز ظهر</option>
                                    <option value="">۱ بعداز ظهر</option>
                                    <option value="">۲ بعداز ظهر</option>
                                    <option value="">۳ بعداز ظهر</option>
                                    <option value="">۴ بعداز ظهر</option>
                                    <option value="">۵ بعداز ظهر</option>
                                    <option value="">۶ بعداز ظهر</option>
                                    <option value="">۷ بعداز ظهر</option>
                                    <option value="">۸ بعداز ظهر</option>
                                </select>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="emailInput">ساعت پایان کار  <span style="color: red">*</span></label>
                                <select name="" id="" class="form-control none-border">
                                    <option value=""></option>
                                    <option value="">۷ صبح</option>
                                    <option value="">۸ صبح</option>
                                    <option value="">۹ صبح</option>
                                    <option value="">۱۰ صبح</option>
                                    <option value="">۱۱ صبح</option>
                                    <option value="">۱۲ بعداز ظهر</option>
                                    <option value="">۱ بعداز ظهر</option>
                                    <option value="">۲ بعداز ظهر</option>
                                    <option value="">۳ بعداز ظهر</option>
                                    <option value="">۴ بعداز ظهر</option>
                                    <option value="">۵ بعداز ظهر</option>
                                    <option value="">۶ بعداز ظهر</option>
                                    <option value="">۷ بعداز ظهر</option>
                                    <option value="">۸ بعداز ظهر</option>
                                </select>
                            </div>

                            <div class="form-group col-md-10">
                                <label for="emailInput">آدرس دقیق پستی<span style="color: red">*</span>  </label>
                                <textarea type="tel" class="form-control none-border" id="emailInput" placeholder=""
                                          required></textarea>
                            </div>
                            <div class="col-md-10  btn " style="margin-bottom: 25px">
                                <div class="compare-btn btn ">
                                    <a href="#">ثبت و ارسال فرم</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
@include('layouts.footer')


</body>

</html>
