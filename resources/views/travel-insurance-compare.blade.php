@include('layouts.header')

<!-- content session -->
<section id="travel-insurance-compare" class="compare-sec">
    <div class="compare-header">
        <span class="bread-crump"> بی می تو / بیمه مسافرتی </span>
        <ul class="advertisment">
        </ul>
    </div>
    <div class="compare-cont" id="wizard-compare5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                    <div class="form-box">
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header first-head" >
                                    <h5 class="mb-0 card-header-box"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn ">
                                            اطلاعات بیمه
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse show"  data-parent="#accordion">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="form-group req">
                                                <label class="compare-label"> مقصد سفر  : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="Countries-select" NameInObj="CountryId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> مدت اقامت : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="TravelDurations-select" NameInObj="DurationId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> طرح ها : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="Plans-select" NameInObj="PlanId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> نوع ویزا : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="VisaTypes-select" NameInObj="VisaTypeId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> تاریخ تولد : </label>
                                                <span class="req-star"> * </span>
                                                <input type="date" class="compare-input form-control " id="BirthDate-select" value=" " NameInObj="BirthDate">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-12">
                    <div class="result-list">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loading-box dn">
        <div class="loading-sq"></div>
    </div>
</section>

@include('layouts.footer')

<script type="text/javascript">


    $(".menu-box .menu-list .menu-list-item").removeClass("active");
    $(".menu-box .menu-list #m5").addClass("active");

</script>
<script src="/js/fetchServerPure.js" ></script>
<script src="/js/travel-compare.js"></script>

</body>

</html>
