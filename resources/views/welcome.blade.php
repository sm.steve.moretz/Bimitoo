@include('layouts.header')
@include('layouts.filter')
@include('layouts.index-content')
@include('layouts.footer')

<!-- kamadatepicker js -->
<script src="js/kamadatepicker.js"></script>

<script>
    kamaDatepicker('date1', { buttonsColor: "red" });

    var customOptions = {
        placeholder: "روز / ماه / سال"
        , twodigit: false
        , closeAfterSelect: false
        , nextButtonIcon: "fa fa-arrow-circle-right"
        , previousButtonIcon: "fa fa-arrow-circle-left"
        , buttonsColor: "blue"
        , forceFarsiDigits: true
        , markToday: true
        , markHolidays: true
        , highlightSelectedDay: true
        , sync: true
        , gotoToday: true
    }
    kamaDatepicker('date2', customOptions);

    kamaDatepicker('date3', {
        nextButtonIcon: "timeir_prev.png"
        , previousButtonIcon: "timeir_next.png"
        , forceFarsiDigits: true
        , markToday: true
        , markHolidays: true
        , highlightSelectedDay: true
        , sync: true
        , pastYearsCount: 0
        , futureYearsCount: 3
        , swapNextPrev: true
    });
    // for testing sync functionallity
    $("#date2").val("1311/10/01");

</script>

</body>

</html>









