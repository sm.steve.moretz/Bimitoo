@include('layouts.header')
<!--  login-box -->
<div class="login-box row reg">
    <div class="materialContainer row" >
        <div class="log-box col-12">
            <div class="log-title"> ثبت نام  </div>
            <form method="POST" action="{{url('verify')}}">
                @csrf
                @if($errors->any())
                    <div class="conftxt failed">
                        @foreach ($errors->all() as $error)
                            {{$error}}<br/>
                        @endforeach
                    </div>
                @endif
                <div class="input">
                    <label for="mobile" class="foc"> شماره موبایل  </label>
                    <input type="text" name="mobile" id="mobile" value="{{$mobile}}" readonly="" >
                    <span class="spin foc2"></span>
                </div>
                <div class="input">
                    <label for="code" class="foc"> کد ارسالی  </label>
                    <input type="text" name="code" id="code"  class="<?php echo ""; ?>" value="<?php echo $newcode ?? ''; ?>" required="">
                    <span class="spin foc2"></span>
                </div>
                <a href="{{url('register')}}"  name="resend" class="resend" >  ارسال مجدد کد  </a>
                <div class="button regg">
                    <button type="submit" name="confirm" class="send-btn">
                        <span>تایید کد  </span>
                        <i class="fa fa-check"></i>
                    </button>
                </div>
            </form>
            <p class="pass-forgot">  اگر  قبلا  در بی می تو ثبت نام کردید  <a href="/login" > ورود  </a>  را کلیک کنید  </p>

        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
@include('layouts.footer')









