@include('layouts.header')
<!--  login-box -->
<div class="login-box row reg">
    <div class="materialContainer row">
        <div class="log-box col-12">
            <div class="log-title"> ثبت نام  </div>
            <form method="POST" action="/reg">
                @csrf
                @if($errors->any())
                    <div class="conftxt failed">
                        @foreach ($errors->all() as $error)
                            {{$error}}<br/>
                        @endforeach
                    </div>
                @endif                <div class="input">
                    <label for="mobile"> شماره موبایل  </label>
                    <input type="text" name="mobile" id="mobile"pattern="09[0-9]{9}"
                           placeholder="09*********" value="{{old('mobile')}}">
                    <span class="spin"></span>
                </div>
                <div class="button regg">
                    <button type="submit" class="send-btn">
                        <span>ارسال  کد  </span>
                        <i class="fa fa-check"></i>
                    </button>
                </div>
            </form>
            <p class="pass-forgot">  اگر  قبلا  در بی می تو ثبت نام کردید  <a href="/login" > ورود  </a>  را کلیک کنید  </p>

        </div>

    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>

@include('layouts.footer')









